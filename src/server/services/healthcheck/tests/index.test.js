import init from '..';
import ConfigProvider from '../../../configProvider';

const mockedDate = new Date(2017, 11, 10);
global.Date = jest.fn(() => mockedDate);
jest.mock('axios', () => ({
  get: () => Promise.resolve({ data: {} }),
}));

describe('server | services | healthcheck', () => {
  it('should run healthcheck service', done => {
    const ctx = { config: { gitHash: 'HASH' }, configProvider: ConfigProvider({}) };
    const tenant = { id: 'TENANT', name: 'LABEL' };
    init(ctx).then(({ services: { healthcheck } }) => {
      healthcheck.run({ service: 'healthcheck', method: 'get' }, { req: { tenant } }).then(res => {
        expect(res.status).toEqual('OK');
        done();
      });
    });
  });
});
