const evtX = require('evtx').default;
const initHealthcheck = require('./healthcheck');
const { initServices } = require('../../utils');

const services = [initHealthcheck];

const init = ctx => {
  const {
    config: { gitHash },
    configProvider,
  } = ctx;
  const globals = { configProvider, startTime: new Date(), gitHash };
  const healthcheck = evtX(globals).configure(initServices(services));
  return Promise.resolve({
    ...ctx,
    services: { ...ctx.services, healthcheck },
  });
};

module.exports = init;
