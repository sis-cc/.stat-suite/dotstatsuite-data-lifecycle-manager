import { assocPath, path, replace, isNil } from 'ramda';
import got from 'got';
import urljoin from 'url-join';
import { HTTPError } from '../../utils/errors';
import * as jsr from 'jsrsasign';
import debug from '../../debug';

const getOpenIdConfiguration = async ({ authority } = {}) => {
  const url = urljoin(authority, '/.well-known/openid-configuration');
  try {
    return await got.get(url).json();
  } catch (err) {
    throw new Error(`Cannot load openid configuration from ${url}`);
  }
};

const getOpenIdKeys = async ({ jwks_uri } = {}) => {
  try {
    return await got.get(jwks_uri).json();
  } catch (err) {
    throw new Error(`Cannot load openid keys from ${jwks_uri}`);
  }
};

export const getUser = async ctx => {
  const { req } = ctx.locals || {};
  const { oidc } = req.member.scope;

  const bearerHeader = path(['headers', 'authorization'], req);
  if (isNil(bearerHeader)) throw new Error(`No Bearer token provided in headers`);

  const config = await getOpenIdConfiguration(oidc);
  const keys = await getOpenIdKeys(config);

  try {
    const bearerToken = replace('Bearer ', '', bearerHeader);
    const parsedtoken = jsr.jws.JWS.parse(bearerToken);

    const key = path(['keys', 0], keys);
    const pubKey = jsr.KEYUTIL.getKey(key);

    const isVerified = jsr.jws.JWS.verifyJWT(bearerToken, pubKey, {
      alg: [parsedtoken?.headerObj?.alg],
    });

    if (!isVerified) return ctx;
    return assocPath(['locals', 'user'], { email: parsedtoken?.payloadObj?.email })(ctx);
  } catch (err) {
    debug.error(`Error while verifying the bearer token: ${err}`);
    throw new Error(`Error while verifying the bearer token`);
  }
};

export const checkUser = () => async ctx => {
  const { user } = ctx.locals;
  if (!user) throw new HTTPError(401);
  return ctx;
};
