import { getMessage } from '../connector';

describe('api services connector', () => {
  describe('getMessage', () => {
    it('should return the service, the method and the input', () => {
      const req = {
        body: { foo: 'bar' },
        query: { bar: 'foo' },
        path: '/api/dataflow',
      };
      expect(getMessage(req)).toEqual({
        input: { ...req.body, ...req.query },
        service: 'api',
        method: 'dataflow',
      });
    });
  });
});
