import { pick } from 'ramda';
import got from 'got';
import urljoin from 'url-join';
import { checkUser } from './utils';

const NAME = 'sfs';

const service = {
  dataflow(dataflow) {
    const {
      config: { sfsUrl, sfsApiKey },
    } = this.globals;
    const {
      req: { member },
    } = this.locals;

    const url = urljoin(sfsUrl, 'dataflow');
    const json = pick(['spaceId', 'id', 'version', 'agencyId'], dataflow);
    const headers = { 'x-api-key': sfsApiKey, 'x-tenant': member?.id };

    return got.post(url, { json, headers }).json();
  },
};

export default evtx =>
  evtx
    .use(NAME, service)
    .service(NAME)
    .before({
      dataflow: [checkUser()],
    });
