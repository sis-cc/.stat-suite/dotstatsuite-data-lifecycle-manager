const config = {
  appId: 'data-lifecycle-manager',
  env: process.env.NODE_ENV || 'development',
  isProduction: process.env.NODE_ENV === 'production',
  gitHash: process.env.GIT_HASH || 'local',
  backendServerUrl: process.env.BACKEND_URL || 'http://localhost:7000',
  server: {
    host: process.env.SERVER_HOST || '0.0.0.0',
    port: Number(process.env.SERVER_PORT) || 7002,
  },
  configUrl: process.env.CONFIG_URL,
  sfsUrl: process.env.SFS_URL,
  sfsApiKey: process.env.SFS_API_KEY,
  authzServerUrl: process.env.AUTHZ_SERVER_URL,
  robotsPolicy: process.env.ROBOTS_POLICY,
};

module.exports = config;
