import React from 'react';
import { createRoot } from 'react-dom/client';
import * as R from 'ramda';
import App from './components/common/app';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { Router } from 'react-router';
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'react-router-redux';
import { QueryClient, QueryClientProvider } from 'react-query';
import { sisccTheme } from '@sis-cc/dotstatsuite-visions';
import {
  createTheme,
  ThemeProvider,
  StyledEngineProvider,
} from '@mui/material/styles';
import Helmet from './components/helmet';
import CssBaseline from '@mui/material/CssBaseline';
import routes from './routes';
import { RoutesContextProvider } from './components/routes-context';
import initializeI18n, { I18nProvider } from './modules/i18n';
import { injectStoreInApi } from './apiManager';
import meta from '../../package.json';
import { AuthPopUp as Auth } from './lib/oidc/auth';
import { OidcProvider } from './lib/oidc';
import {
  userSignedIn,
  userSignedOut,
  refreshToken,
} from './modules/oidc/action-creators';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';

console.info(`${meta.name}@${meta.version}`); // eslint-disable-line no-console
let isFirstRendering = true;

const initClient = () => {
  return new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
        refetchOnMount: false,
        retry: false,
        staleTime: 5 * 60 * 1000,
      },
    },
  });
};

const initAuth = (oidc = {}) => {
  if (R.isEmpty(oidc)) return;

  return Auth({
    authority: oidc.authority,
    client_id: oidc.client_id,
    redirect_uri: `${window.location.origin}/signed`,
    scope: R.defaultTo('openid email offline_access', oidc.scope),
    autoRefresh: true,
  });
};

const run = async () => {
  const initialState = {
    config: R.assocPath(
      ['sdmx', 'spaces'],
      window.CONFIG?.member?.scope?.spaces,
      window.SETTINGS,
    ),
  };

  const client = initClient();
  const history = createBrowserHistory();
  const store = configureStore(initialState, history);
  const auth = initAuth(window.CONFIG?.member?.scope?.oidc, store);

  injectStoreInApi(store);
  syncHistoryWithStore(history, store);
  initializeI18n(window.SETTINGS.i18n);

  const container = document.getElementById('root');
  const root = createRoot(container);

  const render = () => {
    if (!isFirstRendering) return;
    isFirstRendering = false;

    root.render(
      <Provider store={store}>
        <OidcProvider value={auth}>
          <I18nProvider messages={window.I18N}>
            <RoutesContextProvider value={routes}>
              <Router history={history}>
                <StyledEngineProvider injectFirst>
                  <ThemeProvider
                    theme={createTheme(sisccTheme({ rtl: 'ltr' }))}
                  >
                    <Helmet />
                    <CssBaseline />
                    <QueryClientProvider client={client}>
                      <App />
                    </QueryClientProvider>
                  </ThemeProvider>
                </StyledEngineProvider>
              </Router>
            </RoutesContextProvider>
          </I18nProvider>
        </OidcProvider>
      </Provider>,
    );
  };

  if (auth) {
    auth.on('signedIn', ({ access_token, payload }) => {
      const user = {
        given_name: payload.given_name,
        family_name: payload.family_name,
        email: payload.email,
      };
      store.dispatch(userSignedIn(access_token, user));
      render();
    });
    auth.on('tokensRefreshed', ({ access_token }) => {
      store.dispatch(refreshToken(access_token));
    });
    auth.on('signedOut', () => {
      store.dispatch(userSignedOut());
    });
    auth.on('error', event => {
      console.log('error', event); // eslint-disable-line no-console
      render();
    });

    await auth.init();

    window.addEventListener(
      'message',
      event => {
        if (event.origin !== window.location.origin) return;
        if (event.data?.type === 'signed') {
          auth.getAccessTokenFromCode(event.data?.code);
        }
        if (event.data?.type === 'login_required') {
          render();
        }
        if (event.data?.type === 'invalid_request') {
          console.error('invalid_request', event.data?.error_description); // eslint-disable-line no-console
        }
      },
      false,
    );

    if (window.location.pathname === '/signed') {
      const searchParams = new URLSearchParams(window.location.search);
      const error = searchParams.get('error');
      const key = error ? 'error_description' : 'code';
      const payload = {
        type: error ? error : 'signed',
        [key]: searchParams.get(key),
      };
      if (window.opener) {
        // popup
        window.opener.postMessage(payload, window.location.origin);
        window.close();
      } else {
        // iframe
        window.parent.postMessage(payload, window.location.origin);
      }

      return;
    }

    const _frame = window.document.createElement('iframe');

    // shotgun approach
    _frame.style.visibility = 'hidden';
    _frame.style.position = 'absolute';
    _frame.width = 0;
    _frame.height = 0;

    window.document.body.appendChild(_frame);

    _frame.src = `${auth.authorizationUrl}&prompt=none`;
  } else {
    render();
  }
};

run();
