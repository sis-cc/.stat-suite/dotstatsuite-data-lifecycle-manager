import React from 'react';
import ContentWithSide, { Content, Side } from '../components/page/content-with-side';
import Logs from '../components/logs';
import Filters from '../components/filters/logs';

const LogsPage = () => {
  return (
    <ContentWithSide>
      <Side>
        <Filters />
      </Side>
      <Content>
        <Logs />
      </Content>
    </ContentWithSide>
  );
};

export default LogsPage;
