import React from 'react';
import * as R from 'ramda';
import { Classes } from '@blueprintjs/core';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import { FormattedMessage } from '../modules/i18n';
import { styled } from '@mui/material/styles';
import { withUploadData } from '../modules/upload-data';
import Upload from '../components/upload';
import messages from '../components/messages';
import { withDataValidation } from '../components/common/withDataValidation';
import { injectIntl } from 'react-intl';

const Wrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const UploadData = props => (
  <Upload
    {...props}
    headerProps={{
      icon: (
        <UploadFileIcon sx={{ fontSize: 70, color: 'rgba(92,112,128,.5)' }} />
      ),
      title: <FormattedMessage id="upload.data.title" />,
      description: (
        <Wrapper>
          <em className={Classes.TEXT_MUTED}>
            <FormattedMessage {...messages['upload.data.help.sdmx']} />
          </em>
        </Wrapper>
      ),
    }}
    scope="data"
    validation={R.dissoc('onChangeContentType', props.validation)}
  />
);

export default R.compose(
  injectIntl,
  withUploadData,
  withDataValidation,
)(UploadData);
