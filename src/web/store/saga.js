import { all } from 'redux-saga/effects';
import { configSaga } from '../modules/config';
import { artefactsSaga } from '../modules/artefacts';
import { uploadArtefactSaga } from '../modules/upload-artefact';
import { uploadDataSaga } from '../modules/upload-data';
import { transferArtefactSaga } from '../modules/transfer-artefact';
import { transferDataSaga } from '../modules/transfer-data';
import { exportDataSaga } from '../modules/export-data';
import { selectionSaga } from '../modules/selection';
import { transferControlSaga } from '../modules/transfer-control';
import { dataflowDetailsSaga } from '../modules/dataflow-details';
import { categoriesSaga } from '../modules/categories';
import { dumpSaga } from '../modules/dump';
import { relativesSaga, deleteRelativesSaga } from '../modules/relatives';
import { i18nSaga } from '../modules/i18n';
import { categorizeSaga } from '../modules/categorize';
import { externalAuthSaga } from '../modules/external-auth';
import { defineMSDSaga } from '../modules/defineMSD';
import { sfsIndexSaga } from '../modules/sfsIndex';

export const createSaga = () => {
  return function* saga() {
    yield all([
      configSaga(),
      artefactsSaga(),
      uploadArtefactSaga(),
      uploadDataSaga(),
      transferArtefactSaga(),
      transferDataSaga(),
      exportDataSaga(),
      selectionSaga(),
      transferControlSaga(),
      dataflowDetailsSaga(),
      categoriesSaga(),
      dumpSaga(),
      relativesSaga(),
      deleteRelativesSaga(),
      i18nSaga(),
      categorizeSaga(),
      externalAuthSaga(),
      defineMSDSaga(),
      sfsIndexSaga(),
    ]);
  };
};
