import { sdmxXmlListParser } from '../sdmx-lib/parsers/xml';

describe('sdmxXmlListParser tests', () => {
  it('test', () => {
    const xml = `
<?xml version="1.0" encoding="utf-8"?>
<!--NSI Web Service v8.19.6.0-->
<message:Structure xmlns:message="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message" xmlns:structure="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">
  <message:Header>
    <message:ID>IDREF65</message:ID>
    <message:Test>false</message:Test>
    <message:Prepared>2025-02-17T15:53:00.7539866+00:00</message:Prepared>
    <message:Sender id="Unknown" />
    <message:Receiver id="Unknown" />
  </message:Header>
  <message:Structures>
    <structure:Dataflows>
      <structure:Dataflow id="SNA_TABLE1" urn="urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:SNA_TABLE1(1.0)" agencyID="OECD" version="1.0" isExternalReference="true" isFinal="true" structureURL="http://nsi-qa-reset.siscc.org/rest/dataflow/OECD/SNA_TABLE1/1.0">
        <common:Annotations>
          <common:Annotation>
            <common:AnnotationType>NonProductionDataflow</common:AnnotationType>
            <common:AnnotationText xml:lang="en">true</common:AnnotationText>
          </common:Annotation>
        </common:Annotations>
        <common:Name xml:lang="en">1. Gross domestic product (GDP)</common:Name>
        <common:Name xml:lang="fr">1. Produit intérieur brut (PIB)</common:Name>
      </structure:Dataflow>
      <structure:Dataflow id="SNA_TABLE1_JB" urn="urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:SNA_TABLE1_JB(1.0)" agencyID="OECD" version="1.0" isExternalReference="true" isFinal="true" structureURL="http://nsi-qa-reset.siscc.org/rest/dataflow/OECD/SNA_TABLE1_JB/1.0">
        <common:Annotations>
          <common:Annotation>
            <common:AnnotationType>NonProductionDataflow</common:AnnotationType>
            <common:AnnotationText xml:lang="en">true</common:AnnotationText>
          </common:Annotation>
        </common:Annotations>
        <common:Name xml:lang="en">Test of 1. Gross domestic product (GDP)</common:Name>
        <common:Name xml:lang="fr">Test pour 1. Produit intérieur brut (PIB)</common:Name>
      </structure:Dataflow>
      <structure:Dataflow id="SNA_TABLE1_JB_BIS" urn="urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:SNA_TABLE1_JB_BIS(1.0)" agencyID="OECD" version="1.0" isExternalReference="true" isFinal="true" structureURL="http://nsi-qa-reset.siscc.org/rest/dataflow/OECD/SNA_TABLE1_JB_BIS/1.0">
        <common:Annotations>
          <common:Annotation>
            <common:AnnotationType>NonProductionDataflow</common:AnnotationType>
            <common:AnnotationText xml:lang="en">true</common:AnnotationText>
          </common:Annotation>
        </common:Annotations>
        <common:Name xml:lang="en">Test of 1. Gross domestic product (GDP) - JB BIS</common:Name>
        <common:Name xml:lang="fr">Test pour 1. Produit intérieur brut (PIB) - JB BIS</common:Name>
      </structure:Dataflow>
      <structure:Dataflow id="DSD_NAMAIN1@DF_QNA_EXPENDITURE_CONTRIB" urn="urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD.NAD:DSD_NAMAIN1@DF_QNA_EXPENDITURE_CONTRIB(1.1)" agencyID="OECD.SDD.NAD" version="1.1" isExternalReference="true" isFinal="true" structureURL="http://nsi-qa-reset.siscc.org/rest/dataflow/OECD.SDD.NAD/DSD_NAMAIN1@DF_QNA_EXPENDITURE_CONTRIB/1.1">
        <common:Annotations>
          <common:Annotation>
            <common:AnnotationType>NonProductionDataflow</common:AnnotationType>
            <common:AnnotationText xml:lang="en">true</common:AnnotationText>
          </common:Annotation>
          <common:Annotation id="@SDMX">
            <common:AnnotationTitle>TRANSACTION,SECTOR</common:AnnotationTitle>
            <common:AnnotationType>LAYOUT_ROW</common:AnnotationType>
          </common:Annotation>
          <common:Annotation id="@SDMX">
            <common:AnnotationTitle>TIME_PERIOD</common:AnnotationTitle>
            <common:AnnotationType>LAYOUT_COLUMN</common:AnnotationType>
          </common:Annotation>
          <common:Annotation id="@SDMX">
            <common:AnnotationTitle>REF_AREA,COMBINED_UNIT_MEASURE</common:AnnotationTitle>
            <common:AnnotationType>LAYOUT_ROW_SECTION</common:AnnotationType>
          </common:Annotation>
          <common:Annotation id="@SDMX">
            <common:AnnotationTitle>ACTIVITY=(_T+_Z),EXPENDITURE=(_T+_Z),INSTR_ASSET,COUNTERPART_SECTOR,OBS_STATUS=A,CONF_STATUS=F,UNIT_MULT,SECTOR=S1,TABLE_IDENTIFIER</common:AnnotationTitle>
            <common:AnnotationType>NOT_DISPLAYED</common:AnnotationType>
          </common:Annotation>
          <common:Annotation id="@SDMX">
            <common:AnnotationTitle>FREQ=Q,LASTNPERIODS=5</common:AnnotationTitle>
            <common:AnnotationType>DEFAULT</common:AnnotationType>
          </common:Annotation>
          <common:Annotation>
            <common:AnnotationTitle>COMBINED_TRANSACTION:TRANSACTION,SECTOR;COMBINED_UNIT_MEASURE:TRANSFORMATION,UNIT_MEASURE,ADJUSTMENT</common:AnnotationTitle>
            <common:AnnotationType>COMBINED_CONCEPTS</common:AnnotationType>
            <common:AnnotationText xml:lang="en">COMBINED_TRANSACTION:Combined transaction;COMBINED_UNIT_MEASURE:Combined unit of measure</common:AnnotationText>
            <common:AnnotationText xml:lang="fr">COMBINED_TRANSACTION:Transaction combinée;COMBINED_UNIT_MEASURE:Unité de mesure combinée</common:AnnotationText>
          </common:Annotation>
          <common:Annotation id="@SDMX">
            <common:AnnotationTitle>SECTOR:OECD.SDD.NAD:HCL_SECTOR_QNA(1.0).H,TRANSACTION:OECD.SDD.NAD:HCL_TRANSACTION_EXP_QNA(1.0).H</common:AnnotationTitle>
            <common:AnnotationType>HIER_CONTEXT</common:AnnotationType>
          </common:Annotation>
          <common:Annotation>
            <common:AnnotationTitle>14000</common:AnnotationTitle>
            <common:AnnotationType>MAX_TABLE_DATA</common:AnnotationType>
          </common:Annotation>
          <common:Annotation>
            <common:AnnotationTitle>1.0001</common:AnnotationTitle>
            <common:AnnotationType>SEARCH_WEIGHT</common:AnnotationType>
          </common:Annotation>
        </common:Annotations>
        <common:Name xml:lang="en">Quarterly GDP and components  - expenditure approach, real growth rates and contributions to real growth</common:Name>
        <common:Name xml:lang="fr">PIB trimestriel et composantes - approche des dépenses, taux de croissance réelle et contributions à la croissance réelle</common:Name>
        <common:Description xml:lang="en">This table presents growth rates for Gross Domestic Product (GDP) and its main components according to the expenditure approach, as well as the contribution to GDP growth of each of the components (in percentage points). In the expenditure approach, the components of GDP are: final consumption expenditure of households and non-profit institutions serving households (NPISH) plus final consumption expenditure of General Government plus gross fixed capital formation (or investment) plus net trade (exports minus imports).
&lt;br&gt;&lt;br&gt;When using the filters, please note that final consumption expenditure is shown separately for the Households/NPISH and General Government sectors, not for the whole economy. All other components of GDP are shown for the whole economy, not for the sector breakdowns.&lt;br&gt;&lt;br&gt;The growth rates for GDP and its main components shown in this table are the same as those shown in Quarterly real GDP growth - OECD countries, but here the contributions of the components to real GDP growth are also shown for the G7 countries and the OECD total.&lt;br&gt;&lt;br&gt;These indicators were presented in the previous dissemination system in the QNA dataset.
&lt;br&gt;&lt;br&gt;See User Guide on Quarterly National Accounts (QNA) in OECD Data Explorer: &lt;a href="https://stats.oecd.org/wbos/fileview2.aspx?IDFile=431d3be5-b42d-4c6c-ade1-49d65a926f72"&gt;QNA User guide&lt;/a&gt;
&lt;br&gt;See QNA Calendar for information on advance release dates:  &lt;a href="https://stats.oecd.org/wbos/fileview2.aspx?IDFile=de5f2c7a-3fb0-4521-803a-d78030331e32"&gt;QNA Calendar&lt;/a&gt; 
&lt;br&gt;See QNA Changes for information on changes in methodology:  &lt;a href="https://stats.oecd.org/wbos/fileview2.aspx?IDFile=479ecd3c-28ec-4b04-bf6b-a6903ce31c55"&gt;QNA Changes&lt;/a&gt; 
&lt;br&gt;See QNA TIPS for a better use of QNA data:  &lt;a href="https://www.oecd.org/sdd/na/tipsforabetteruseoftheoecdquarterlynationalaccountsstatistics.htm"&gt;QNA TIPS&lt;/a&gt;
&lt;br&gt;Explore also the GDP and non-financial accounts webpage: &lt;a href="https://www.oecd.org/en/data/datasets/gdp-and-non-financial-accounts.html"&gt;GDP and non-financial accounts webpage&lt;/a&gt;&lt;br&gt;OECD statistics contact: &lt;a href="mailto:STAT.Contact@oecd.org"&gt;STAT.Contact@oecd.org&lt;/a&gt;</common:Description>
        <common:Description xml:lang="fr">Ce tableau présente le Produit Intérieur Brut (PIB) et ses composantes principales selon l'approche par les dépenses, ainsi que les contributions de chaque composante du PIB à la croissance du PIB (en points de pourcentage). Dans l'approche par les dépenses, les composantes du PIB sont : les dépenses de consommation finale des ménages et des institutions sans but lucratif au service des ménages (ISBLSM) plus les dépenses de consommation finale des administrations publiques plus la formation brute de capital fixe (ou investissement) plus le commerce net (exportations moins importations).
&lt;br&gt;&lt;br&gt;En utilisant les filtres, veuillez noter que les dépenses de consommation finale sont montrées séparément pour les secteurs des ménages/NPISH et des administrations publiques, et non pour l'économie totale. Les autres composantes du PIB sont montrées pour l'économie totale, et non pour les secteurs détaillés.
&lt;br&gt;&lt;br&gt;Les taux de croissance du PIB et de ses composantes montrés dans ce tableau sont les mêmes que ceux montrés dans le tableau de la croissance trimestrielle réelle du PIB pour les pays de l'OCDE, mais ici les contributions des composantes sont montrées pour les pays du G7 et l'OCDE total seulement.
&lt;br&gt;Les données sont présentées pour les pays du G20 individuellement, ainsi que pour les zones OCDE total, G20, G7, OCDE Europe, Accord Canada–États-Unis–Mexique (ACEUM), Union européenne et zone euro. 
&lt;br&gt;&lt;br&gt;Ces indicateurs étaient présentés dans le système de diffusion précédent dans l'ensemble de données QNA.
&lt;br&gt;Voir le guide utilisateur des Comptes Nationaux Trimestriels (QNA) dans l'explorateur des données de l'OCDE (en anglais seulement): &lt;a href="https://stats.oecd.org/wbos/fileview2.aspx?IDFile=431d3be5-b42d-4c6c-ade1-49d65a926f72"&gt;QNA User guide&lt;/a&gt; 
&lt;br&gt;Voir le calendrier "QNA Calendar" pour des informations sur les prochaines dates de publication (en anglais uniquement):  &lt;a href="https://stats.oecd.org/wbos/fileview2.aspx?IDFile=de5f2c7a-3fb0-4521-803a-d78030331e32"&gt;QNA Calendar&lt;/a&gt;
&lt;br&gt;Voir le document "QNA Changes" pour des informations sur les changement de méthodologie:  &lt;a href="https://stats.oecd.org/wbos/fileview2.aspx?IDFile=e81c82e7-ad8b-4ab9-8b4c-8c3e1e104c9b"&gt;QNA Changes&lt;/a&gt; 
&lt;br&gt;Explorez aussi la page Web du PIB et comptes non financiers :  &lt;a href="https://www.oecd.org/fr/donnees/datasets/gdp-and-non-financial-accounts.html"&gt;page Web du PIB et comptes non financiers&lt;/a&gt;
&lt;br&gt;Contact statistique OCDE: &lt;a href="mailto:STAT.Contact@oecd.org"&gt;STAT.Contact@oecd.org&lt;/a&gt;</common:Description>
      </structure:Dataflow>
      <structure:Dataflow id="SNA_TABLE1" urn="urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TEST:SNA_TABLE1(1.0)" agencyID="TEST" version="1.0" isExternalReference="true" isFinal="true" structureURL="http://nsi-qa-reset.siscc.org/rest/dataflow/TEST/SNA_TABLE1/1.0">
        <common:Annotations>
          <common:Annotation>
            <common:AnnotationType>NonProductionDataflow</common:AnnotationType>
            <common:AnnotationText xml:lang="en">true</common:AnnotationText>
          </common:Annotation>
          <common:Annotation>
            <common:AnnotationType>EXTERNAL_RESOURCE</common:AnnotationType>
            <common:AnnotationURL>https://statistiques.public.lu/fr.html</common:AnnotationURL>
            <common:AnnotationText xml:lang="en">Test En</common:AnnotationText>
            <common:AnnotationText xml:lang="fr">Test Fr</common:AnnotationText>
          </common:Annotation>
        </common:Annotations>
        <common:Name xml:lang="en">1. Gross domestic product (GDP)</common:Name>
        <common:Name xml:lang="fr">1. Produit intérieur brut (PIB)</common:Name>
      </structure:Dataflow>
    </structure:Dataflows>
  </message:Structures>
</message:Structure>
    `;

    expect(sdmxXmlListParser(xml)).toEqual([
      {
        agencyId: 'OECD',
        code: 'SNA_TABLE1',
        version: '1.0',
        sdmxId: 'OECD:SNA_TABLE1(1.0)',
        isFinal: 'true',
        type: 'dataflow',
      },
      {
        agencyId: 'OECD',
        code: 'SNA_TABLE1_JB',
        version: '1.0',
        sdmxId: 'OECD:SNA_TABLE1_JB(1.0)',
        isFinal: 'true',
        type: 'dataflow',
      },
      {
        agencyId: 'OECD',
        code: 'SNA_TABLE1_JB_BIS',
        version: '1.0',
        sdmxId: 'OECD:SNA_TABLE1_JB_BIS(1.0)',
        isFinal: 'true',
        type: 'dataflow',
      },
      {
        agencyId: 'OECD.SDD.NAD',
        code: 'DSD_NAMAIN1@DF_QNA_EXPENDITURE_CONTRIB',
        version: '1.1',
        sdmxId: 'OECD.SDD.NAD:DSD_NAMAIN1@DF_QNA_EXPENDITURE_CONTRIB(1.1)',
        isFinal: 'true',
        type: 'dataflow',
      },
      {
        agencyId: 'TEST',
        code: 'SNA_TABLE1',
        version: '1.0',
        sdmxId: 'TEST:SNA_TABLE1(1.0)',
        isFinal: 'true',
        type: 'dataflow',
      },
    ]);
  });
});
