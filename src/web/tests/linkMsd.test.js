import { getMsdAttachedXml, getXmlDefinedMsd } from '../modules/defineMSD/utils';

describe('getMsdAttachedXml tests', () => {
  const msd = { agencyId: 'AG', code: 'MSD', version: '1.0' };
  it('no annotations on DSD', () => {
    const dsd = `<?xml version="1.0" encoding="utf-8"?>
      <message:Structure>
        <message:Structures>
          <structure:DataStructures>
            <structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false"/>
          </structure:DataStructures>
        </message:Structures>
      </message:Structure>`;

    const expected = `<message:Structure><message:Structures><structure:DataStructures><structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false"><common:Annotations><common:Annotation><common:AnnotationTitle>urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=AG:MSD(1.0)</common:AnnotationTitle><common:AnnotationType>METADATA</common:AnnotationType></common:Annotation></common:Annotations></structure:DataStructure></structure:DataStructures></message:Structures></message:Structure>`;

    expect(getMsdAttachedXml(dsd, msd)).toEqual(expected);
  });
  it('one existing annotation on DSD', () => {
    const dsd = `<?xml version="1.0" encoding="utf-8"?>
      <message:Structure>
        <message:Structures>
          <structure:DataStructures>
            <structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false">
              <common:Annotations>
                <common:Annotation>
                  <common:AnnotationTitle>test</common:AnnotationTitle>
                  <common:AnnotationType>ANNOT</common:AnnotationType>
                </common:Annotation>
              </common:Annotations>
            </structure:DataStructure>
          </structure:DataStructures>
        </message:Structures>
      </message:Structure>`;

    const expected = `<message:Structure><message:Structures><structure:DataStructures><structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false"><common:Annotations><common:Annotation><common:AnnotationTitle>test</common:AnnotationTitle><common:AnnotationType>ANNOT</common:AnnotationType></common:Annotation><common:Annotation><common:AnnotationTitle>urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=AG:MSD(1.0)</common:AnnotationTitle><common:AnnotationType>METADATA</common:AnnotationType></common:Annotation></common:Annotations></structure:DataStructure></structure:DataStructures></message:Structures></message:Structure>`;

    expect(getMsdAttachedXml(dsd, msd)).toEqual(expected);
  });
  it('multiple existing annotations on DSD', () => {
    const dsd = `<?xml version="1.0" encoding="utf-8"?>
      <message:Structure>
        <message:Structures>
          <structure:DataStructures>
            <structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false">
              <common:Annotations>
                <common:Annotation>
                  <common:AnnotationTitle>test</common:AnnotationTitle>
                  <common:AnnotationType>ANNOT</common:AnnotationType>
                </common:Annotation>
                <common:Annotation>
                  <common:AnnotationTitle>random</common:AnnotationTitle>
                  <common:AnnotationType>ANNOT_2</common:AnnotationType>
                </common:Annotation>
              </common:Annotations>
            </structure:DataStructure>
          </structure:DataStructures>
        </message:Structures>
      </message:Structure>`;

    const expected = `<message:Structure><message:Structures><structure:DataStructures><structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false"><common:Annotations><common:Annotation><common:AnnotationTitle>test</common:AnnotationTitle><common:AnnotationType>ANNOT</common:AnnotationType></common:Annotation><common:Annotation><common:AnnotationTitle>random</common:AnnotationTitle><common:AnnotationType>ANNOT_2</common:AnnotationType></common:Annotation><common:Annotation><common:AnnotationTitle>urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=AG:MSD(1.0)</common:AnnotationTitle><common:AnnotationType>METADATA</common:AnnotationType></common:Annotation></common:Annotations></structure:DataStructure></structure:DataStructures></message:Structures></message:Structure>`;

    expect(getMsdAttachedXml(dsd, msd)).toEqual(expected);
  });
});

describe('getXmlDefinedMsd tests', () => {
  it('no annotations attached test', () => {
    const dsd = `<?xml version="1.0" encoding="utf-8"?>
      <message:Structure>
        <message:Structures>
          <structure:DataStructures>
            <structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false"/>
          </structure:DataStructures>
        </message:Structures>
      </message:Structure>`;

    expect(getXmlDefinedMsd(dsd)).toEqual(undefined);
  });
  it('no msd attached test 1', () => {
    const dsd = `<?xml version="1.0" encoding="utf-8"?>
      <message:Structure>
        <message:Structures>
          <structure:DataStructures>
            <structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false">
              <common:Annotations>
                <common:Annotation>
                  <common:AnnotationTitle>test</common:AnnotationTitle>
                  <common:AnnotationType>ANNOT</common:AnnotationType>
                </common:Annotation>
              </common:Annotations>
            </structure:DataStructure>
          </structure:DataStructures>
        </message:Structures>
      </message:Structure>`;

    expect(getXmlDefinedMsd(dsd)).toEqual(undefined);
  });
  it('no msd attached test 2', () => {
    const dsd = `<?xml version="1.0" encoding="utf-8"?>
      <message:Structure>
        <message:Structures>
          <structure:DataStructures>
            <structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false">
              <common:Annotations>
                <common:Annotation>
                  <common:AnnotationTitle>test</common:AnnotationTitle>
                  <common:AnnotationType>ANNOT</common:AnnotationType>
                </common:Annotation>
                <common:Annotation>
                  <common:AnnotationTitle>random</common:AnnotationTitle>
                  <common:AnnotationType>ANNOT_2</common:AnnotationType>
                </common:Annotation>
              </common:Annotations>
            </structure:DataStructure>
          </structure:DataStructures>
        </message:Structures>
      </message:Structure>`;

    expect(getXmlDefinedMsd(dsd)).toEqual(undefined);
  });
  it('msd attached test 1', () => {
    const dsd = `<message:Structure>
      <message:Structures>
        <structure:DataStructures>
          <structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false">
            <common:Annotations>
              <common:Annotation>
                <common:AnnotationTitle>urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=AG:MSD(1.0)</common:AnnotationTitle>
                <common:AnnotationType>METADATA</common:AnnotationType>
              </common:Annotation>
            </common:Annotations>
          </structure:DataStructure>
        </structure:DataStructures>
      </message:Structures>
    </message:Structure>`;

    expect(getXmlDefinedMsd(dsd)).toEqual('AG:MSD(1.0)');
  });
  it('msd attached test 2', () => {
    const dsd = `<message:Structure>
      <message:Structures>
        <structure:DataStructures>
          <structure:DataStructure id="DSD" agencyID="AG" version="1.0" isFinal="false">
            <common:Annotations>
              <common:Annotation>
                <common:AnnotationTitle>test</common:AnnotationTitle>
                <common:AnnotationType>ANNOT</common:AnnotationType>
              </common:Annotation>
              <common:Annotation>
                <common:AnnotationTitle>random</common:AnnotationTitle>
                <common:AnnotationType>ANNOT_2</common:AnnotationType>
              </common:Annotation>
              <common:Annotation>
                <common:AnnotationTitle>urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=AG:MSD(1.0)</common:AnnotationTitle>
                <common:AnnotationType>METADATA</common:AnnotationType>
              </common:Annotation>
            </common:Annotations>
          </structure:DataStructure>
        </structure:DataStructures>
      </message:Structures>
    </message:Structure>`;

    expect(getXmlDefinedMsd(dsd)).toEqual('AG:MSD(1.0)');
  });
});
