import { sdmxXmlErrorParser } from '../sdmx-lib/parsers';

describe('sdmxXmlErrorParser test', () => {
  it('test', () => {
    const xml = `<?xml version="1.0" encoding="utf-8"?>
    <message:Error xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:message="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message">
      <message:ErrorMessage code="200">
        <common:Text xml:lang="en">First Message</common:Text>
      </message:ErrorMessage>
      <message:ErrorMessage code="200">
        <common:Text xml:lang="en">Second Message</common:Text>
      </message:ErrorMessage>
      <message:ErrorMessage code="200">
        <common:Text xml:lang="en">Third Message</common:Text>
      </message:ErrorMessage>
    </message:Error>`;

    expect(sdmxXmlErrorParser(xml)).toEqual([
      'First Message',
      'Second Message',
      'Third Message',
    ]);
  });
});
