import { isArtefactDeleteAuthorized } from '../lib/permissions';

const artefact = {
  type: 'dataflow',
  code: 'DF',
  agencyId: 'A',
  version: '1',
  space: { id: 'nsi_stable' },
};

describe('isArtefactDeleteAuthorized tests', () => {
  it('no permissions', () => {
    expect(isArtefactDeleteAuthorized({}, [])).toEqual(true);
  });
  it('no delete permissions for dataspace', () => {
    const permissions = [
      {
        permission: 1,
        dataSpace: 'nsi_stable',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '*',
      },
      {
        permission: 4095,
        dataSpace: 'nsi_reset',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '*',
      },
    ];
    expect(isArtefactDeleteAuthorized(artefact, permissions)).toEqual(false);
  });
  it('no delete permissions for artefact type', () => {
    const permissions = [
      {
        permission: 1,
        dataSpace: '*',
        artefactType: 22,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '*',
      },
      {
        permission: 4095,
        dataSpace: 'nsi_reset',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '*',
      },
    ];
    expect(isArtefactDeleteAuthorized(artefact, permissions)).toEqual(false);
  });
  it('no delete permissions for artefact agencyId', () => {
    const permissions = [
      {
        permission: 1,
        dataSpace: '*',
        artefactType: 0,
        artefactAgencyId: 'A',
        artefactId: '*',
        artefactVersion: '*',
      },
      {
        permission: 4095,
        dataSpace: 'nsi_reset',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '*',
      },
    ];
    expect(isArtefactDeleteAuthorized(artefact, permissions)).toEqual(false);
  });
  it('no delete permissions for artefact code', () => {
    const permissions = [
      {
        permission: 1,
        dataSpace: '*',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: 'DF',
        artefactVersion: '*',
      },
      {
        permission: 4095,
        dataSpace: 'nsi_reset',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '*',
      },
    ];
    expect(isArtefactDeleteAuthorized(artefact, permissions)).toEqual(false);
  });
  it('no delete permissions for artefact version', () => {
    const permissions = [
      {
        permission: 1,
        dataSpace: '*',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '1',
      },
      {
        permission: 4095,
        dataSpace: 'nsi_reset',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '*',
      },
    ];
    expect(isArtefactDeleteAuthorized(artefact, permissions)).toEqual(false);
  });
  it('delete permissions granted', () => {
    const permissions = [
      {
        permission: 4095,
        dataSpace: 'nsi_stable',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '*',
      },
      {
        permission: 4095,
        dataSpace: 'nsi_reset',
        artefactType: 0,
        artefactAgencyId: '*',
        artefactId: '*',
        artefactVersion: '*',
      },
    ];
    expect(isArtefactDeleteAuthorized(artefact, permissions)).toEqual(true);
  });
});
