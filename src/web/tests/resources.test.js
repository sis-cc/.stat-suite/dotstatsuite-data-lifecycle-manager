import {
  aggregateFieldsByLocale,
  formatForAnnotation,
  addResourcesToXML,
} from '../lib/artefacts/resources';

describe('manage resources lib tests', () => {
  it('aggregateFieldsByLocale test', () => {
    const resource = {
      labels: [
        { value: 'My resource', locales: ['en'] },
        { value: 'Ma ressource', locales: ['fr'] },
      ],
      links: [{ value: 'http://link', locales: ['en', 'fr'] }],
      icons: [{ value: 'http://icon-link', locales: ['en'] }],
    };

    expect(aggregateFieldsByLocale(resource)).toEqual({
      en: {
        label: 'My resource',
        link: 'http://link',
        icon: 'http://icon-link',
      },
      fr: {
        label: 'Ma ressource',
        link: 'http://link',
      },
    });
  });
  it('formatForAnnotation test', () => {
    const resource = {
      en: {
        label: 'My resource',
        link: 'http://link',
        icon: 'http://icon-link',
      },
      fr: {
        label: 'Ma ressource',
        link: 'http://link',
      },
    };

    expect(formatForAnnotation(resource)).toEqual({
      common$AnnotationType: {
        $t: 'EXT_RESOURCE',
      },
      common$AnnotationText: [
        {
          xml$lang: 'en',
          $t: 'My resource|http://link|http://icon-link',
        },
        {
          xml$lang: 'fr',
          $t: 'Ma ressource|http://link',
        },
      ],
    });
  });
  it('addResourcesToXML no resources test', () => {
    const dfXML = {
      message$Structure: {
        xmlns$message:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message',
        xmlns$structure:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure',
        xmlns$common:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common',
        message$Header: {
          message$ID: {
            $t: 'IDREF147',
          },
          message$Test: {
            $t: 'false',
          },
          message$Prepared: {
            $t: '2025-02-05T17:11:09.6563546+00:00',
          },
          message$Sender: {
            id: 'Unknown',
          },
          message$Receiver: {
            id: 'Unknown',
          },
        },
        message$Structures: {
          structure$Dataflows: {
            structure$Dataflow: {
              id: 'DF_D7101',
              urn:
                'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=LU1:DF_D7101(1.1)',
              agencyID: 'LU1',
              version: '1.1',
              isFinal: 'true',
              common$Annotations: {
                common$Annotation: [
                  {
                    common$AnnotationType: {
                      $t: 'NonProductionDataflow',
                    },
                    common$AnnotationText: {
                      xml$lang: 'en',
                      $t: 'true',
                    },
                  },
                  {
                    id: 'DDBDataflow',
                    common$AnnotationTitle: {
                      $t: 'F983BEBEA657621BAB8A64042E93BF08',
                    },
                    common$AnnotationType: {
                      $t: 'DDBDataflow',
                    },
                  },
                  {
                    id: '@SDMX',
                    common$AnnotationTitle: {
                      $t:
                        'OBS_STATUS,UPPER_BOUND,LOWER_BOUND,REF_AREA_ISO2,REF_AREA_ISO3,REF_PERIOD',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_NOTE',
                    },
                  },
                  {
                    common$AnnotationType: {
                      $t: 'EXT_RESOURCE',
                    },
                    common$AnnotationText: [
                      {
                        xml$lang: 'fr',
                        $t:
                          'Personne de contact|http://www.statistiques.public.lu/fr/support/contact/index.php',
                      },
                      {
                        xml$lang: 'en',
                        $t:
                          'Contact person|http://www.statistiques.public.lu/en/support/contact/index.php',
                      },
                    ],
                  },
                  {
                    id: 'LAST_UPDATE',
                    common$AnnotationTitle: {
                      $t: '05/29/2023 12:35:39',
                    },
                    common$AnnotationType: {
                      $t: 'LAST_UPDATE',
                    },
                  },
                  {
                    id: 'DEFAULT',
                    common$AnnotationTitle: {
                      $t:
                        'TIME_PERIOD_START=2017,TIME_PERIOD_END=2023,LASTNOBSERVATIONS=3',
                    },
                    common$AnnotationType: {
                      $t: 'DEFAULT',
                    },
                  },
                  {
                    id: 'LAYOUT_ROW',
                    common$AnnotationTitle: {
                      $t: 'REF_AREA',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_ROW',
                    },
                  },
                  {
                    id: 'LAYOUT_COLUMN',
                    common$AnnotationTitle: {
                      $t: 'TIME_PERIOD',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_COLUMN',
                    },
                  },
                  {
                    id: 'LAYOUT_ROW_SECTION',
                    common$AnnotationTitle: {
                      $t:
                        'FREQ,REPORTING_TYPE,SERIES,SEX,AGE,CUST_BREAKDOWN,COMPOSITE_BREAKDOWN,ACTIVITY,PRODUCT,BIOCLIMATIC_BELT,UNIT_MEASURE,UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_ROW_SECTION',
                    },
                  },
                  {
                    id: 'TABLE_LOCKED_DIMS',
                    common$AnnotationTitle: {
                      $t: 'UNIT_MEASURE|UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'TABLE_LOCKED_DIMS',
                    },
                  },
                  {
                    id: 'GRAPH_LOCKED_DIMS',
                    common$AnnotationTitle: {
                      $t: 'UNIT_MEASURE|UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'GRAPH_LOCKED_DIMS',
                    },
                  },
                  {
                    id: 'NOT_DISPLAYED',
                    common$AnnotationTitle: {
                      $t:
                        'REPORTING_TYPE,CUST_BREAKDOWN,COMPOSITE_BREAKDOWN,ACTIVITY,PRODUCT,BIOCLIMATIC_BELT,BASE_PER,NATURE,TIME_COVERAGE,CUST_BREAKDOWN_LB,DATA_LAST_UPDATE,SERIES_ID,INDICATOR,SERIES_DESC,GEO_AREA_NAME,REF_AREA_TYPE,FREQ,SERIES',
                    },
                    common$AnnotationType: {
                      $t: 'NOT_DISPLAYED',
                    },
                  },
                  {
                    id: 'LAYOUT_NUMBER_OF_DECIMALS',
                    common$AnnotationTitle: {
                      $t: '1',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_NUMBER_OF_DECIMALS',
                    },
                  },
                ],
              },
              common$Name: [
                {
                  xml$lang: 'en',
                  $t:
                    'Aggregated balance sheet of credit institutions (in million EUR) 1999 - 2019',
                },
                {
                  xml$lang: 'fr',
                  $t:
                    'Bilan agrégé des établissements de crédit luxembourgeois (en millions EUR) 1999 - 2019',
                },
              ],
              common$Description: [
                {
                  xml$lang: 'en',
                  $t:
                    'Publication date: 30/03/2020 - Periodicity: yearly - Author: Banque centrale du Luxembourg (BCL) - Category: Enterprises - Financial activities - Keywords: financial activities',
                },
                {
                  xml$lang: 'fr',
                  $t:
                    'Date de publication: 30/03/2020 - Périodicité: annuelle - Auteur: Banque centrale du Luxembourg (BCL) - Catégorie: Entreprises - Activités financières - Mots-clés: activités financières',
                },
              ],
              structure$Structure: {
                Ref: {
                  id: 'DSD_D7101',
                  version: '1.1',
                  agencyID: 'LU1',
                  package: 'datastructure',
                  class: 'DataStructure',
                },
              },
            },
          },
        },
      },
    };

    const resources = [];

    expect(addResourcesToXML(dfXML, resources)).toEqual({
      message$Structure: {
        xmlns$message:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message',
        xmlns$structure:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure',
        xmlns$common:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common',
        message$Header: {
          message$ID: {
            $t: 'IDREF147',
          },
          message$Test: {
            $t: 'false',
          },
          message$Prepared: {
            $t: '2025-02-05T17:11:09.6563546+00:00',
          },
          message$Sender: {
            id: 'Unknown',
          },
          message$Receiver: {
            id: 'Unknown',
          },
        },
        message$Structures: {
          structure$Dataflows: {
            structure$Dataflow: {
              id: 'DF_D7101',
              urn:
                'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=LU1:DF_D7101(1.1)',
              agencyID: 'LU1',
              version: '1.1',
              isFinal: 'true',
              common$Annotations: {
                common$Annotation: [
                  {
                    common$AnnotationType: {
                      $t: 'NonProductionDataflow',
                    },
                    common$AnnotationText: {
                      xml$lang: 'en',
                      $t: 'true',
                    },
                  },
                  {
                    id: 'DDBDataflow',
                    common$AnnotationTitle: {
                      $t: 'F983BEBEA657621BAB8A64042E93BF08',
                    },
                    common$AnnotationType: {
                      $t: 'DDBDataflow',
                    },
                  },
                  {
                    id: '@SDMX',
                    common$AnnotationTitle: {
                      $t:
                        'OBS_STATUS,UPPER_BOUND,LOWER_BOUND,REF_AREA_ISO2,REF_AREA_ISO3,REF_PERIOD',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_NOTE',
                    },
                  },
                  {
                    id: 'LAST_UPDATE',
                    common$AnnotationTitle: {
                      $t: '05/29/2023 12:35:39',
                    },
                    common$AnnotationType: {
                      $t: 'LAST_UPDATE',
                    },
                  },
                  {
                    id: 'DEFAULT',
                    common$AnnotationTitle: {
                      $t:
                        'TIME_PERIOD_START=2017,TIME_PERIOD_END=2023,LASTNOBSERVATIONS=3',
                    },
                    common$AnnotationType: {
                      $t: 'DEFAULT',
                    },
                  },
                  {
                    id: 'LAYOUT_ROW',
                    common$AnnotationTitle: {
                      $t: 'REF_AREA',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_ROW',
                    },
                  },
                  {
                    id: 'LAYOUT_COLUMN',
                    common$AnnotationTitle: {
                      $t: 'TIME_PERIOD',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_COLUMN',
                    },
                  },
                  {
                    id: 'LAYOUT_ROW_SECTION',
                    common$AnnotationTitle: {
                      $t:
                        'FREQ,REPORTING_TYPE,SERIES,SEX,AGE,CUST_BREAKDOWN,COMPOSITE_BREAKDOWN,ACTIVITY,PRODUCT,BIOCLIMATIC_BELT,UNIT_MEASURE,UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_ROW_SECTION',
                    },
                  },
                  {
                    id: 'TABLE_LOCKED_DIMS',
                    common$AnnotationTitle: {
                      $t: 'UNIT_MEASURE|UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'TABLE_LOCKED_DIMS',
                    },
                  },
                  {
                    id: 'GRAPH_LOCKED_DIMS',
                    common$AnnotationTitle: {
                      $t: 'UNIT_MEASURE|UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'GRAPH_LOCKED_DIMS',
                    },
                  },
                  {
                    id: 'NOT_DISPLAYED',
                    common$AnnotationTitle: {
                      $t:
                        'REPORTING_TYPE,CUST_BREAKDOWN,COMPOSITE_BREAKDOWN,ACTIVITY,PRODUCT,BIOCLIMATIC_BELT,BASE_PER,NATURE,TIME_COVERAGE,CUST_BREAKDOWN_LB,DATA_LAST_UPDATE,SERIES_ID,INDICATOR,SERIES_DESC,GEO_AREA_NAME,REF_AREA_TYPE,FREQ,SERIES',
                    },
                    common$AnnotationType: {
                      $t: 'NOT_DISPLAYED',
                    },
                  },
                  {
                    id: 'LAYOUT_NUMBER_OF_DECIMALS',
                    common$AnnotationTitle: {
                      $t: '1',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_NUMBER_OF_DECIMALS',
                    },
                  },
                ],
              },
              common$Name: [
                {
                  xml$lang: 'en',
                  $t:
                    'Aggregated balance sheet of credit institutions (in million EUR) 1999 - 2019',
                },
                {
                  xml$lang: 'fr',
                  $t:
                    'Bilan agrégé des établissements de crédit luxembourgeois (en millions EUR) 1999 - 2019',
                },
              ],
              common$Description: [
                {
                  xml$lang: 'en',
                  $t:
                    'Publication date: 30/03/2020 - Periodicity: yearly - Author: Banque centrale du Luxembourg (BCL) - Category: Enterprises - Financial activities - Keywords: financial activities',
                },
                {
                  xml$lang: 'fr',
                  $t:
                    'Date de publication: 30/03/2020 - Périodicité: annuelle - Auteur: Banque centrale du Luxembourg (BCL) - Catégorie: Entreprises - Activités financières - Mots-clés: activités financières',
                },
              ],
              structure$Structure: {
                Ref: {
                  id: 'DSD_D7101',
                  version: '1.1',
                  agencyID: 'LU1',
                  package: 'datastructure',
                  class: 'DataStructure',
                },
              },
            },
          },
        },
      },
    });
  });
  it('addResourcesToXML test', () => {
    const dfXML = {
      message$Structure: {
        xmlns$message:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message',
        xmlns$structure:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure',
        xmlns$common:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common',
        message$Header: {
          message$ID: {
            $t: 'IDREF147',
          },
          message$Test: {
            $t: 'false',
          },
          message$Prepared: {
            $t: '2025-02-05T17:11:09.6563546+00:00',
          },
          message$Sender: {
            id: 'Unknown',
          },
          message$Receiver: {
            id: 'Unknown',
          },
        },
        message$Structures: {
          structure$Dataflows: {
            structure$Dataflow: {
              id: 'DF_D7101',
              urn:
                'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=LU1:DF_D7101(1.1)',
              agencyID: 'LU1',
              version: '1.1',
              isFinal: 'true',
              common$Annotations: {
                common$Annotation: [
                  {
                    common$AnnotationType: {
                      $t: 'NonProductionDataflow',
                    },
                    common$AnnotationText: {
                      xml$lang: 'en',
                      $t: 'true',
                    },
                  },
                  {
                    id: 'DDBDataflow',
                    common$AnnotationTitle: {
                      $t: 'F983BEBEA657621BAB8A64042E93BF08',
                    },
                    common$AnnotationType: {
                      $t: 'DDBDataflow',
                    },
                  },
                  {
                    id: '@SDMX',
                    common$AnnotationTitle: {
                      $t:
                        'OBS_STATUS,UPPER_BOUND,LOWER_BOUND,REF_AREA_ISO2,REF_AREA_ISO3,REF_PERIOD',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_NOTE',
                    },
                  },
                  {
                    common$AnnotationType: {
                      $t: 'EXT_RESOURCE',
                    },
                    common$AnnotationText: [
                      {
                        xml$lang: 'fr',
                        $t:
                          'Personne de contact|http://www.statistiques.public.lu/fr/support/contact/index.php',
                      },
                      {
                        xml$lang: 'en',
                        $t:
                          'Contact person|http://www.statistiques.public.lu/en/support/contact/index.php',
                      },
                    ],
                  },
                  {
                    id: 'LAST_UPDATE',
                    common$AnnotationTitle: {
                      $t: '05/29/2023 12:35:39',
                    },
                    common$AnnotationType: {
                      $t: 'LAST_UPDATE',
                    },
                  },
                  {
                    id: 'DEFAULT',
                    common$AnnotationTitle: {
                      $t:
                        'TIME_PERIOD_START=2017,TIME_PERIOD_END=2023,LASTNOBSERVATIONS=3',
                    },
                    common$AnnotationType: {
                      $t: 'DEFAULT',
                    },
                  },
                  {
                    id: 'LAYOUT_ROW',
                    common$AnnotationTitle: {
                      $t: 'REF_AREA',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_ROW',
                    },
                  },
                  {
                    id: 'LAYOUT_COLUMN',
                    common$AnnotationTitle: {
                      $t: 'TIME_PERIOD',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_COLUMN',
                    },
                  },
                  {
                    id: 'LAYOUT_ROW_SECTION',
                    common$AnnotationTitle: {
                      $t:
                        'FREQ,REPORTING_TYPE,SERIES,SEX,AGE,CUST_BREAKDOWN,COMPOSITE_BREAKDOWN,ACTIVITY,PRODUCT,BIOCLIMATIC_BELT,UNIT_MEASURE,UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_ROW_SECTION',
                    },
                  },
                  {
                    id: 'TABLE_LOCKED_DIMS',
                    common$AnnotationTitle: {
                      $t: 'UNIT_MEASURE|UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'TABLE_LOCKED_DIMS',
                    },
                  },
                  {
                    id: 'GRAPH_LOCKED_DIMS',
                    common$AnnotationTitle: {
                      $t: 'UNIT_MEASURE|UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'GRAPH_LOCKED_DIMS',
                    },
                  },
                  {
                    id: 'NOT_DISPLAYED',
                    common$AnnotationTitle: {
                      $t:
                        'REPORTING_TYPE,CUST_BREAKDOWN,COMPOSITE_BREAKDOWN,ACTIVITY,PRODUCT,BIOCLIMATIC_BELT,BASE_PER,NATURE,TIME_COVERAGE,CUST_BREAKDOWN_LB,DATA_LAST_UPDATE,SERIES_ID,INDICATOR,SERIES_DESC,GEO_AREA_NAME,REF_AREA_TYPE,FREQ,SERIES',
                    },
                    common$AnnotationType: {
                      $t: 'NOT_DISPLAYED',
                    },
                  },
                  {
                    id: 'LAYOUT_NUMBER_OF_DECIMALS',
                    common$AnnotationTitle: {
                      $t: '1',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_NUMBER_OF_DECIMALS',
                    },
                  },
                ],
              },
              common$Name: [
                {
                  xml$lang: 'en',
                  $t:
                    'Aggregated balance sheet of credit institutions (in million EUR) 1999 - 2019',
                },
                {
                  xml$lang: 'fr',
                  $t:
                    'Bilan agrégé des établissements de crédit luxembourgeois (en millions EUR) 1999 - 2019',
                },
              ],
              common$Description: [
                {
                  xml$lang: 'en',
                  $t:
                    'Publication date: 30/03/2020 - Periodicity: yearly - Author: Banque centrale du Luxembourg (BCL) - Category: Enterprises - Financial activities - Keywords: financial activities',
                },
                {
                  xml$lang: 'fr',
                  $t:
                    'Date de publication: 30/03/2020 - Périodicité: annuelle - Auteur: Banque centrale du Luxembourg (BCL) - Catégorie: Entreprises - Activités financières - Mots-clés: activités financières',
                },
              ],
              structure$Structure: {
                Ref: {
                  id: 'DSD_D7101',
                  version: '1.1',
                  agencyID: 'LU1',
                  package: 'datastructure',
                  class: 'DataStructure',
                },
              },
            },
          },
        },
      },
    };

    const resources = [
      {
        labels: [
          { value: 'My resource', locales: ['en'] },
          { value: 'Ma ressource', locales: ['fr'] },
        ],
        links: [{ value: 'http://link', locales: ['en', 'fr'] }],
        icons: [{ value: 'http://icon-link', locales: ['en'] }],
      },
      {
        labels: [{ value: 'Contact', locales: ['en', 'es', 'fr'] }],
        links: [
          { value: 'http://contact-link-en', locales: ['en'] },
          { value: 'http://contact-link-es', locales: ['es'] },
          { value: 'http://contact-link-fr', locales: ['fr'] },
        ],
        icons: [],
      },
    ];

    expect(addResourcesToXML(dfXML, resources)).toEqual({
      message$Structure: {
        xmlns$message:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message',
        xmlns$structure:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure',
        xmlns$common:
          'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common',
        message$Header: {
          message$ID: {
            $t: 'IDREF147',
          },
          message$Test: {
            $t: 'false',
          },
          message$Prepared: {
            $t: '2025-02-05T17:11:09.6563546+00:00',
          },
          message$Sender: {
            id: 'Unknown',
          },
          message$Receiver: {
            id: 'Unknown',
          },
        },
        message$Structures: {
          structure$Dataflows: {
            structure$Dataflow: {
              id: 'DF_D7101',
              urn:
                'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=LU1:DF_D7101(1.1)',
              agencyID: 'LU1',
              version: '1.1',
              isFinal: 'true',
              common$Annotations: {
                common$Annotation: [
                  {
                    common$AnnotationType: {
                      $t: 'EXT_RESOURCE',
                    },
                    common$AnnotationText: [
                      {
                        xml$lang: 'en',
                        $t: 'My resource|http://link|http://icon-link',
                      },
                      {
                        xml$lang: 'fr',
                        $t: 'Ma ressource|http://link',
                      },
                    ],
                  },
                  {
                    common$AnnotationType: {
                      $t: 'EXT_RESOURCE',
                    },
                    common$AnnotationText: [
                      {
                        xml$lang: 'en',
                        $t: 'Contact|http://contact-link-en',
                      },
                      {
                        xml$lang: 'es',
                        $t: 'Contact|http://contact-link-es',
                      },
                      {
                        xml$lang: 'fr',
                        $t: 'Contact|http://contact-link-fr',
                      },
                    ],
                  },
                  {
                    common$AnnotationType: {
                      $t: 'NonProductionDataflow',
                    },
                    common$AnnotationText: {
                      xml$lang: 'en',
                      $t: 'true',
                    },
                  },
                  {
                    id: 'DDBDataflow',
                    common$AnnotationTitle: {
                      $t: 'F983BEBEA657621BAB8A64042E93BF08',
                    },
                    common$AnnotationType: {
                      $t: 'DDBDataflow',
                    },
                  },
                  {
                    id: '@SDMX',
                    common$AnnotationTitle: {
                      $t:
                        'OBS_STATUS,UPPER_BOUND,LOWER_BOUND,REF_AREA_ISO2,REF_AREA_ISO3,REF_PERIOD',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_NOTE',
                    },
                  },
                  {
                    id: 'LAST_UPDATE',
                    common$AnnotationTitle: {
                      $t: '05/29/2023 12:35:39',
                    },
                    common$AnnotationType: {
                      $t: 'LAST_UPDATE',
                    },
                  },
                  {
                    id: 'DEFAULT',
                    common$AnnotationTitle: {
                      $t:
                        'TIME_PERIOD_START=2017,TIME_PERIOD_END=2023,LASTNOBSERVATIONS=3',
                    },
                    common$AnnotationType: {
                      $t: 'DEFAULT',
                    },
                  },
                  {
                    id: 'LAYOUT_ROW',
                    common$AnnotationTitle: {
                      $t: 'REF_AREA',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_ROW',
                    },
                  },
                  {
                    id: 'LAYOUT_COLUMN',
                    common$AnnotationTitle: {
                      $t: 'TIME_PERIOD',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_COLUMN',
                    },
                  },
                  {
                    id: 'LAYOUT_ROW_SECTION',
                    common$AnnotationTitle: {
                      $t:
                        'FREQ,REPORTING_TYPE,SERIES,SEX,AGE,CUST_BREAKDOWN,COMPOSITE_BREAKDOWN,ACTIVITY,PRODUCT,BIOCLIMATIC_BELT,UNIT_MEASURE,UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_ROW_SECTION',
                    },
                  },
                  {
                    id: 'TABLE_LOCKED_DIMS',
                    common$AnnotationTitle: {
                      $t: 'UNIT_MEASURE|UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'TABLE_LOCKED_DIMS',
                    },
                  },
                  {
                    id: 'GRAPH_LOCKED_DIMS',
                    common$AnnotationTitle: {
                      $t: 'UNIT_MEASURE|UNIT_MULT',
                    },
                    common$AnnotationType: {
                      $t: 'GRAPH_LOCKED_DIMS',
                    },
                  },
                  {
                    id: 'NOT_DISPLAYED',
                    common$AnnotationTitle: {
                      $t:
                        'REPORTING_TYPE,CUST_BREAKDOWN,COMPOSITE_BREAKDOWN,ACTIVITY,PRODUCT,BIOCLIMATIC_BELT,BASE_PER,NATURE,TIME_COVERAGE,CUST_BREAKDOWN_LB,DATA_LAST_UPDATE,SERIES_ID,INDICATOR,SERIES_DESC,GEO_AREA_NAME,REF_AREA_TYPE,FREQ,SERIES',
                    },
                    common$AnnotationType: {
                      $t: 'NOT_DISPLAYED',
                    },
                  },
                  {
                    id: 'LAYOUT_NUMBER_OF_DECIMALS',
                    common$AnnotationTitle: {
                      $t: '1',
                    },
                    common$AnnotationType: {
                      $t: 'LAYOUT_NUMBER_OF_DECIMALS',
                    },
                  },
                ],
              },
              common$Name: [
                {
                  xml$lang: 'en',
                  $t:
                    'Aggregated balance sheet of credit institutions (in million EUR) 1999 - 2019',
                },
                {
                  xml$lang: 'fr',
                  $t:
                    'Bilan agrégé des établissements de crédit luxembourgeois (en millions EUR) 1999 - 2019',
                },
              ],
              common$Description: [
                {
                  xml$lang: 'en',
                  $t:
                    'Publication date: 30/03/2020 - Periodicity: yearly - Author: Banque centrale du Luxembourg (BCL) - Category: Enterprises - Financial activities - Keywords: financial activities',
                },
                {
                  xml$lang: 'fr',
                  $t:
                    'Date de publication: 30/03/2020 - Périodicité: annuelle - Auteur: Banque centrale du Luxembourg (BCL) - Catégorie: Entreprises - Activités financières - Mots-clés: activités financières',
                },
              ],
              structure$Structure: {
                Ref: {
                  id: 'DSD_D7101',
                  version: '1.1',
                  agencyID: 'LU1',
                  package: 'datastructure',
                  class: 'DataStructure',
                },
              },
            },
          },
        },
      },
    });
  });
});
