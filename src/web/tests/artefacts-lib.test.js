import { getDfDataExplorerUrl } from '../lib/artefacts';

describe('getDfDataExplorerUrl tests', () => {
  it('basic case without customized tenant in base url', () => {
    const df = {
      space: { id: 'sp', dataExplorerUrl: 'https://de.com' },
      agencyId: 'A',
      code: 'DF',
      version: '1.0',
    };
    expect(getDfDataExplorerUrl(df, 'en')).toEqual(
      'https://de.com/vis?lc=en&df[ds]=sp&df[ag]=A&df[id]=DF&df[vs]=1.0&av=true',
    );
  });
  it('customized tenant in base url', () => {
    const df = {
      space: { id: 'sp', dataExplorerUrl: 'https://de.com?tenant=oecd:de4' },
      agencyId: 'A',
      code: 'DF',
      version: '1.0',
    };
    expect(getDfDataExplorerUrl(df, 'en')).toEqual(
      'https://de.com/vis?tenant=oecd:de4&lc=en&df[ds]=sp&df[ag]=A&df[id]=DF&df[vs]=1.0&av=true',
    );
  });
});
