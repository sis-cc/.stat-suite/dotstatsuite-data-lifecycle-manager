import { getAllChildrenIds } from '../components/artefacts/relatives';

describe('related artefacts - getAllChildrenIds tests', () => {
  it('test', () => {
    const tree = [
      {
        id: 'A',
        children: [{ id: 'A.A' }, { id: 'A.B' }, { id: 'A.C' }],
      },
      {
        id: 'B',
        childrenInTree: [{ id: 'B.A' }, { id: 'B.B' }, { id: 'B.C' }],
        children: [
          { id: 'B.A' },
          {
            id: 'B.B',
            childrenInTree: [{ id: 'B.B.A' }, { id: 'B.B.B' }],
            children: [{ id: 'B.B.A' }, { id: 'B.B.B' }],
          },
          { id: 'B.C' },
          { id: 'B.D' },
        ],
      },
      {
        id: 'C',
        sdmxParents: [{ id: 'D' }],
      },
    ];

    const logs = { 'B.D': { type: 'log' } };

    expect(getAllChildrenIds(tree, logs)).toEqual([
      'A',
      'A.A',
      'A.B',
      'A.C',
      'B',
      'B.A',
      'B.B',
      'B.B.A',
      'B.B.B',
      'B.C',
    ]);
  });
});
