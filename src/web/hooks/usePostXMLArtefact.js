import { useMutation } from 'react-query';
import { dump } from 'xml-mapping';
import * as R from 'ramda';
import apiManager from '../apiManager';
import { withAuthHeader } from '../modules/common/withauth';
import { sdmxXmlErrorParser } from '../sdmx-lib/parsers';

export const WARNING_POST_CODES = [207, 407];

export const postArtefact = (options = {}) => {
  const { space, xmlJson, locale } = options;
  const data = dump(xmlJson);
  const url = `${space.endpoint}/structure/`;
  return apiManager
    .post(url, data, {
      mode: 'cors',
      headers: withAuthHeader(space)({
        'Accept-Language': locale,
      }),
    })
    .then(response => {
      const messages = sdmxXmlErrorParser(response.data);
      const isWarning = R.includes(response.status, WARNING_POST_CODES);
      return { messages, isWarning };
    });
};

export const usePostXMLArtefact = () => {
  const mutation = useMutation(options => postArtefact(options), {
    onError: error => {
      if (error.response) {
        return sdmxXmlErrorParser(error.response.data);
      }
      return error.message;
    },
  });

  return mutation;
};
