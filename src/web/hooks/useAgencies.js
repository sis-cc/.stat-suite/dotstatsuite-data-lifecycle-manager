import { useMemo } from 'react';
import * as R from 'ramda';
import { useSelector } from 'react-redux';
import { useQueries } from 'react-query';
import { getRequestArgs, parseAgencies } from '@sis-cc/dotstatsuite-sdmxjs';
import { withAuthHeader } from '../modules/common/withauth';
import apiManager from '../apiManager';
import { getSpaces } from '../modules/config';
import { getLocale } from '../modules/i18n';

const ROOT_AGENCY_SCHEME_ID = 'SDMX';

const recurse = (agencies, agencyId, codeSelector = 'id') =>
  R.map(agency => {
    if (
      R.propOr(null, agency[codeSelector], agencies) &&
      agency[codeSelector] !== ROOT_AGENCY_SCHEME_ID
    ) {
      return {
        ...agency,
        children: recurse(agencies, agency[codeSelector]),
      };
    }
    return agency;
  }, agencies[agencyId] || []);

export default () => {
  const spaces = useSelector(getSpaces());
  const locale = useSelector(getLocale());

  const queryFn = (locale, space) => () => {
    const { url, headers, params } = getRequestArgs({
      datasource: { url: space.endpoint, ...space },
      locale,
      type: 'agencyscheme',
      identifiers: { version: '1.0', agencyId: 'all', code: 'all' },
      withReferences: false,
    });
    return apiManager
      .get(url, { headers: withAuthHeader(space)(headers), params })
      .then(res => parseAgencies({ data: R.path(['data', 'data'], res) }));
  };

  const queries = useQueries(
    R.pipe(
      R.values,
      R.map(space => ({
        queryKey: ['getAgencies', space.endpoint, locale],
        queryFn: queryFn(locale, space),
        onError: err => ({
          log: R.has('response', err)
            ? R.path(['response', 'data'], err)
            : R.path(['request', 'data'], err),
        }),
      })),
    )(spaces),
  );

  const isLoading = R.find(R.propEq('isLoading', true), queries);

  const agencies = useMemo(() => {
    if (isLoading) return [];
    return R.pipe(
      R.reject(q => !R.isNil(q.error)),
      R.pluck('data'),
      R.mergeAll,
      agencies => {
        const groupedItems = R.groupBy(item => item.parent, R.values(agencies));
        return recurse(groupedItems, ROOT_AGENCY_SCHEME_ID, 'code');
      },
    )(queries);
  }, [queries, isLoading]);

  return { isLoading, agencies };
};
