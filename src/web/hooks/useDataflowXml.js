import { useQuery } from 'react-query';
import * as R from 'ramda';
import { load } from 'xml-mapping';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import apiManager from '../apiManager';
import { withAuthHeader } from '../modules/common/withauth';

export default (dataflow = {}) => {
  const space = R.propOr({}, 'space', dataflow);
  const { url, headers, params } = getRequestArgs({
    identifiers: dataflow || {},
    datasource: R.assoc('url', space.endpoint, space),
    format: 'xml',
    type: 'dataflow',
    withReferences: false,
  });

  const queryKey = `dataflow/xml/${JSON.stringify({ url, headers, params })}`;

  const queryParams = {
    isEnabled: !R.isNil(dataflow),
  };

  return useQuery(
    queryKey,
    () =>
      apiManager
        .get(url, {
          headers: withAuthHeader(space)(headers),
          params,
        })
        .then(({ data }) => {
          const parsed = load(data);
          return parsed;
        }),
    queryParams,
  );
};
