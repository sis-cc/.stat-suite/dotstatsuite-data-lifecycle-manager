import { useQuery } from 'react-query';
import * as R from 'ramda';
import { load } from 'xml-mapping';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import apiManager from '../apiManager';
import { withAuthHeader } from '../modules/common/withauth';

export default (artefact, parser = R.identity) => {
  const space = R.propOr({}, 'space', artefact);
  const type = R.prop('type', artefact);
  const { url, headers, params } = getRequestArgs({
    identifiers: artefact || {},
    datasource: R.assoc('url', space.endpoint, space),
    format: 'xml',
    type,
    withReferences: false,
  });

  const queryKey = `${type}/xml/${JSON.stringify({ url, headers, params })}`;

  const queryParams = {
    isEnabled: !R.isNil(artefact),
    onSuccess: parser,
  };

  return useQuery(
    queryKey,
    () =>
      apiManager
        .get(url, {
          headers: withAuthHeader(space)(headers),
          params,
        })
        .then(({ data }) => {
          const parsed = load(data);
          return parsed;
        }),
    queryParams,
  );
};
