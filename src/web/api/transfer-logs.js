import * as R from 'ramda';
import apiManager from '../apiManager';
import { EXECUTION_OUTCOME_IDS, EXECUTION_STATUS_IDS } from '../modules/logs/model';

const logsRequest = requestArgs => (space = {}) => {
  const { transferUrl, id } = space;
  const {
    submissionStart,
    submissionEnd,
    artefact,
    requestId,
    executionOutcomeId,
    executionStatusId,
  } = requestArgs;

  const headers = { 'Content-Type': 'multipart/form-data' };

  const formData = new FormData();
  formData.append('dataspace', id);

  if (requestId) {
    formData.append('id', requestId);
  } else {
    if (!R.isNil(artefact)) formData.append('artefact', artefact);
    if (!R.isNil(submissionStart)) formData.append('submissionStart', submissionStart);
    if (!R.isNil(submissionEnd)) formData.append('submissionEnd', submissionEnd);
    if (!R.isNil(executionStatusId))
      formData.append('executionStatus', R.indexOf(executionStatusId, EXECUTION_STATUS_IDS));
    if (!R.isNil(executionOutcomeId))
      formData.append('executionOutcome', R.indexOf(executionOutcomeId, EXECUTION_OUTCOME_IDS));
  }

  const requestUrl = requestId ? 'request' : 'requests';
  return apiManager
    .post(`${transferUrl}/status/${requestUrl}`, formData, headers)
    .catch(err => console.error(err)); // eslint-disable-line no-console
};

export const getLogs = ({ requestArgs = {} /*parserArgs = {}*/ }) => {
  const { spaces } = requestArgs;

  // if no log, transfer should not return 404
  // the log resource exists, 200 with an empty response is expected
  return Promise.all(R.map(logsRequest(requestArgs), R.values(spaces)))
    .then(R.chain(R.propOr([], 'data')))
    .catch(err => console.error(err)); // eslint-disable-line no-console
};
