import * as R from 'ramda';
import apiManager from '../apiManager';

export const initDataflow = ({ id, sdmxId, space }) => {
  const url = `${space.transferUrl}/init/dataflow`;
  const axiosOptions = {
    mode: 'cors',
    headers: { Accept: 'application/json' },
  };
  const formData = new FormData();
  formData.append('dataspace', space.id);
  formData.append('dataflow', sdmxId);
  return apiManager
    .post(url, formData, axiosOptions)
    .catch(error => {
      if (error.response) {
        return {
          type: 'ERROR',
          artefactId: id,
          id: sdmxId,
          message: R.path(['response', 'data', 'message'], error),
        };
      }
      return { type: 'ERROR', artefactId: id, id: sdmxId, message: error.message };
    })
    .then(response => {
      if (response.type === 'ERROR') {
        return response;
      }
      if (R.hasPath(['data', 'success'], response)) {
        return {
          type: 'SUCCESS',
          artefactId: id,
          id: sdmxId,
          message: R.path(['data', 'message'], response),
        };
      }
      return {
        type: 'ERROR',
        artefactId: id,
        id: sdmxId,
        message: R.path(['data', 'message'], response),
      };
    });
};
