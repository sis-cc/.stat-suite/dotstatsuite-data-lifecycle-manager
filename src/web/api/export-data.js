import fileSaver from 'file-saver';
import * as R from 'ramda';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { withAuthHeader } from '../modules/common/withauth';
import apiManager from '../apiManager';

export const download = (file, options, format) => {
  const { agencyId, code, version } = options;
  let blob;
  if (format === 'xml') blob = new Blob([file], { type: 'text/html' });
  if (format === 'csv') blob = new Blob([file], { type: 'text/csv' });
  const filename = `${agencyId}-${code}-${version}-data.${format}`;
  fileSaver.saveAs(blob, filename);
};

export const dataflowExportData = (options = {}, format, dataquery = null) => {
  const { url, headers, params } = getRequestArgs({
    dataquery: dataquery && R.startsWith('/', dataquery) ? R.tail(dataquery) : dataquery,
    identifiers: R.pick(['agencyId', 'code', 'version'], options),
    datasource: R.assoc('url', R.path(['space', 'endpoint'], options), options.space),
    type: 'data',
    format,
  });

  return apiManager
    .get(url, { headers: withAuthHeader(options.space)(headers), params })
    .then(res => res.data)
    .then(data => download(data, options, format));
};
