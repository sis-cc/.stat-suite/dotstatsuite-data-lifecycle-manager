import * as R from 'ramda';
import apiManager from '../apiManager';

export const getUserRights = (authzServerUrl, onlyMine = false) => {
  return apiManager
    .get(`${authzServerUrl}/AuthorizationRules${onlyMine ? '/me' : ''}`)
    .then(res => R.path(['data', 'payload'], res))
    .then(R.map(R.assoc('authzServerUrl', authzServerUrl)));
};

export const deleteUserRight = (authzServerUrl, id) => {
  return apiManager
    .delete(`${authzServerUrl}/AuthorizationRules/${id}`)
    .catch(error => {
      if (error.response) {
        return {
          type: 'ERROR',
          action: 'delete',
          message: R.pipe(R.path(['response', 'data', 'errors']), R.values, R.unnest)(error),
        };
      }
      return { type: 'ERROR', action: 'delete', message: error.message };
    })
    .then(res => {
      if (res.type === 'ERROR') {
        return res;
      }
      if (R.hasPath(['data', 'success'], res)) {
        return {
          type: 'SUCCESS',
          action: 'delete',
          message: R.path(['data', 'message'], res),
        };
      }
      return {
        type: 'ERROR',
        action: 'delete',
        message: R.path(['data', 'message'], res),
      };
    });
};

export const postUserRight = (authzServerUrl, permission) => {
  const action = R.has('id', permission || {}) ? 'edit' : 'add';
  return apiManager
    .post(`${authzServerUrl}/AuthorizationRules`, permission, {
      mode: 'cors',
      headers: { Accept: 'application/json' },
    })
    .catch(error => {
      if (error.response) {
        return {
          type: 'ERROR',
          action,
          message: R.pipe(R.path(['response', 'data', 'errors']), R.values, R.unnest)(error),
        };
      }
      return { type: 'ERROR', action, message: error.message };
    })
    .then(res => {
      if (res.type === 'ERROR') {
        return res;
      }
      if (R.hasPath(['data', 'success'], res)) {
        return {
          type: 'SUCCESS',
          action,
          message: R.path(['data', 'message'], res),
        };
      }
      return {
        type: 'ERROR',
        action,
        message: R.path(['data', 'message'], res),
      };
    });
};
