import * as R from 'ramda';
import * as dateFns from 'date-fns';
import apiManager from '../apiManager';
import { SUCCESS, ERROR } from '../modules/common/constants';
import {
  XML_MIME_TYPES,
  MIME_TYPE_XLSX,
  EXCEL_EDD,
  SDMX_CSV,
} from '../modules/upload';

export const upload = (options = {}) => {
  const {
    files,
    space,
    mode,
    byPath,
    filepath,
    targetVersion,
    date,
    withRestoration,
    validationType,
    authenticateToRemoteURL = false,
  } = options;
  const endpoint = R.prop('transferUrl', space);
  const method = validationType === 'nochange' ? 'validate' : 'import';
  let url = null;
  if (mode === EXCEL_EDD) {
    url = `${endpoint}/${method}/excel`;
  }
  if (mode === SDMX_CSV) {
    url = `${endpoint}/${method}/sdmxFile`;
  }

  // API limitation: one of a kind only
  const eddFile = R.find(
    file => R.includes(file.type, XML_MIME_TYPES),
    files || [],
  );
  const excelFile = R.find(
    file => R.includes(file.type, MIME_TYPE_XLSX),
    files || [],
  );
  const sdmxFile = R.head(files || []);

  const formData = new FormData();
  formData.append('dataspace', space.id);

  if (mode === EXCEL_EDD) {
    formData.append('eddFile', eddFile);
    formData.append('excelFile', excelFile);
  }
  if (mode === SDMX_CSV) {
    if (byPath) {
      formData.append('filepath', filepath);
      formData.append('authenticateToRemoteURL', authenticateToRemoteURL);
    } else {
      formData.append('file', sdmxFile);
    }
  }
  formData.append('targetVersion', targetVersion === 'live' ? 0 : 1);
  if (method === 'import') {
    if (!R.isNil(date) && !dateFns.isBefore(date, new Date())) {
      formData.append('PITReleaseDate', dateFns.formatISO(date));
    }
    formData.append(
      'restorationOptionRequired',
      targetVersion !== 'live' && withRestoration,
    );
    formData.append('validationType', validationType === 'basic' ? 0 : 1);
  }

  const axiosOptions = {
    mode: 'cors',
    headers: { Accept: 'application/json' },
  };

  return apiManager
    .post(url, formData, axiosOptions)
    .catch(error => {
      if (error.response) {
        return {
          type: ERROR,
          message: R.path(['response', 'data', 'message'], error),
          statusKey: `transfer-ws.status.${R.path(
            ['response', 'status'],
            error,
          )}`,
        };
      }
      throw error;
    })
    .then(response => {
      if (response.type === ERROR) {
        return response;
      }
      if (R.path(['data', 'success'], response)) {
        return {
          type: SUCCESS,
          message: R.path(['data', 'message'], response),
        };
      }
      throw Error(R.path(['data', 'message'], response));
    });
};
