import { SUCCESS, ERROR } from '../modules/common/constants';
import { hasPath, isNil, path, prop } from 'ramda';
import * as dateFns from 'date-fns';
import apiManager from '../apiManager';

export const transfer = (options = {}) => {
  const {
    dataquery,
    space,
    sourceVersion,
    targetVersion,
    date,
    withRestoration,
    validationType,
    contentType,
    attachment = 'dataflow',
  } = options;
  const artefact = prop(attachment, options);
  const endpoint = prop('transferUrl', space);
  const formatType = attachment === 'dsd' ? 'Dsd' : 'Dataflow';
  const url =
    validationType === 'nochange'
      ? `${endpoint}/validate/transfer${formatType}`
      : `${endpoint}/transfer/${attachment}`;

  const formData = new FormData();
  formData.append('sourceDataspace', path(['space', 'id'], artefact));
  formData.append(`source${formatType}`, artefact.sdmxId);
  formData.append('destinationDataspace', space.id);
  formData.append(`destination${formatType}`, artefact.sdmxId);

  let transferContent = 0;
  if (contentType === 'data') {
    transferContent = 1;
  }
  if (contentType === 'metadata') {
    transferContent = 2;
  }
  if (attachment === 'dataflow') {
    formData.append('transferContent', transferContent);
  }

  formData.append('sourceVersion', sourceVersion === 'live' ? 0 : 1);
  formData.append('targetVersion', targetVersion === 'live' ? 0 : 1);

  if (validationType !== 'nochange') {
    if (!isNil(date) && !dateFns.isBefore(date, new Date())) {
      formData.append('PITReleaseDate', dateFns.formatISO(date));
    }
    formData.append(
      'restorationOptionRequired',
      targetVersion !== 'live' && withRestoration,
    );
    formData.append('validationType', validationType === 'basic' ? 0 : 1);
  }

  if (!isNil(dataquery)) {
    formData.append('sourceQuery', dataquery);
  }

  const axiosOptions = {
    method: 'POST',
    mode: 'cors',
    headers: { Accept: 'application/json' },
    body: formData,
  };

  return apiManager
    .post(url, formData, axiosOptions)
    .catch(error => {
      if (error.response) {
        return {
          type: ERROR,
          message: path(['response', 'data', 'message'], error),
          statusKey: `transfer-ws.status.${path(
            ['response', 'status'],
            error,
          )}`,
        };
      }
      throw error;
    })
    .then(response => {
      if (response.type === ERROR) {
        return response;
      }
      if (hasPath(['data', 'success'], response)) {
        return { type: SUCCESS, message: path(['data', 'message'], response) };
      }
      throw Error(path(['data', 'message'], response));
    })
    .then(log => ({
      ...log,
      source: artefact.space,
      target: space,
    }));
};
