import React from 'react';
import { compose, reduce, toPairs, find, filter, prop } from 'ramda';
import HomeIcon from '@mui/icons-material/Home';
import DriveFolderUploadIcon from '@mui/icons-material/DriveFolderUpload';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import SecurityIcon from '@mui/icons-material/Security';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import ManageStructure from './pages/manage-structure';
import UploadArtefact from './pages/upload-artefact';
import UploadData from './pages/upload-data';
import Dump from './pages/dump';
import AllRightsList from './pages/all-rights';
import MyRightsList from './pages/my-rights';
import Logs from './pages/logs';

const routes = {
  manageStructure: {
    path: '/',
    component: ManageStructure,
    exact: true,
    default: true,
    name: 'home',
    icon: <HomeIcon />,
  },
  uploadArtefact: {
    path: '/upload-artefact',
    component: UploadArtefact,
    exact: true,
    name: 'uploadArtefact',
    icon: <DriveFolderUploadIcon />,
    translationKey: 'route.upload.structure',
  },
  uploadData: {
    path: '/upload-data',
    component: UploadData,
    exact: true,
    name: 'uploadData',
    icon: <UploadFileIcon />,
    translationKey: 'route.upload.data',
  },
  logs: {
    path: '/logs',
    component: Logs,
    exact: true,
    name: 'logs',
    icon: <CalendarMonthIcon />,
    translationKey: 'route.logs',
  },
  allRights: {
    path: '/all-rights',
    component: AllRightsList,
    exact: true,
    hidden: true,
    name: 'allRights',
    icon: <SecurityIcon />,
    translationKey: 'route.all.rights',
  },
  dumpDownload: {
    path: '/dump',
    component: Dump,
    exact: true,
    hidden: true,
    name: 'dumpDownload',
    icon: <CloudDownloadIcon />,
    translationKey: 'route.dump',
  },
  myRights: {
    path: '/my-rights',
    component: MyRightsList,
    exact: true,
    name: 'myRights',
    icon: <SecurityIcon />,
    translationKey: 'route.my.rights',
  },
};

const exportedRoutes = compose(
  reduce((acc, [name, r]) => [...acc, { ...r, name }], []),
  toPairs,
  filter(prop('path')),
)(routes);

export const getDefault = () => find(route => route.default, exportedRoutes);

export const getRouteByName = name => routes[name] && { ...routes[name], name };

export const getPathByName = (name, param) => {
  const path = prop('path', getRouteByName(name));
  return param ? `${path.replace(':id', param)}` : path;
};

export default {
  getRoutes: () => exportedRoutes,
  getDefault,
  getRouteByName,
  getPathByName,
};
