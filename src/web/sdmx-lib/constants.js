//-------------------------------------------------------------types
export const AGENCY = 'agency';
export const AGENCY_SCHEME = 'agencyscheme';
export const ALL = 'all';
export const CATEGORISATION = 'categorisation';
export const CATEGORY_SCHEME = 'categoryscheme';
export const CODELIST = 'codelist';
export const CONCEPT_SCHEME = 'conceptscheme';
export const CONTENT_CONSTRAINT = 'contentconstraint';
export const DATAFLOW = 'dataflow';
export const DATA_STRUCTURE = 'datastructure';
export const HIERARCHICAL_CODELIST = 'hierarchicalcodelist';
