import { load } from 'xml-mapping';
import * as R from 'ramda';

const toArray = content => (R.is(Array, content) ? content : [content]);

export const sdmxXmlErrorParser = xml => {
  const json = load(xml);
  const messages = R.pipe(
    R.path(['message$Error', 'message$ErrorMessage']),
    toArray,
    R.map(R.path(['common$Text', '$t'])),
  )(json);

  return messages;
};

export const sdmxXmlListParser = (xml, type = 'Dataflow') => {
  const xmlJson = load(xml);
  return R.pipe(
    R.pathOr(
      [],
      [
        'message$Structure',
        'message$Structures',
        `structure$${type}s`,
        `structure$${type}`,
      ],
    ),
    R.map(df => ({
      agencyId: df.agencyID,
      code: df.id,
      version: df.version,
      isFinal: df.isFinal,
      sdmxId: `${df.agencyID}:${df.id}(${df.version})`,
      type: R.toLower(type),
    })),
  )(xmlJson);
};

export const getAnnotations = R.pipe(
  R.pathOr([], ['common$Annotations', 'common$Annotation']),
  toArray,
);

export const getAnnotationType = R.path(['common$AnnotationType', '$t']);

export const getAnnotationTexts = R.pipe(
  R.pathOr([], ['common$AnnotationText']),
  toArray,
);

export const parseLocalizedText = t => {
  const locale = R.prop('xml$lang', t);
  const text = R.prop('$t', t);

  return { locale, text };
};

export const generateLocalizedTexts = R.map(({ locale, text }) => ({
  xml$lang: locale,
  $t: text,
}));

export const generateAnnotation = ({ type, title, texts }) =>
  R.pipe(
    annot =>
      !R.isNil(title)
        ? R.assoc('common$AnnotationTitle', { $t: title }, annot)
        : annot,
    annot =>
      !R.isNil(texts)
        ? R.assoc('common$AnnotationText', generateLocalizedTexts(texts), annot)
        : annot,
  )({ common$AnnotationType: { $t: type } });

export const getDataflow = R.path([
  'message$Structure',
  'message$Structures',
  'structure$Dataflows',
  'structure$Dataflow',
]);
