import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import {
  useGetUserRights,
  useDeleteUserRight,
  useSubmitUserRight,
} from '../../hooks/user-rights';
import { Classes, Spinner, Tag } from '@blueprintjs/core';
import classnames from 'classnames';
import { getSpaces } from '../../modules/config';
import { useIntl } from 'react-intl';
import { FormattedMessage } from '../../modules/i18n';
import { setPaginationHandler, PaginationBlock } from '../list';
import { makeStyles } from '@mui/styles';
import PersonIcon from '@mui/icons-material/Person';
import GroupIcon from '@mui/icons-material/Group';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import SecurityIcon from '@mui/icons-material/Security';
import EditIcon from '@mui/icons-material/Edit';
import { Button } from '@sis-cc/dotstatsuite-visions';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import UserRightForm from './UserRightForm';
import { UserRightsFilters } from './filters';
import {
  getPermissionLabel,
  getLabel,
  filterRights,
  getArtefactTypeLabel,
  getFilterOptions,
} from './utils';
import { ActionLog } from './log';
import { DeleteDialog } from './delete-dialog';
import { SpacesScope } from './spaces-scope';

const useStyles = makeStyles(() => ({
  table: {
    '& .MuiTableCell-root': {
      padding: '10px 0',
      paddingRight: 10,
      cursor: 'pointer',
    },
    '& .MuiTableCell-head': {
      fontWeight: 'bold',
    },
    '& .MuiTableRow-root.Mui-selected': {
      backgroundColor: 'orange',
    },
  },
  action: {
    padding: '5px 0',
  },
  addButton: {
    marginRight: 7,
  },
  topButtons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
    '& .pt-align-left': {
      marginRight: 7,
    },
    '& .pt-navbar': {
      height: 'auto',
    },
    '& .pt-navbar-group': {
      height: 'auto',
    },
  },
  user: {
    display: 'flex',
    flexDirection: 'row',
    '& .MuiSvgIcon-root': {
      marginRight: 5,
    },
  },
  wrapper: {
    display: 'flex',
  },
  permissions: {
    flexGrow: 1,
    borderTop: '2px solid #999',
    paddingTop: 10,
  },
  toast: {
    maxWidth: '100% !important',
    margin: '0 0 10px !important',
  },
}));

const defaultFilterSelection = {
  spaces: {},
  types: {},
  users: {},
  agencies: {},
  artefacts: {},
  versions: {},
  permissionTypes: {},
};

const permissionTemplate = {
  userMask: '*',
  artefactAgencyId: '*',
  artefactId: '*',
  artefactType: 0,
  artefactVersion: '*',
  dataSpace: '*',
  permission: 0,
};

export const UserRightsList = ({ queryKey }) => {
  const intl = useIntl();
  const classes = useStyles();
  const { isLoading, rights } = useGetUserRights({ queryKey });
  const isManageable = queryKey === 'all-rights';
  const deleteMutation = useDeleteUserRight(rights);
  const submitMutation = useSubmitUserRight();
  const [deleteState, setDeleteState] = useState({ id: null, isOpen: false });
  const [manageState, setManageState] = useState({
    permission: null,
    isOpen: false,
    simpleView: false,
  });
  const [filtersSelection, setFiltersSelection] = useState(
    defaultFilterSelection,
  );
  const [log, setLog] = useState(null);
  const [newPermission, setNewPermission] = useState(permissionTemplate);
  const [{ page, pageSize, pageBuffer }, setPagination] = useState({
    page: 0,
    pageSize: 12,
    pageBuffer: 3,
  });

  const spaces = useSelector(getSpaces());
  const spacesByAuthz = R.pipe(
    R.values,
    R.filter(R.prop('authzServerUrl')),
    R.groupBy(R.prop('authzServerUrl')),
  )(spaces);

  const filteredRights = filterRights(rights || [], filtersSelection);

  const pageChange = page => event => {
    event.preventDefault();
    setPagination({ pageSize, pageBuffer, page });
  };
  const pagination = setPaginationHandler({ page, pageSize, pageBuffer })({
    items: filteredRights,
  });

  const paginatedUserRightsList =
    pagination.pages < 2
      ? filteredRights
      : R.slice(page * pageSize, page * pageSize + pageSize, filteredRights);

  const filtersOptions = getFilterOptions(rights || []);

  if (isLoading) {
    return <Spinner />;
  }

  const onDelete = () => {
    deleteMutation.mutate(deleteState.id, {
      onSuccess: data => {
        setLog(data);
      },
    });
    setDeleteState({ id: null, isOpen: false });
  };

  const onSubmitPermission = ({ authzServerUrl, permission }) => {
    if ((!R.has('id'), permission)) {
      setNewPermission(permission);
    }
    submitMutation.mutate(
      { authzServerUrl, permission },
      {
        onSuccess: data => {
          setLog(data);
        },
      },
    );
    setManageState({ permission: null, isOpen: false, simpleView: false });
  };

  const onPermissionView = permission =>
    setManageState({ permission, isOpen: true, simpleView: true });

  return (
    <div className={classes.wrapper}>
      <UserRightsFilters
        filters={filtersOptions}
        onChange={setFiltersSelection}
        selection={filtersSelection}
      />
      <div className={classes.permissions}>
        <div className={classes.topButtons}>
          <div>
            <PaginationBlock pagination={pagination} pageChange={pageChange} />
          </div>
          <div>
            {isManageable && (
              <Button
                className={classes.addButton}
                variant="contained"
                color="primary"
                alternative="siscc"
                startIcon={<SecurityIcon />}
                onClick={() =>
                  setManageState({
                    permission: newPermission,
                    isOpen: true,
                    simpleView: false,
                  })
                }
              >
                <FormattedMessage id="user.right.add" />
              </Button>
            )}
            <Tag className={classnames(Classes.MINIMAL, Classes.LARGE)}>
              <FormattedMessage
                id="user.rights.count"
                values={{
                  count: R.length(filteredRights),
                  total: R.length(rights),
                }}
              />
            </Tag>
          </div>
        </div>
        <ActionLog
          className={classes.toast}
          resetLog={() => setLog(null)}
          log={log}
          intl={intl}
        />
        <DeleteDialog
          {...deleteState}
          id={String(deleteState.id)}
          intl={intl}
          onDelete={onDelete}
          onClose={() => setDeleteState({ id: null, isOpen: false })}
        />
        <UserRightForm
          {...manageState}
          onSubmit={onSubmitPermission}
          onClose={() =>
            setManageState({
              permission: null,
              isOpen: false,
              simpleView: false,
            })
          }
          spaces={spacesByAuthz}
        />
        <Table
          className={classes.table}
          sx={{ minWidth: 650 }}
          aria-label="simple table"
        >
          <TableHead>
            <TableRow>
              <TableCell>
                <FormattedMessage id="user.right.id" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="user.right.group" />/
                <FormattedMessage id="user.right.user" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="user.right.space" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="user.right.artefact.type" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="user.right.artefact.agency" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="user.right.artefact.id" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="user.right.artefact.version" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="user.right.permission" />
              </TableCell>
              {isManageable && (
                <TableCell>
                  <FormattedMessage id="user.right.actions" />
                </TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            {R.addIndex(R.map)(
              (userPermission, index) => (
                <TableRow
                  key={`permission-row-${index}`}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  selected={
                    R.equals(
                      R.path(['permission', 'id'], manageState),
                      userPermission.id,
                    ) || R.equals(R.prop('id', deleteState), userPermission.id)
                  }
                  hover
                >
                  <TableCell
                    component="th"
                    scope="row"
                    onClick={() => onPermissionView(userPermission)}
                  >
                    {userPermission.id}
                  </TableCell>
                  <TableCell onClick={() => onPermissionView(userPermission)}>
                    <div className={classes.user}>
                      {userPermission.isGroup ? (
                        <GroupIcon color="primary" />
                      ) : (
                        <PersonIcon color="primary" />
                      )}
                      <p>{userPermission.userMask}</p>
                    </div>
                  </TableCell>
                  <TableCell onClick={() => onPermissionView(userPermission)}>
                    <SpacesScope permission={userPermission} />
                  </TableCell>
                  <TableCell onClick={() => onPermissionView(userPermission)}>
                    {getArtefactTypeLabel(userPermission.artefactType, intl)}
                  </TableCell>
                  <TableCell onClick={() => onPermissionView(userPermission)}>
                    {getLabel(intl, userPermission.artefactAgencyId)}
                  </TableCell>
                  <TableCell onClick={() => onPermissionView(userPermission)}>
                    {getLabel(intl, userPermission.artefactId)}
                  </TableCell>
                  <TableCell onClick={() => onPermissionView(userPermission)}>
                    {getLabel(intl, userPermission.artefactVersion)}
                  </TableCell>
                  <TableCell onClick={() => onPermissionView(userPermission)}>
                    {getPermissionLabel(intl, userPermission.permission)}
                  </TableCell>
                  {isManageable && (
                    <TableCell>
                      <div>
                        <IconButton
                          className={classes.action}
                          color="primary"
                          onClick={() =>
                            setManageState({
                              permission: userPermission,
                              isOpen: true,
                              simpleView: false,
                            })
                          }
                          size="large"
                        >
                          <EditIcon />
                        </IconButton>
                        <IconButton
                          className={classes.action}
                          color="primary"
                          onClick={() =>
                            setDeleteState({
                              id: userPermission.id,
                              isOpen: true,
                            })
                          }
                          size="large"
                        >
                          <DeleteForeverIcon />
                        </IconButton>
                      </div>
                    </TableCell>
                  )}
                </TableRow>
              ),
              paginatedUserRightsList,
            )}
          </TableBody>
        </Table>
      </div>
    </div>
  );
};

UserRightsList.propTypes = {
  isLoading: PropTypes.bool,
  isManageable: PropTypes.bool,
  permissions: PropTypes.array,
};
