import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import * as R from 'ramda';
import { makeStyles } from '@mui/styles';
import { useIntl } from 'react-intl';
import { formatMessage } from '../../modules/i18n';
import { getSpaces } from '../../modules/config';
import { getArtefactTypeLabel, getPermissionLabel } from './utils';
import Space from '../common/space';
import Filter from './filter';
import messages from '../messages';

const useStyles = makeStyles(() => ({
  filters: {
    width: 250,
    marginRight: 20,
  },
}));

export const UserRightsFilters = ({ filters, selection, onChange }) => {
  const intl = useIntl();
  const classes = useStyles();
  const spaces = useSelector(getSpaces());
  const handleCheck = filter => id => {
    const nextSelection = R.hasPath([filter, id], selection)
      ? R.dissocPath([filter, id], selection)
      : R.assocPath([filter, id], id, selection);
    onChange(nextSelection);
  };

  const enhancedFilters = R.map(
    ({ key, idRenderer, ...rest }) => ({
      ...rest,
      key,
      options: idRenderer
        ? R.map(el => ({ id: idRenderer(el) }))(R.prop(key, filters))
        : R.prop(key, filters),
      onChange: handleCheck(key),
      selection: R.prop(key, selection),
    }),
    [
      {
        key: 'spaces',
        renderer: space => (
          <Space {...R.propOr({}, space.id, spaces)} key={space.id} />
        ),
        title: formatMessage(intl)(R.prop('user.right.filter.space', messages)),
      },
      {
        key: 'types',
        idRenderer: ({ id }) => getArtefactTypeLabel(id, intl),
        title: formatMessage(intl)(
          R.prop('user.right.filter.artefact.type', messages),
        ),
      },
      {
        key: 'users',
        title: formatMessage(intl)(R.prop('user.right.filter.user', messages)),
      },
      {
        key: 'agencies',
        title: formatMessage(intl)(
          R.prop('user.right.filter.artefact.agency', messages),
        ),
      },
      {
        key: 'artefacts',
        title: formatMessage(intl)(
          R.prop('user.right.filter.artefact.id', messages),
        ),
      },
      {
        key: 'versions',
        title: formatMessage(intl)(
          R.prop('user.right.filter.artefact.version', messages),
        ),
      },
      {
        key: 'permissionTypes',
        idRenderer: ({ id }) => getPermissionLabel(intl, id),
        title: formatMessage(intl)(
          R.prop('user.right.filter.permission', messages),
        ),
      },
    ],
  );

  return (
    <div className={classes.filters}>
      {R.map(
        entry => (
          <Filter key={entry.key} id={entry.key} {...entry} />
        ),
        enhancedFilters,
      )}
    </div>
  );
};

UserRightsFilters.propTypes = {
  filters: PropTypes.object,
  onChange: PropTypes.func,
  selection: PropTypes.object,
};
