import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@mui/styles';
import { Checkbox } from '@blueprintjs/core';
import { onlyUpdateForKeys, renameProps } from 'recompose';
import Filter from '../filters/filter';
import { Spotlight, withList, withSpotlight } from '../list';

const useStyles = makeStyles(() => ({
  list: {
    maxHeight: 250,
    overflow: 'auto',
  },
  checkbox: {
    minHeight: '20px !important',
    lineHeight: '20px !important',
    '& .pt-control-indicator': {
      top: '2px !important',
    },
  },
}));
const getSortedOptions = opts =>
  R.sortBy(R.pipe(R.prop('id'), R.replace(/\t/g, ''), R.toLower))(opts);

const FilterWrapper = ({
  id,
  title,
  items,
  onChange,
  renderer,
  selection,
  ...props
}) => {
  const classes = useStyles();
  if (R.length(items) <= 1) {
    return null;
  }
  return (
    <Filter title={title} isOpen={true}>
      {R.gt(R.length(items), 8) && (
        <div style={{ padding: '10px 0px' }}>
          <Spotlight
            {...props.spotlight}
            fieldChange={props.spotlightFieldChange}
            valueChange={props.spotlightValueChange}
          />
        </div>
      )}

      <div className={classes.list}>
        {R.map(
          item => (
            <Checkbox
              key={`${id}-${item.id}`}
              className={classes.checkbox}
              onChange={() => onChange(item.id)}
              checked={R.has(item.id, selection)}
            >
              {R.is(Function, renderer) ? renderer(item) : item.id}
            </Checkbox>
          ),
          // options
          R.is(Function, renderer)
            ? props.list.items
            : getSortedOptions(props.list.items, renderer),
        )}
      </div>
    </Filter>
  );
};

FilterWrapper.propTypes = {
  id: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.array,
  renderer: PropTypes.func,
  selection: PropTypes.object,
  title: PropTypes.string,
};

export default R.compose(
  onlyUpdateForKeys(['options']),
  renameProps({ options: 'items' }),
  withList(),
  withSpotlight([{ id: 'id', accessor: 'id', isSelected: true }]),
)(FilterWrapper);
