import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Space from '../common/space';
import { getHasMultipleAuthZ, getSpaces } from '../../modules/config/selectors';
import { FormattedMessage } from '../../modules/i18n';

export const SpacesScope = ({ permission }) => {
  const hasMultipleAuthz = useSelector(getHasMultipleAuthZ);
  const spaces = useSelector(getSpaces());
  const { authzServerUrl, dataSpace } = permission;

  if (!hasMultipleAuthz && dataSpace !== '*') {
    return <Space key={dataSpace} {...R.propOr({}, dataSpace, spaces)} />;
  } else if (dataSpace === '*') {
    return <FormattedMessage id="user.right.scope.any" />;
  }
  const scope = R.filter(R.propEq('authzServerUrl', authzServerUrl), R.values(spaces));
  return (
    <div>
      {R.map(
        space => (
          <Space key={space.id} {...space} />
        ),
        scope,
      )}
    </div>
  );
};

SpacesScope.propTypes = {
  intl: PropTypes.object,
  permission: PropTypes.object,
  spacesByAuthz: PropTypes.object,
};
