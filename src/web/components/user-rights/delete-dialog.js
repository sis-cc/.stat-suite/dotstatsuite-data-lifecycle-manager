import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { formatMessage } from '../../modules/i18n';
import Dialog from '../dialog-menus/Dialog';
import messages from '../messages';

export const DeleteDialog = ({ id, isOpen, onClose, onDelete, intl }) => (
  <Dialog
    title={formatMessage(intl)(R.prop('user.right.delete', messages), { id })}
    applyLabel={formatMessage(intl)(R.prop('action.delete', messages))}
    isOpen={isOpen}
    onApply={onDelete}
    onClose={onClose}
  >
    <div>{formatMessage(intl)(R.prop('user.right.delete.confirmation', messages), { id })}</div>
  </Dialog>
);

DeleteDialog.propTypes = {
  id: PropTypes.string,
  intl: PropTypes.object,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  onDelete: PropTypes.func,
};
