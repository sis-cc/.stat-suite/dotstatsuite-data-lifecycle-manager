import React from 'react';
import { prop } from 'ramda';
import { Helmet } from 'react-helmet';
import { useIntl } from 'react-intl';
import { formatMessage } from '../modules/i18n';
import messages from './messages';

const View = () => {
  const intl = useIntl();

  return (
    <Helmet>
      <title>{formatMessage(intl)(prop('dlm.app.title')(messages))}</title>
    </Helmet>
  );
};

export default View;
