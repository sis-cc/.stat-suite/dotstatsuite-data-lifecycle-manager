import React from 'react';
import PropTypes from 'prop-types';
import Spaces from '../common/spaces';
import Actions from '../common/actions';
import Files from './files';
import Log from './log';
import { isNil, map, pick, values, is } from 'ramda';
import { styled } from '@mui/material/styles';
import cx from 'classnames';
import VerifiedUser from '@mui/icons-material/VerifiedUser';
import { Classes, Intent, NonIdealState, Colors } from '@blueprintjs/core';
import { FormattedMessage, formatMessage } from '../../modules/i18n';
import DataValidationForm, { Menu } from '../common/data-validation';
import messages from '../messages';

const Container = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const Header = styled(NonIdealState)({
  maxWidth: 'unset !important',
});

const Item = styled('div')({
  width: 500,
  margin: 10,
  padding: '0 !important',
});

const ItemContent = styled('div')({
  padding: 12,
});

const Upload = props => {
  const filesProps = pick(
    [
      'dropzoneConfig',
      'filesChange',
      'fileRemove',
      'files',
      'acceptedFiles',
      'rejectedFiles',
      'refinedRejectedFiles',
      'hasNoFile',
      'maxSize',
      'byPath',
      'filepath',
      'changeFilePath',
      'toggleByPath',
      'hasPathOption',
    ],
    props,
  );
  const { upload, validation = {}, formReset } = props;
  const resetValidationOptions = validation.resetValidationOptions;
  const spacesProps = pick(['spaces', 'selectSpace', 'removeSpace'], props);
  const actions = [
    {
      icon: 'upload',
      id: 'upload',
      disabled: !props.formIsValid,
      loading: props.isUploading,
      onClick: () => upload(validation),
      label: <FormattedMessage id="upload" />,
    },
    {
      icon: 'delete',
      id: 'upload.reset',
      intent: Intent.DANGER,
      onClick: () => {
        if (is(Function, formReset)) {
          formReset();
        }
        if (is(Function, resetValidationOptions)) {
          resetValidationOptions();
        }
      },
      label: <FormattedMessage id="upload.reset" />,
    },
  ];
  const validationLinks = isNil(props.intl)
    ? null
    : {
        validation: formatMessage(props.intl)(messages['data.validation.info']),
        pit: formatMessage(props.intl)(messages['pit.upload.info']),
      };
  const ValidationIcon = styled(VerifiedUser)({
    color: Colors.GRAY1,
  });

  return (
    <Container>
      <Item>
        <Header
          title={props.headerProps.title}
          description={props.headerProps.description}
          visual={props.headerProps.icon}
        />
      </Item>
      <Item className={Classes.CARD}>
        <div className={cx(Classes.CALLOUT, Classes.iconClass('duplicate'))}>
          <FormattedMessage id="upload.files" />
        </div>
        <ItemContent>
          <Files {...filesProps} />
        </ItemContent>
      </Item>
      <Item className={Classes.CARD}>
        <div className={cx(Classes.CALLOUT, Classes.iconClass('database'))}>
          <FormattedMessage id="upload.spaces" />
        </div>
        <ItemContent>
          <Spaces {...spacesProps} />
        </ItemContent>
      </Item>
      {isNil(props.validation) ? null : (
        <Item>
          <Menu
            icon={<ValidationIcon />}
            labels={{
              title: formatMessage(props.intl)({ id: 'data.validations' }),
              basic: formatMessage(props.intl)({
                id: 'data.validation.default',
              }),
              advanced: formatMessage(props.intl)({
                id: 'data.validation.advanced',
              }),
              nochange: formatMessage(props.intl)({
                id: 'data.validation.nochange',
              }),
            }}
            link={validationLinks.validation}
            onChange={props.validation.onChangeValidationType}
            options={['basic', 'advanced', 'nochange']}
            value={props.validation.validationType}
          />
        </Item>
      )}
      {isNil(props.validation) ? null : (
        <Item>
          <DataValidationForm
            {...props.validation}
            hasSource={false}
            links={validationLinks}
            intl={props.intl}
          />
        </Item>
      )}
      <Item>
        <Actions actions={actions} />
      </Item>
      <Item>
        {map(
          log => (
            <Log {...log} scope={props.scope} key={log.space.id} />
          ),
          values(props.log),
        )}
      </Item>
    </Container>
  );
};

Upload.propTypes = {
  headerProps: PropTypes.object,
  fileRemove: PropTypes.func.isRequired,
  filesChange: PropTypes.func.isRequired,
  files: PropTypes.array,
  acceptedFiles: PropTypes.object,
  rejectedFiles: PropTypes.object,
  hasNoFile: PropTypes.bool,
  selectSpace: PropTypes.func.isRequired,
  removeSpace: PropTypes.func.isRequired,
  scope: PropTypes.string,
  spaces: PropTypes.object,
  upload: PropTypes.func.isRequired,
  dropzoneConfig: PropTypes.object.isRequired,
  formIsValid: PropTypes.bool,
  formReset: PropTypes.func.isRequired,
  isUploading: PropTypes.bool,
  log: PropTypes.object,
  maxSize: PropTypes.number,
  validation: PropTypes.object,
  intl: PropTypes.object,
};

export default Upload;
