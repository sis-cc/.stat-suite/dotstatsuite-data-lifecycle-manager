import React from 'react';
import PropTypes from 'prop-types';
import { Classes } from '@blueprintjs/core';
import cx from 'classnames';
import { styled } from '@mui/material/styles';
import * as R from 'ramda';
import { FormattedMessage } from '../../modules/i18n';
import { SUCCESS, ERROR, WARNING } from '../../modules/common/constants';
import messages from '../messages';

const MyLog = styled('div')({
  marginBottom: 10,
});

const Log = ({ type, statusKey, message, error, scope, space }) => {
  let iconClass, intentClass, messageKey;
  let fallback = null;
  switch (type) {
    case SUCCESS:
      iconClass = Classes.iconClass('tick-circle');
      intentClass = Classes.INTENT_SUCCESS;
      messageKey = `upload.${scope}.success`;
      break;
    case WARNING:
      iconClass = Classes.iconClass('warning-sign');
      intentClass = Classes.INTENT_WARNING;
      messageKey = `upload.${scope}.warning`;
      fallback = `upload.unknown`;
      break;
    case ERROR:
      iconClass = Classes.iconClass('error');
      intentClass = Classes.INTENT_DANGER;
      messageKey = `upload.${scope}.error`;
      fallback = `upload.unknown`;
      break;
    default:
      break;
  }
  let enhancedMessage = message;
  if (R.has(statusKey, messages)) {
    enhancedMessage = <FormattedMessage {...messages[statusKey]} />;
  }

  return (
    <MyLog>
      <div className={cx(Classes.CALLOUT, intentClass, iconClass)}>
        <h5>
          <FormattedMessage
            {...messages[messageKey]}
            values={{ label: space.label }}
          />
        </h5>
        {(R.isNil(enhancedMessage) || R.isEmpty(enhancedMessage)) &&
        fallback ? (
          <FormattedMessage
            id="upload.unknown"
            values={{ label: space.label }}
          />
        ) : null}
        {R.is(Array, enhancedMessage) ? (
          R.addIndex(R.map)(
            (m, i) => <p key={`upload-log-${i}`}>{m}</p>,
            enhancedMessage,
          )
        ) : (
          <p>{enhancedMessage}</p>
        )}
        {error ? <p>{error.message}</p> : null}
      </div>
    </MyLog>
  );
};

Log.propTypes = {
  type: PropTypes.string.isRequired,
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  scope: PropTypes.string,
  space: PropTypes.shape({ label: PropTypes.string }),
  error: PropTypes.shape({ message: PropTypes.string }),
};

export default Log;
