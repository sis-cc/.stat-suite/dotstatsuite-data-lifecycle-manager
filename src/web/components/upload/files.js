import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Tag, Colors, Button } from '@blueprintjs/core';
import cx from 'classnames';
import Dropzone from 'react-dropzone';
import { has, isEmpty, map } from 'ramda';
import numeral from 'numeral';
import { styled } from '@mui/material/styles';
import { FormattedMessage } from '../../modules/i18n';
import Path from './path';

const FileStyled = styled(Tag)({
  margin: 5,
});

const Span = styled('span')({
  color: 'red',
  fontWeight: 'bold',
  marginLeft: 5,
});

const File = ({ file, fileRemove, intentClass, maxSize }) => {
  const onRemoveHandler = event => {
    event.preventDefault();
    event.stopPropagation();
    fileRemove(file);
  };

  return (
    <FileStyled
      className={cx(Classes.MINIMAL, Classes.LARGE, intentClass)}
      onRemove={onRemoveHandler}
    >
      <FormattedMessage
        id="file"
        values={{ name: file.name, size: numeral(file.size).format('0.00 b') }}
      />
      {intentClass === Classes.INTENT_DANGER && file.size > maxSize ? (
        <Span>
          <FormattedMessage
            id="upload.exceed.oversize.limit"
            values={{ size: numeral(maxSize).format('0.00 b') }}
          />
        </Span>
      ) : null}
    </FileStyled>
  );
};

File.propTypes = {
  file: PropTypes.object.isRequired,
  fileRemove: PropTypes.func.isRequired,
  intentClass: PropTypes.string,
  maxSize: PropTypes.number.isRequired,
};

const IntentedFiles = ({
  files,
  fileRemove,
  acceptedFiles,
  rejectedFiles,
  maxSize,
}) => {
  if (isEmpty(files)) return null;
  const getIntentClass = file => {
    if (has(file.name, acceptedFiles)) return Classes.INTENT_SUCCESS;
    if (has(file.name, rejectedFiles)) return Classes.INTENT_DANGER;
    return Classes.INTENT_PRIMARY;
  };

  return (
    <div>
      {map(
        (file, index) => (
          <File
            key={`${file.name}:${index}`}
            file={file}
            fileRemove={fileRemove}
            intentClass={getIntentClass(file)}
            maxSize={maxSize}
          />
        ),
        files,
      )}
    </div>
  );
};

IntentedFiles.propTypes = {
  acceptedFiles: PropTypes.object,
  rejectedFiles: PropTypes.object,
  files: PropTypes.array,
  fileRemove: PropTypes.func.isRequired,
  maxSize: PropTypes.number,
};

const NoFile = () => {
  return (
    <FileStyled
      className={cx(Classes.MINIMAL, Classes.LARGE, Classes.INTENT_WARNING)}
    >
      <FormattedMessage id="files.no.file" />
    </FileStyled>
  );
};

// glamorous and dropzone+ref are not compliant
const dropzoneStyle = {
  width: '100%',
  minHeight: 100,
  border: `1px dashed ${Colors.LIGHT_GRAY1}`,
  borderRadius: 5,
  cursor: 'pointer',
};

const FilesStyled = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const Files = ({
  dropzoneConfig,
  filesChange,
  fileRemove,
  files,
  acceptedFiles,
  rejectedFiles,
  hasNoFile,
  maxSize,
  hasPathOption,
}) => {
  let dropzoneRef; // mon dieu que c'est moche.
  return (
    <FilesStyled>
      <Button
        className={Classes.MINIMAL}
        icon="add"
        onClick={() => {
          dropzoneRef.open();
        }}
      >
        <FormattedMessage
          id="upload.files.add"
          values={{ size: numeral(maxSize).format('0.00 b') }}
        />
      </Button>
      <p className={Classes.TEXT_MUTED}>
        <em>
          <FormattedMessage id="upload.files.add.help" />
        </em>
      </p>
      <Dropzone
        {...dropzoneConfig}
        onDrop={files => filesChange(files)}
        ref={node => {
          dropzoneRef = node;
        }}
        style={dropzoneStyle}
      >
        {hasNoFile ? <NoFile /> : null}
        <IntentedFiles
          files={files}
          acceptedFiles={acceptedFiles}
          rejectedFiles={rejectedFiles}
          fileRemove={fileRemove}
          maxSize={maxSize}
        />
      </Dropzone>
      {hasPathOption ? <Path /> : null}
    </FilesStyled>
  );
};

Files.propTypes = {
  filesChange: PropTypes.func.isRequired,
  fileRemove: PropTypes.func.isRequired,
  files: PropTypes.array,
  acceptedFiles: PropTypes.object,
  rejectedFiles: PropTypes.object,
  hasNoFile: PropTypes.bool,
  maxSize: PropTypes.number,
  dropzoneConfig: PropTypes.shape({
    accept: PropTypes.string,
    multiple: PropTypes.bool,
    minSize: PropTypes.number,
    maxSize: PropTypes.number,
  }).isRequired,
  hasPathOption: PropTypes.bool,
};

export default Files;
