import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Popover, Position, Button } from '@blueprintjs/core';
import { injectIntl } from 'react-intl';
import { withState, withHandlers, compose, withProps } from 'recompose';
import {
  ascend,
  descend,
  filter,
  head,
  sortBy,
  sortWith,
  map,
  prop,
} from 'ramda';
import { styled } from '@mui/material/styles';
import classnames from 'classnames';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';

const [ASC, DESC] = ['asc', 'desc'];

const Wrapper = styled('div')({ padding: 10 });

export const iconFactory = (field, isField) => {
  if (!isField || !field.isSelected) return 'blank';

  switch (field.direction) {
    case ASC:
      return 'sort-alphabetical';
    case DESC:
      return 'sort-alphabetical-desc';
    default:
      return 'blank';
  }
};

export const Sort = injectIntl(({ field, fields, fieldChange, intl }) => {
  return (
    <Popover
      position={Position.BOTTOM}
      target={
        <Button
          className={Classes.MINIMAL}
          text={formatMessage(intl)(messages[`artefact.${field.id}`])}
          icon={iconFactory(field, true)}
        />
      }
      content={
        <Wrapper
          className={classnames(
            Classes.BUTTON_GROUP,
            Classes.MINIMAL,
            Classes.VERTICAL,
            Classes.ALIGN_LEFT,
          )}
        >
          {map(
            _field => (
              <Button
                key={_field.id}
                className={Classes.MINIMAL}
                text={formatMessage(intl)(messages[`artefact.${_field.id}`])}
                icon={iconFactory(_field, field.id === _field.id)}
                onClick={fieldChange(_field)}
                active={field.id === _field.id && _field.isSelected}
              />
            ),
            fields,
          )}
        </Wrapper>
      }
    />
  );
});

Sort.proptypes = {
  field: PropTypes.objectOf(
    PropTypes.shape({
      id: PropTypes.string,
      direction: PropTypes.oneOf([ASC, DESC]),
      isSelected: PropTypes.bool,
    }),
  ),
  fields: PropTypes.object,
  fieldChange: PropTypes.func,
};

// isSelected is useful for the UI
// for the sort, we use several casdading and the index is the relavant attr to select fields
export const getSortFields = fields =>
  sortBy(
    prop('index'),
    filter(field => Number.isFinite(field.index), fields),
  );
export const getSortDirection = (field, _field) => {
  if (_field.id !== field.id) return;

  switch (
    field.direction // revert
  ) {
    case ASC:
      return DESC;
    case DESC:
      return ASC;
    default:
      return ASC;
  }
};

export const setListHandler = sort => list => {
  const sortFields = getSortFields(sort.fields);

  return {
    ...list,
    items: sortWith(
      map(sF => {
        const dir = sF.direction === 'asc' ? ascend : descend;
        return dir(prop(sF.accessor));
      }, sortFields),
      list.items,
    ),
  };
};

export const withSort = fields =>
  compose(
    withState('sort', 'setSort', { fields: [...fields] }),
    // sorts can be combined, ie sort by type then by name, only the selected sort is displayed
    withProps(state => ({
      list: setListHandler(state.sort)(state.list),
      sort: { ...state.sort, field: head(getSortFields(state.sort.fields)) },
    })),
    withHandlers({
      sortFieldChange: ({ setSort }) => field => event => {
        event.preventDefault();
        setSort(state => ({
          ...state,
          fields: map(
            _field => ({
              ..._field,
              direction: getSortDirection(field, _field),
              isSelected: _field.id === field.id,
              index:
                _field.id === field.id
                  ? 0
                  : _field.index === 0
                  ? null
                  : _field.index,
            }),
            fields,
          ),
        }));
      },
    }),
  );
