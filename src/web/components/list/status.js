import React from 'react';
import PropTypes from 'prop-types';
import { compose, withProps } from 'recompose';
import { Classes, Tag, Spinner } from '@blueprintjs/core';
import { length } from 'ramda';
import { FormattedMessage } from '../../modules/i18n';
import classnames from 'classnames';

export const Status = ({ current, total, isFetching, messageKey = 'artefacts.status' } = {}) => {
  if (isFetching) return <Spinner className={Classes.SMALL} />;

  const label = current ? (
    <FormattedMessage id={messageKey} values={{ current, total }} />
  ) : (
    <FormattedMessage id="artefacts.no.artefact" />
  );

  return <Tag className={classnames(Classes.MINIMAL, Classes.LARGE)}>{label}</Tag>;
};

Status.propTypes = {
  current: PropTypes.number,
  total: PropTypes.number,
  isFetching: PropTypes.bool,
  messageKey: PropTypes.string,
};

export const withStatus = compose(
  withProps(state => ({
    status: {
      current: length(state.list.items),
      total: length(state.list._items),
    },
  })),
);
