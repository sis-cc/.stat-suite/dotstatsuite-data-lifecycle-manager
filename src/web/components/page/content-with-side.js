import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';

export const Side = ({ children }) => children;
export const Content = ({ children }) => children;

const ContentWithSide = ({ children }) => {
  const side = () =>
    React.Children.toArray(children).find(child => child.type === Side);
  const content = () =>
    React.Children.toArray(children).find(child => child.type === Content);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={3}>
        {side()}
      </Grid>
      <Grid item xs={12} sm={9}>
        {content()}
      </Grid>
    </Grid>
  );
};

Side.propTypes = {
  children: PropTypes.node,
};

Content.propTypes = {
  children: PropTypes.node,
};

ContentWithSide.propTypes = {
  children: PropTypes.node,
};

export default ContentWithSide;
