import React, { useMemo, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import * as R from 'ramda';
import Filter from './filter';
import { Spotlight } from '../list';
import { spotlightHandler } from '../list/spotlight';
import { FormattedMessage } from '../../modules/i18n';

import useAgencies from '../../hooks/useAgencies';
import { SimpleTreeView } from '@mui/x-tree-view/SimpleTreeView';
import { TreeItem } from '@mui/x-tree-view/TreeItem';
import PropTypes from 'prop-types';
import { agencyToggle } from '../../modules/filters/action-creators';
import { useSpotlight } from '../list/spotlight';
import Tooltip from '@mui/material/Tooltip';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(() => ({
  container: {
    borderTop: '1px dashed #ccc',
  },
  treeWrapper: {
    maxHeight: 250,
    overflow: 'auto',
    '& .MuiTreeItem-root': {
      minHeight: 34,
    },
  },
  spotlightWrapper: {
    paddingTop: 10,
    paddingBottom: 10,
  },
}));

const Agency = ({ agency }) => {
  const getTooltipLabel = agency => (
    <Tooltip title={`(${agency.code}) ${agency.name}`}>
      {`(${agency.code}) ${agency.name}`}
    </Tooltip>
  );
  return (
    <TreeItem itemId={agency.id} label={getTooltipLabel(agency)}>
      {R.map(
        child => (
          <Agency key={child.id} agency={child} />
        ),
        agency.children || [],
      )}
    </TreeItem>
  );
};

Agency.propTypes = {
  agency: PropTypes.object.isRequired,
};

const Agencies = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { agencies, isLoading } = useAgencies();
  const [expandedItems, setExpandedItems] = useState([]);
  const flattenTree = R.chain(node => [
    R.dissoc('children', node),
    ...flattenTree(node.children || []),
  ]);
  const indexedFlattened = R.pipe(
    flattenTree,
    R.indexBy(R.prop('id')),
  )(agencies);
  const {
    spotlight,
    spotlightValueChange,
    spotlightFieldChange,
  } = useSpotlight([
    { id: 'label', accessor: 'name', isSelected: true },
    { id: 'code', accessor: 'code', isSelected: true },
  ]);

  const { filtered, expanded } = useMemo(() => {
    const expanded = [];
    const recurse = list =>
      R.reduce(
        (acc, agency) => {
          let res = agency;
          const nodeIsValid = spotlightHandler(spotlight)(agency);
          const children = recurse(agency.children || []);
          const childrenAreValid =
            R.is(Array, children) && !R.isEmpty(children);
          const isExpanded =
            (!R.isEmpty(spotlight.value) && childrenAreValid) ||
            agency.isExpanded;
          if (!R.isEmpty(children)) {
            res = R.pipe(
              R.assoc('children', children),
              R.assoc('isExpanded', isExpanded),
            )(res);
          } else {
            res = R.pipe(R.dissoc('children'))(res);
          }
          if (!R.isEmpty(spotlight.value) && childrenAreValid) {
            expanded.push(agency.id);
          }
          return R.or(nodeIsValid, childrenAreValid) ? R.append(res, acc) : acc;
        },
        [],
        list,
      );
    const filtered = recurse(agencies);
    return { filtered, expanded };
  }, [agencies, spotlight]);

  useEffect(() => {
    if (!R.isEmpty(expanded)) {
      setExpandedItems(expanded);
    }
  }, [expanded]);

  const handleExpandedItemsChange = (_, ids) => {
    setExpandedItems(ids);
  };

  const onClick = (_, itemId) => {
    dispatch(agencyToggle(R.prop(itemId, indexedFlattened)));
  };

  return (
    <Filter
      isBlank={R.isEmpty(agencies)}
      isOpen={true}
      isFetching={isLoading}
      title={<FormattedMessage id="artefacts.filters.agencies.title" />}
    >
      {
        <div className={classes.container}>
          <div className={classes.spotlightWrapper}>
            <Spotlight
              {...spotlight}
              fieldChange={spotlightFieldChange}
              valueChange={spotlightValueChange}
            />
          </div>
          <div className={classes.treeWrapper}>
            <SimpleTreeView
              onItemSelectionToggle={onClick}
              expandedItems={expandedItems}
              onExpandedItemsChange={handleExpandedItemsChange}
            >
              {R.map(
                agency => (
                  <Agency key={agency.id} agency={agency} />
                ),
                filtered,
              )}
            </SimpleTreeView>
          </div>
        </div>
      }
    </Filter>
  );
};

export default Agencies;
