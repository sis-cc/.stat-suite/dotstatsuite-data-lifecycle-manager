import React from 'react';
import * as R from 'ramda';
import { Position, Tooltip } from '@blueprintjs/core';
import Filter from './filter';
import Space from '../common/space';
import { withCategories, withSpacedCategories } from '../../modules/categories';
import { withFilters } from '../../modules/filters';
import { Tree } from '@blueprintjs/core';
import { styled } from '@mui/material/styles';
import { onlyUpdateForKeys, renameProps, compose, withProps } from 'recompose';
import { withList } from '../list';
import { FormattedMessage } from '../../modules/i18n';

const FilterWrapper = styled('div')({
  maxHeight: 250,
  overflow: 'auto',
});

const CategoriesTreeWrapper = styled('div')({
  borderTop: '1px dashed #ccc',
  paddingBottom: 10,
  paddingTop: 10,
  '& .pt-tree-node-content': {
    minHeight: 34,
  },
});

const addToolTip = categories => {
  return R.map(category => ({
    ...category,
    label: (
      <Tooltip
        content={`(${category.code}) ${category.label}`}
        position={Position.RIGHT}
      >
        {`(${category.code}) ${category.label}`}
      </Tooltip>
    ),
    childNodes: category.childNodes && addToolTip(category.childNodes),
  }))(categories);
};

const CategoriesTree = props => (
  <CategoriesTreeWrapper>
    <Space {...R.propOr({}, 'space', props)} />
    <Tree {...props} contents={addToolTip(props.contents)} />
  </CategoriesTreeWrapper>
);

const spacedCategoriesTree = spaceId =>
  compose(
    withSpacedCategories(spaceId),
    renameProps({ categories: 'items' }),
    withList({ isTree: true }),
    withProps(({ items }) => ({ isBlank: R.isEmpty(items) })),
    renameProps({
      categoryToggle: 'onNodeClick',
      items: 'contents',
    }),
    withProps(({ categoryExpandToggle }) => ({
      onNodeExpand: categoryExpandToggle,
      onNodeCollapse: categoryExpandToggle,
    })),
  )(CategoriesTree);

export const Categories = props => {
  return (
    <Filter
      {...props}
      isOpen
      title={<FormattedMessage id="artefacts.filters.categories.title" />}
    >
      {() => (
        <FilterWrapper>
          {R.pipe(
            R.mapObjIndexed((space, spaceId) => {
              const SpacedCategoriesTree = spacedCategoriesTree(spaceId);
              return (
                <SpacedCategoriesTree key={spaceId} {...props} space={space} />
              );
            }),
            R.values,
          )(props.selectedSpaces)}
        </FilterWrapper>
      )}
    </Filter>
  );
};

export default compose(
  withFilters,
  withCategories,
  onlyUpdateForKeys(['isFetching', 'categoryToggle', 'categoryClear']),
  renameProps({ categoryClear: 'clearHandler' }),
)(Categories);
