import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Filter } from './filter';
import { RadioGroup, Radio, Checkbox } from '@blueprintjs/core';
import { withFilters } from '../../modules/filters';
import { onlyUpdateForKeys, compose } from 'recompose';
import { FormattedMessage } from '../../modules/i18n';
import { styled } from '@mui/material/styles';
import messages from '../messages';

const CheckboxWrapper = styled('div')({
  paddingTop: 10,
  borderTop: '1px dashed #ccc',
});

export const Version = props => {
  return (
    <Filter
      {...props}
      isBlank={false}
      title={<FormattedMessage id="artefacts.filters.version.title" />}
    >
      {() => (
        <div>
          <RadioGroup
            onChange={e => props.changeVersion(e.target.value)}
            selectedValue={props.version}
          >
            {R.map(
              v => (
                <Radio
                  key={v}
                  label={
                    <FormattedMessage
                      {...R.propOr(
                        { id: `artefacts.filters.version.${v}` },
                        `artefacts.filters.version.${v}`,
                        messages,
                      )}
                    />
                  }
                  value={v}
                />
              ),
              R.values(props.versions),
            )}
          </RadioGroup>
          <CheckboxWrapper>
            <Checkbox
              checked={props.isFinal}
              label={<FormattedMessage id="filter.final" />}
              onChange={() => props.toggleIsFinal()}
            />
          </CheckboxWrapper>
        </div>
      )}
    </Filter>
  );
};

Version.propTypes = {
  version: PropTypes.string.isRequired,
  changeVersion: PropTypes.func.isRequired,
  isFinal: PropTypes.bool,
  toggleIsFinal: PropTypes.func.isRequired,
};

export default compose(
  withFilters,
  onlyUpdateForKeys([
    'versions',
    'version',
    'changeVersion',
    'isFinal',
    'toggleIsFinal',
  ]),
)(Version);
