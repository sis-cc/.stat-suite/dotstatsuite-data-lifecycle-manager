import React from 'react';
import spaces from './spaces';
import Agencies from './agencies';
import Categories from './categories';
import Types from './types';
import Version from './version';
import { FormattedMessage } from '../../modules/i18n';

const Filters = () => {
  const InternalSpaces = spaces('internalSpaces');
  const ExternalSpaces = spaces('externalSpaces');
  return (
    <div className="filters">
      <InternalSpaces title={<FormattedMessage id="artefacts.filters.spaces.title" />} />
      <ExternalSpaces title={<FormattedMessage id="artefacts.filters.external.spaces.title" />} />
      <Types />
      <Categories />
      <Agencies />
      <Version />
    </div>
  );
};

Filters.propTypes = {};

export default Filters;
