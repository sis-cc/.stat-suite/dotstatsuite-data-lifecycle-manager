import React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import Filter from './filter';
import { List, Spotlight } from '../list';

const useStyles = makeStyles(theme => ({
  spotlight: {
    paddingBottom: theme.spacing(1),
  },
  list: {
    maxHeight: 250,
    overflowY: 'auto',
    overflowWrap: 'anywhere',
  },
}));

const SpotlightList = props => {
  const classes = useStyles();

  return (
    <Filter {...props}>
      <div>
        <div className={classes.spotlight}>
          <Spotlight
            {...props.spotlight}
            fieldChange={props.spotlightFieldChange}
            valueChange={props.spotlightValueChange}
          />
        </div>
        <div className={classes.list}>
          <List {...props} items={props.list.items} />
        </div>
      </div>
    </Filter>
  );
};

export default SpotlightList;
