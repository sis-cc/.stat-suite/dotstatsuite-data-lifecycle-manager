import React from 'react';
import PropTypes from 'prop-types';
import Filter from './filter';
import Item from './item';
import Space from '../common/space';
import { withList, List } from '../list';
import { withConfig } from '../../modules/config';
import { withFilters } from '../../modules/filters';
import { onlyUpdateForKeys, renameProps, compose, withProps } from 'recompose';
import { isEmpty, pick, propOr } from 'ramda';
import { styled } from '@mui/material/styles';

const ListWrapper = styled('div')({
  maxHeight: 250,
  overflow: 'auto',
  marginRight: 6, // align with header right actions
});

export const SpaceItem = props => {
  return (
    <Item {...props}>
      <Space {...pick(['label', 'color', 'backgroundColor'], props)} />
    </Item>
  );
};

SpaceItem.propTypes = {
  label: PropTypes.string,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
};

export const Spaces = props => {
  return (
    <Filter {...props}>
      {() => (
        <ListWrapper>
          <List {...props} itemRenderer={SpaceItem} />
        </ListWrapper>
      )}
    </Filter>
  );
};

export default spacesScope =>
  compose(
    withConfig,
    withFilters,
    onlyUpdateForKeys(['isFetching', 'spaceToggle', 'spaceClear', spacesScope]),
    renameProps({ spaceToggle: 'onChangeHandler', spaceClear: 'clearHandler' }),
    withProps(({ onChangeHandler, ...rest }) => {
      const items = propOr([], spacesScope, rest);
      return {
        handlers: { onChangeHandler },
        isBlank: isEmpty(items),
        isOpen: true,
        items,
      };
    }),
    withList(),
  )(Spaces);
