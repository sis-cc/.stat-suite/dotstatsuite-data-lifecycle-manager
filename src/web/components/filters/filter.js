import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { styled } from '@mui/material/styles';
import { Spinner, Classes, Collapse, Button } from '@blueprintjs/core';
import { FormattedMessage } from '../../modules/i18n';
import {
  compose,
  branch,
  renderComponent,
  withState,
  withHandlers,
} from 'recompose';
import classnames from 'classnames';

export const StyledFilter = styled('div')({
  borderTop: '2px solid #999',
  marginBottom: 20,
});

export const StyledHeaderFilter = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
  paddingBottom: 5,
  alignItems: 'center',
  minHeight: 35,
  '& h6': {
    color: '#666',
    margin: 0,
  },
});

export const FilterSpinner = () => {
  return (
    <div>
      <Spinner className={Classes.SMALL} />
    </div>
  );
};

export const FilterBlank = () => {
  return (
    <div>
      <em>
        <FormattedMessage id="filters.no.filter" />
      </em>
    </div>
  );
};

export const FilterHeader = ({
  title,
  isOpen,
  collapseChange,
  clearHandler,
}) => {
  const collapseButton = collapseChange ? (
    <Button
      className={Classes.MINIMAL}
      icon={classnames({ 'chevron-up': isOpen, 'chevron-down': !isOpen })}
      onClick={collapseChange}
    />
  ) : null;

  const clearAllButton =
    clearHandler && isOpen ? (
      <Button
        className={Classes.MINIMAL}
        icon="eraser"
        onClick={clearHandler}
      />
    ) : null;

  return (
    <StyledHeaderFilter>
      <div>
        <h6>{title}</h6>
      </div>
      <div>
        {clearAllButton}
        {collapseButton}
      </div>
    </StyledHeaderFilter>
  );
};

FilterHeader.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  isOpen: PropTypes.bool,
  collapseChange: PropTypes.func,
  clearHandler: PropTypes.func,
};

const CollapseFilterBody = ({ children, isOpen, collapseChange }) => {
  return (
    <Collapse isOpen={collapseChange ? isOpen : true}>
      {R.is(Function, children) ? children() : children}
    </Collapse>
  );
};

CollapseFilterBody.propTypes = {
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  isOpen: PropTypes.bool,
  collapseChange: PropTypes.func,
};

export const FilterBody = compose(
  branch(({ isFetching }) => isFetching, renderComponent(FilterSpinner)),
  branch(({ isBlank }) => isBlank, renderComponent(FilterBlank)),
)(CollapseFilterBody);

export const Filter = props => {
  return (
    <StyledFilter style={{ opacity: props.disabled ? 0.4 : 1 }}>
      <FilterHeader
        title={props.title}
        isOpen={props.isOpen}
        collapseChange={props.collapseChange}
        clearHandler={props.clearHandler}
      />
      <FilterBody
        isFetching={props.isFetching}
        isBlank={props.isBlank || props.disabled}
        isOpen={props.isOpen}
        collapseChange={props.collapseChange}
      >
        {props.children}
      </FilterBody>
    </StyledFilter>
  );
};

Filter.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  isOpen: PropTypes.bool,
  isBlank: PropTypes.bool,
  isFetching: PropTypes.bool,
  collapseChange: PropTypes.func,
  clearHandler: PropTypes.func,
  disabled: PropTypes.bool,
};

export default compose(
  withState('isOpen', 'setOpen', props => !!props.isOpen),
  withHandlers({
    collapseChange: ({ setOpen }) => event => {
      event.preventDefault();
      setOpen(isOpen => !isOpen);
    },
  }),
)(Filter);
