import React from 'react';
import * as R from 'ramda';
import { FormattedMessage } from '../../../modules/i18n';
import { useSelector } from 'react-redux';
import { changeStructureId } from '../../../modules/logs/action-creators';
import { getRequestId, getStructureId } from '../../../modules/logs/selectors';
import SpotlightExclusive from './factory/spotlightExclusive';

export const StructureFilter = () => {
  const structureId = useSelector(getStructureId);
  const requestId = useSelector(getRequestId);

  return (
    <SpotlightExclusive
      title={<FormattedMessage id="logs.filters.structure.title" />}
      isSelectedHandler={R.equals(structureId)}
      currentIdHandler={changeStructureId}
      idAccessor={R.prop('artefact')}
      disabled={!!requestId}
    />
  );
};

export default StructureFilter;
