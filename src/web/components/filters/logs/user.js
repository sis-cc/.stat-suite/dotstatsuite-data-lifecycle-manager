import React from 'react';
import * as R from 'ramda';
import { FormattedMessage } from '../../../modules/i18n';
import { useSelector } from 'react-redux';
import SpotlightExclusive from './factory/spotlightExclusive';
import {
  getRequestId,
  getSelectedUserEmails,
} from '../../../modules/logs/selectors';
import { toggleUserEmail } from '../../../modules/logs/action-creators';
import CheckBoxItem from '../item';

export const UserFilter = () => {
  const selectedUserEmails = useSelector(getSelectedUserEmails);
  const requestId = useSelector(getRequestId);

  return (
    <SpotlightExclusive
      title={<FormattedMessage id="logs.filters.user.title" />}
      currentIdHandler={toggleUserEmail}
      isSelectedHandler={id => selectedUserEmails.has(id)}
      idAccessor={R.prop('userEmail')}
      disabled={!!requestId}
      itemRenderer={CheckBoxItem}
    />
  );
};

export default UserFilter;
