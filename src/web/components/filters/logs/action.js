import React from 'react';
import * as R from 'ramda';
import { Checkbox, FormControlLabel } from '@mui/material';
import { FormattedMessage } from '../../../modules/i18n';
import Filter from '../filter';
import { useDispatch, useSelector } from 'react-redux';
import {
  getRequestId,
  getSelectedActionIds,
} from '../../../modules/logs/selectors';
import { toggleActionId } from '../../../modules/logs/action-creators';
import useTransferLogs from '../../../hooks/useTransferLogs';
import { useMemo } from 'react';

const ActionFilter = () => {
  const dispatch = useDispatch();
  const requestId = useSelector(getRequestId);
  const selectedActionIds = useSelector(getSelectedActionIds);
  const idAccessor = R.prop('action');
  const { logs } = useTransferLogs();

  const actionIds = useMemo(() => {
    return R.pipe(
      R.uniqBy(idAccessor),
      R.reject(R.pipe(idAccessor, R.isNil)),
      R.pluck('action'),
    )(logs);
  }, [logs]);

  const changeHandler = id => {
    dispatch(toggleActionId(id));
  };

  return (
    <Filter
      title={<FormattedMessage id="logs.filters.action.title" />}
      isOpen
      disabled={!!requestId}
    >
      {R.map(
        id => (
          <FormControlLabel
            key={id}
            style={{ width: '90%' }} // keep it simple with MUI, not 100% to pesky scrollbar while collapsing
            control={
              <Checkbox
                size="small"
                checked={selectedActionIds.has(id)}
                onChange={() => changeHandler(id)}
                color="primary"
              />
            }
            label={id}
          />
        ),
        actionIds,
      )}
    </Filter>
  );
};

export default ActionFilter;
