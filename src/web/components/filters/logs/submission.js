import React, { useEffect, useState } from 'react';
import * as R from 'ramda';
import { FormattedMessage } from '../../../modules/i18n';
import { useDispatch, useSelector } from 'react-redux';
import { changeSubmissionPeriod } from '../../../modules/logs/action-creators';
import {
  getRequestId,
  getSubmissionPeriod,
} from '../../../modules/logs/selectors';
import { PeriodPicker } from '@sis-cc/dotstatsuite-visions';
import Filter from '../filter';
import { isValid, parseISO, subDays, subHours } from 'date-fns';
import { Button } from '@mui/material';

const fallbackDate = fallback =>
  R.ifElse(date => isValid(date), R.identity, R.always(fallback));

const dateFunctor = today => (fallback, functor) =>
  R.pipe(functor(today, fallback), fallbackDate(fallback));

const endDateFunctor = (today, fallback) =>
  R.cond([
    [R.isNil, R.always(fallback)],
    [R.equals('now'), R.always(today)],
    [R.T, end => parseISO(end)],
  ]);

const startDateFunctor = (today, fallback) =>
  R.cond([
    [R.isNil, R.always(fallback)],
    [R.equals('now'), R.always(today)],
    [R.T, start => parseISO(start)],
  ]);

const getTimeExtents = today => {
  const { start, end } = R.defaultTo(
    {},
    window?.SETTINGS?.logs?.transfer?.timeExtents,
  );

  const dateFunctorFixed = dateFunctor(today);
  const endDate = dateFunctorFixed(today, endDateFunctor)(end);
  const startDate = dateFunctorFixed(
    subDays(today, 30),
    startDateFunctor,
  )(start);

  return [startDate, endDate];
};

export const SubmissionFilter = () => {
  const dispatch = useDispatch();
  const requestId = useSelector(getRequestId);
  const submissionPeriod = useSelector(getSubmissionPeriod);
  const today = new Date();
  const boundaries = getTimeExtents(today);

  const [localPeriod, setLocalPeriod] = useState();
  useEffect(() => {
    setLocalPeriod(R.defaultTo([subHours(today, 24), today], submissionPeriod));
  }, [submissionPeriod]);

  const changeHandler = period => {
    dispatch(changeSubmissionPeriod(period));
  };

  return (
    <Filter
      title={<FormattedMessage id="logs.filters.submissionTime.title" />}
      clearHandler={() => changeHandler()}
      disabled={!!requestId}
      isOpen
    >
      <PeriodPicker
        defaultFrequency="N"
        availableFrequencies={['N']}
        boundaries={boundaries}
        period={localPeriod}
        changePeriod={setLocalPeriod}
        labels={{
          year: <FormattedMessage id="de.period.year" />,
          month: <FormattedMessage id="de.period.month" />,
          hour: <FormattedMessage id="de.period.hour" />,
          minute: <FormattedMessage id="de.period.minute" />,
          day: <FormattedMessage id="de.period.day" />,
          end: <FormattedMessage id="de.period.end" />,
          start: <FormattedMessage id="de.period.start" />,
        }}
        frequencyDisabled={true}
        periodDisabled={false}
      />
      <Button
        color="secondary"
        size="small"
        variant="contained"
        onClick={() => changeHandler(localPeriod)}
      >
        <FormattedMessage id="logs.filters.submissionTime.apply" />
      </Button>
    </Filter>
  );
};

export default SubmissionFilter;
