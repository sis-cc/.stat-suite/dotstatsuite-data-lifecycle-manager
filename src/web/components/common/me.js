import React from 'react';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { Button, Popover, Position, NonIdealState } from '@blueprintjs/core';
import { styled } from '@mui/material/styles';
import PersonIcon from '@mui/icons-material/Person';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';
import { useOidc } from '../../lib/oidc';
import { getUser } from '../../modules/oidc/selectors';
import { useSelector } from 'react-redux';
import { Button as ButtonVisions } from '@sis-cc/dotstatsuite-visions';

const NonIdealStateStyled = styled(NonIdealState)({
  padding: 15,
});

const ButtonStyled = styled(ButtonVisions)({
  color: 'white !important',
  '::before': {
    color: 'white !important',
  },
  fontSize: 12,
});

const Me = () => {
  const intl = useIntl();
  const auth = useOidc();
  const user = useSelector(getUser);
  const history = useHistory();

  if (!user) {
    return (
      <ButtonStyled startIcon={<PersonIcon />}>
        {formatMessage(intl)(messages['users.none'])}
      </ButtonStyled>
    );
  }

  const handleMyPermissionsClick = () => {
    history.push('my-rights');
  };

  const userName = `${user.given_name} ${user.family_name}`;
  return (
    <Popover
      target={
        <ButtonStyled startIcon={<PersonIcon />}>{userName}</ButtonStyled>
      }
      content={
        <NonIdealStateStyled
          description={
            <div>
              <p>{user.email}</p>
              <Button icon="shield" onClick={handleMyPermissionsClick}>
                <span>{formatMessage(intl)(messages['route.my.rights'])}</span>
              </Button>
              <Button icon="log-out" onClick={() => auth.signOut()}>
                <span>{formatMessage(intl)(messages['logout'])}</span>
              </Button>
            </div>
          }
        />
      }
      position={Position.BOTTOM}
      inheritDarkTheme={false}
    />
  );
};

export default Me;
