/* eslint react/prop-types: 0 */
import * as R from 'ramda';
import React, { useState } from 'react';

const model = {
  contentType: 'both',
  date: null,
  sourceVersion: 'live',
  targetVersion: 'live',
  validationType: R.pathOr(
    'basic',
    ['app', 'defaultOptionDataValidation'],
    window.SETTINGS,
  ),
  withRestoration: false,
};

export const withDataValidation = Component => props => {
  const [state, setState] = useState(model);

  const resetValidationOptions = () => setState(model);
  const onChangeDate = date => setState({ ...state, date });
  const onChangeContentType = type => setState({ ...state, contentType: type });
  const onChangeValidationType = type =>
    setState({ ...state, validationType: type });
  const onChangeSourceVersion = version =>
    setState({ ...state, sourceVersion: version });
  const onChangeTargetVersion = version =>
    setState({ ...state, targetVersion: version });
  const onChangeWithRestoration = withRestoration =>
    setState({ ...state, withRestoration });

  return (
    <Component
      {...props}
      validation={{
        ...state,
        onChangeDate,
        onChangeContentType,
        onChangeValidationType,
        onChangeSourceVersion,
        onChangeTargetVersion,
        onChangeWithRestoration,
        resetValidationOptions,
      }}
    />
  );
};
