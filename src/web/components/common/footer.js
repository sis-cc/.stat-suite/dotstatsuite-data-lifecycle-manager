import React from 'react';
import { FormattedMessage } from '../../modules/i18n';
import { path } from 'ramda';
import { styled } from '@mui/material/styles';

const StyledFooter = styled('nav')({
  display: 'flex',
  justifyContent: 'space-between',
  padding: 15,
  borderTop: '1px solid #ccc',
  backgroundColor: '#f0f0f0',
  color: '#494444',
  height: 85,
  '& img': {
    maxHeight: 20,
    margin: '0 5px',
  },
  '& a': {
    margin: '0 5px',
  },
  // sticky footer -> https://philipwalton.com/articles/normalizing-cross-browser-flexbox-bugs/
  flexShrink: 0,
});

const IconAndText = styled('div')({
  display: 'flex',
  fontWeight: 'bold',
});

const Img = styled('img')({
  maxHeight: 20,
  verticalAlign: 'middle',
});

const Footer = () => {
  return (
    <StyledFooter>
      <IconAndText>
        <FormattedMessage
          id="dlm.app.footer.description"
          values={{
            icon: (
              <Img src={path(['assets', 'icon'], window.SETTINGS)} alt="icon" />
            ),
            link: (
              <a href={path(['app', 'footer', 'link'], window.SETTINGS)}>
                <FormattedMessage id="dlm.app.footer.author" />
              </a>
            ),
          }}
        />
      </IconAndText>
      <div className="pt-rtl">
        <FormattedMessage
          id="dlm.app.footer.disclaimer"
          values={{ br: <br /> }}
        />
      </div>
    </StyledFooter>
  );
};

Footer.propTypes = {};

export default Footer;
