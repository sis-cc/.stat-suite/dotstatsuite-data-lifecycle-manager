import React from 'react';
import { has, isEmpty, map, prop } from 'ramda';
import { Classes, Spinner } from '@blueprintjs/core';
import { withList, List } from '../list';
import { withConfig } from '../../modules/config';
import { SpaceItem } from '../filters/spaces';
import { FormattedMessage } from '../../modules/i18n';
import { styled } from '@mui/material/styles';
import {
  onlyUpdateForKeys,
  renameProps,
  compose,
  withProps,
  branch,
  renderComponent,
} from 'recompose';

const SpaceStyled = styled('div')({
  display: 'flex',
  justifyContent: 'center',
});

const SpaceSpinner = () => (
  <SpaceStyled>
    <Spinner className={Classes.SMALL} />
  </SpaceStyled>
);

const SpaceBlank = () => (
  <SpaceStyled>
    <em>
      <FormattedMessage id="spaces.no.space" />
    </em>
  </SpaceStyled>
);

export default compose(
  withConfig,
  onlyUpdateForKeys([
    'isFetching',
    'internalSpaces',
    'spaces',
    'internalSpacesById',
  ]),
  renameProps({ internalSpaces: 'items' }),
  withList(),
  withProps(
    ({
      items,
      list,
      spaces,
      removeSpace,
      selectSpace,
      internalSpacesById,
    }) => ({
      isBlank: isEmpty(items),
      isOpen: true,
      itemRenderer: SpaceItem,
      handlers: {
        onChangeHandler: id =>
          has(id, spaces)
            ? removeSpace(prop(id, internalSpacesById))
            : selectSpace(prop(id, internalSpacesById)),
      },
      items: map(
        item => ({ ...item, isSelected: has(item.id, spaces) }),
        list.items,
      ),
    }),
  ),
  branch(({ isFetching }) => isFetching, renderComponent(SpaceSpinner)),
  branch(({ isBlank }) => isBlank, renderComponent(SpaceBlank)),
)(List);
