import React from 'react';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { Button, Popover, Position, NonIdealState } from '@blueprintjs/core';
import { styled } from '@mui/material/styles';
import MenuIcon from '@mui/icons-material/Menu';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';
import { useGetUserRights } from '../../hooks/user-rights';
import { hasManagePermission } from '../../lib/permissions';
import { Button as ButtonVisions } from '@sis-cc/dotstatsuite-visions';

const NonIdealStateStyled = styled(NonIdealState)({
  padding: 15,
});

const ButtonStyled = styled(ButtonVisions)({
  color: 'white !important',
  '::before': {
    color: 'white !important',
  },
  fontSize: 12,
});

const More = () => {
  const intl = useIntl();
  const history = useHistory();

  const handleClick = path => {
    history.push(path);
  };
  const { rights } = useGetUserRights({ queryKey: 'my-rights' });
  const canManage = hasManagePermission(rights || []);
  return (
    <Popover
      target={
        <ButtonStyled startIcon={<MenuIcon />}>
          {formatMessage(intl)(messages['fmr.more.menu'])}
        </ButtonStyled>
      }
      content={
        <NonIdealStateStyled
          description={
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              {canManage && (
                <Button
                  icon="shield"
                  onClick={() => handleClick('/all-rights')}
                >
                  <span>
                    {formatMessage(intl)(messages['route.all.rights'])}
                  </span>
                </Button>
              )}
              <Button
                icon="cloud-download"
                onClick={() => handleClick('/dump')}
              >
                <span>{formatMessage(intl)(messages['route.dump'])}</span>
              </Button>
            </div>
          }
        />
      }
      position={Position.BOTTOM}
      inheritDarkTheme={false}
    />
  );
};

export default More;
