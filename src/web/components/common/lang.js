import React, { useState } from 'react';
import * as R from 'ramda';
import { FormattedMessage } from '../../modules/i18n';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { styled } from '@mui/material/styles';
import messages from '../messages';
import { changeLocale } from '../../modules/i18n/action-creators';
import { useDispatch, useSelector } from 'react-redux';
import { getAvailableLocales, getLocale } from '../../modules/i18n/selectors';

const StyledBtn = styled(Button)({
  textTransform: 'none',
  fontSize: 12,
});

const Lang = () => {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState();
  const availableLocales = useSelector(getAvailableLocales());
  const locale = useSelector(getLocale());

  const handleOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl();
  };

  const handleItem = id => {
    handleClose();
    dispatch(changeLocale(id));
  };

  return (
    <div>
      <StyledBtn
        aria-owns={anchorEl ? 'locales-menu' : undefined}
        aria-haspopup="true"
        onClick={handleOpen}
        color="inherit"
        size="small"
      >
        <FormattedMessage {...R.propOr({ id: locale }, locale, messages)} />
      </StyledBtn>
      <Menu
        id="locales-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {R.map(
          ({ id }) => (
            <MenuItem key={id} onClick={() => handleItem(id)}>
              <FormattedMessage {...R.propOr({ id }, id, messages)} />
            </MenuItem>
          ),
          availableLocales,
        )}
      </Menu>
    </div>
  );
};

export default Lang;
