import React from 'react';
import { FormattedMessage, getLocale } from '../../modules/i18n';
import { useSelector } from 'react-redux';

const Locale = () => {
  const locale = useSelector(getLocale());

  return (
    <span>
      <FormattedMessage id="locale.current" />: {locale}
    </span>
  );
};

export default Locale;
