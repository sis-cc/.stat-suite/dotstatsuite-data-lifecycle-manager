import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'ramda';
import { NavLink } from 'react-router-dom';
import { FormattedMessage } from '../../modules/i18n';
import messages from '../messages';
import { Tooltip } from '@sis-cc/dotstatsuite-visions';
import { Button, useMediaQuery } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
  btn: {
    minWidth: '30px',
    minHeight: '30px',
    verticalAlign: 'middle',
    lineHeight: '30px',
    padding: '0px 8px',
    margin: '0px 8px',
    color: 'white !important',
    '::before': {
      color: 'white !important',
    },
    '&.a-hover': {
      textDecoration: 'none',
      backgroundColor: 'rgba(138,155,168,.15)',
    },
    '&.active': {
      textDecoration: 'none',
      backgroundColor: 'rgba(255, 255, 255, 0.2) !important',
    },
    fontSize: '12px ! important',
  },
}));

const Menu = ({ routes }) => {
  const classes = useStyles();
  const hiddenupmd = useMediaQuery(theme => theme.breakpoints.up('md'));
  const hiddendownmd = useMediaQuery(theme => theme.breakpoints.down('md'));
  return (
    <div>
      {map(({ name, path, translationKey, disabled, icon, exact }) => {
        const transkey = translationKey ? (
          <FormattedMessage {...messages[translationKey]} />
        ) : null;
        const link = (
          <Button
            className={classes.btn}
            startIcon={icon}
            disabled={!!disabled}
            component={NavLink}
            to={path}
            exact={exact}
            underline="none"
            sx={{
              '& .MuiButton-startIcon': {
                marginRight: 0,
              },
            }}
          >
            {hiddendownmd ? null : transkey}
          </Button>
        );
        return (
          <React.Fragment key={name}>
            {hiddenupmd ? null : (
              <Tooltip
                placement="bottom"
                title={
                  translationKey ? (
                    <FormattedMessage {...messages[translationKey]} />
                  ) : (
                    ''
                  )
                }
              >
                {link}
              </Tooltip>
            )}
            {hiddendownmd ? null : link}
          </React.Fragment>
        );
      }, routes)}
    </div>
  );
};

Menu.propTypes = {
  routes: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      path: PropTypes.string.isRequired,
      icon: PropTypes.string,
      disabled: PropTypes.bool,
      translationKey: PropTypes.string,
    }),
  ),
};

export default Menu;
