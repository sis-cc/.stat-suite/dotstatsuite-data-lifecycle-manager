import React from 'react';
import { NonIdealState } from '@blueprintjs/core';
import { FormattedMessage } from '../../modules/i18n';

const NotFound = () => {
  return <NonIdealState title={<FormattedMessage id="route.not.found" />} visual="send-to-map" />;
};

NotFound.propTypes = {};

export default NotFound;
