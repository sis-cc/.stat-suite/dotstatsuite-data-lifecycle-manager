import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';
import { map } from 'ramda';
import { Grid, Typography, Button } from '@mui/material';
import withRoutes from '../../hoc/routes';
import Layout from './layout';
import NotFound from './not-found';
import { useOidc, userSignIn } from '../../lib/oidc';
import { getUser } from '../../modules/oidc/selectors';
import { useSelector } from 'react-redux';
import { FormattedMessage } from '../../modules/i18n';

const defaultAppRoute = routes => {
  const route = routes.getDefault();
  if (route) return <Redirect to={route.path} />;
};

const App = ({ routes }) => {
  const auth = useOidc();
  const user = useSelector(getUser);

  return (
    <Layout routes={routes.getRoutes()}>
      {user ? (
        <Switch>
          {map(
            route => (
              <Route
                key={route.path}
                exact={route.exact}
                path={route.path}
                component={route.component}
              />
            ),
            routes.getRoutes(),
          )}
          {defaultAppRoute(routes)}
          <Route component={NotFound} />
        </Switch>
      ) : (
        <Grid container direction="column">
          <Grid item container justifyContent="center">
            <Typography variant="h6" align="center">
              <FormattedMessage id="auth.required" />
            </Typography>
          </Grid>
          <Grid item container justifyContent="center">
            <Button
              onClick={() => userSignIn(auth)}
              variant="outlined"
              color="primary"
            >
              <FormattedMessage id="user.login" />
            </Button>
          </Grid>
        </Grid>
      )}
    </Layout>
  );
};

App.propTypes = {
  routes: PropTypes.object.isRequired,
};

export default withRoutes(App);
