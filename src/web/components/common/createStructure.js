import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import * as R from 'ramda';
import { Tooltip, useMediaQuery } from '@mui/material';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import makeStyles from '@mui/styles/makeStyles';
import { FormattedMessage, formatMessage } from '../../modules/i18n';
import messages from '../messages';
import {
  getSelectedSpaces,
  getSortedTypes,
} from '../../modules/filters/selectors';
import { useSelector } from 'react-redux';
import CreateNewFolderIcon from '@mui/icons-material/CreateNewFolder';
import { artefactTypesFMR } from '../artefacts/constants';

const useStyles = makeStyles(() => ({
  btn: {
    minWidth: '30px',
    minHeight: '30px',
    verticalAlign: 'middle',
    lineHeight: '30px',
    color: 'white !important',
    '&.pt-active': {
      color: 'white !important',
      background: 'rgba(255, 255, 255, 0.2) !important',
    },
    '::before': {
      color: 'white !important',
    },

    fontSize: '12px ! important',
  },
}));

const CreateStructre = () => {
  const classes = useStyles();
  const intl = useIntl();
  const types = useSelector(getSortedTypes());
  const [anchorEl, setAnchorEl] = useState(null);
  const FMRoot = R.prop('linkFMR', window.CONFIG?.member?.scope);
  const spaces = useSelector(getSelectedSpaces());
  const space = R.pipe(R.values, R.head, R.prop('id'))(spaces);
  const hiddenupmd = useMediaQuery(theme => theme.breakpoints.up('md'));

  const button = (
    <Button
      aria-haspopup="true"
      onClick={e => setAnchorEl(e.currentTarget)}
      size="medium"
      className={classes.btn}
      startIcon={<CreateNewFolderIcon />}
    >
      {hiddenupmd ? (
        <FormattedMessage {...messages['fmr.create.structure']} />
      ) : null}
    </Button>
  );
  return (
    <div>
      <Tooltip
        placement="bottom"
        title={<FormattedMessage {...messages['fmr.create.structure']} />}
      >
        {button}
      </Tooltip>
      <Menu
        id="types-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
      >
        {R.map(type => {
          return (
            <MenuItem
              key={type.id}
              onClick={() => {
                window.open(
                  `${FMRoot}/${R.path(
                    [type.id, 'artefactTypeWizard'],
                    artefactTypesFMR,
                  )}.html?env=${space}`,
                  '_blank',
                );
                setAnchorEl(null);
              }}
            >
              {formatMessage(intl)(
                R.propOr(
                  { id: `artefact.type.${type.id}` },
                  `artefact.type.${type.id}`,
                  messages,
                ),
              )}
            </MenuItem>
          );
        }, R.reject(R.propEq('id', 'categorisation'), types))}
      </Menu>
    </div>
  );
};

CreateStructre.propTypes = {
  availableLocales: PropTypes.array,
  locale: PropTypes.string,
  changeLocale: PropTypes.func,
};

export default CreateStructre;
