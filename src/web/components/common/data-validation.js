import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import * as dateFns from 'date-fns';
import { styled } from '@mui/material/styles';
import {
  Checkbox,
  Classes,
  Collapse,
  Colors,
  Icon,
  Position,
  RadioGroup,
  Radio,
} from '@blueprintjs/core';
import { DateInput, TimePickerPrecision } from '@blueprintjs/datetime';
import Layers from '@mui/icons-material/Layers';
import Update from '@mui/icons-material/Update';
import { FilterHeader } from '../filters/filter';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';

const Container = styled('div')({
  borderTop: '2px solid grey',
});

const Item = styled('div')({
  margin: 10,
  padding: '0 !important',
});

const Content = styled('div')({
  padding: 12,
});

const PITContent = styled('div')({
  padding: 12,
  display: 'flex',
});

const PITTargetOptions = styled('div')({
  border: '1px solid grey',
  display: 'flex',
  padding: 6,
});
const PITDate = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  minWidth: 200,
  padding: '0 6px',
});

const Disclaimer = styled('span')({
  fontSize: 12,
  color: 'grey',
});

const ContentIcon = styled(Layers)({
  color: Colors.GRAY1,
});

const PITIcon = styled(Update)({
  color: Colors.GRAY1,
});

const Header = styled('div')({
  display: 'flex',
  alignItems: 'center',
});

const PITManagement = ({
  date,
  hasSource,
  onChangeDate,
  onChangeWithRestoration,
  onChangeSourceVersion,
  onChangeTargetVersion,
  sourceVersion,
  targetVersion,
  validationType,
  withRestoration,
  intl,
  minDate,
  link,
}) => (
  <Item className={Classes.CARD}>
    <Header className={Classes.CALLOUT}>
      <PITIcon />
      {formatMessage(intl)(R.prop('pit.management', messages))}
      {link && (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={link}
          style={{ marginLeft: '15px' }}
        >
          <Icon icon="help" style={{ color: Colors.GRAY1 }} />
        </a>
      )}
    </Header>
    <PITContent>
      {hasSource && (
        <div style={{ marginRight: '10px' }}>
          {formatMessage(intl)(R.prop('pit.management.source', messages))}
          <RadioGroup
            selectedValue={sourceVersion}
            onChange={e => onChangeSourceVersion(e.target.value)}
          >
            <Radio
              label={formatMessage(intl)(R.prop('pit.inactive', messages))}
              value="live"
            />
            <Radio
              label={formatMessage(intl)(R.prop('pit.active', messages))}
              value="pit"
            />
          </RadioGroup>
        </div>
      )}
      <div>
        {formatMessage(intl)(R.prop('pit.management.target', messages))}
        <Radio
          checked={targetVersion === 'live'}
          label={formatMessage(intl)(R.prop('pit.inactive', messages))}
          onChange={() => onChangeTargetVersion('live')}
        />
        <PITTargetOptions>
          <div>
            <Radio
              checked={targetVersion === 'pit'}
              label={formatMessage(intl)(R.prop('pit.active', messages))}
              onChange={() => onChangeTargetVersion('pit')}
            />
            <Disclaimer>
              {formatMessage(intl)(
                R.prop('pit.active.target.disclaimer', messages),
              )}
            </Disclaimer>
          </div>
          <PITDate>
            {formatMessage(intl)(R.prop('pit.target.date', messages))}
            <DateInput
              disabled={
                targetVersion === 'live' || validationType === 'nochange'
              }
              value={date}
              onChange={onChangeDate}
              formatDate={date => date.toLocaleString()}
              parseDate={date => new Date(date)}
              placeholder="DD-MM-YYYY HH:mm:ss"
              minDate={minDate}
              maxDate={dateFns.addYears(new Date(), 5)}
              popoverProps={{ position: Position.TOP }}
              rightElement={
                <Icon
                  icon="calendar"
                  style={{ color: Colors.GRAY1, paddingTop: '6px' }}
                />
              }
              timePrecision={TimePickerPrecision.SECOND}
              timePickerProps={{ minTime: minDate }}
            />
          </PITDate>
          <Checkbox
            disabled={targetVersion === 'live' || validationType === 'nochange'}
            checked={withRestoration}
            label={formatMessage(intl)(R.prop('pit.restoration', messages))}
            onChange={() => onChangeWithRestoration(!withRestoration)}
          />
        </PITTargetOptions>
      </div>
    </PITContent>
  </Item>
);

PITManagement.propTypes = {
  date: PropTypes.instanceOf(Date),
  hasSource: PropTypes.bool,
  link: PropTypes.string,
  minDate: PropTypes.instanceOf(Date),
  onChangeDate: PropTypes.func.isRequired,
  onChangeWithRestoration: PropTypes.func.isRequired,
  onChangeSourceVersion: PropTypes.func.isRequired,
  onChangeTargetVersion: PropTypes.func.isRequired,
  sourceVersion: PropTypes.string,
  targetVersion: PropTypes.string,
  validationType: PropTypes.string,
  withRestoration: PropTypes.bool,
  intl: PropTypes.object,
};

export const Menu = ({ icon, value, options, onChange, labels, link }) => (
  <Item className={Classes.CARD}>
    <Header className={Classes.CALLOUT}>
      {icon}
      {R.prop('title', labels)}
      {link && (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={link}
          style={{ paddingLeft: '15px' }}
        >
          <Icon icon="help" style={{ color: Colors.GRAY1 }} />
        </a>
      )}
    </Header>
    <Content>
      <RadioGroup
        onChange={e => onChange(e.target.value)}
        selectedValue={value}
      >
        {R.map(
          v => (
            <Radio key={v} label={R.prop(v, labels)} value={v} />
          ),
          options,
        )}
      </RadioGroup>
    </Content>
  </Item>
);

Menu.propTypes = {
  icon: PropTypes.node,
  labels: PropTypes.object,
  link: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array,
  value: PropTypes.string,
};

const DataValidationForm = ({
  contentType,
  date,
  hasSource,
  links,
  onChangeDate,
  onChangeWithRestoration,
  onChangeSourceVersion,
  onChangeTargetVersion,
  onChangeContentType,
  sourceVersion,
  targetVersion,
  title,
  validationType,
  withRestoration,
  intl,
}) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const minDate = new Date();
  return (
    <Container>
      <FilterHeader
        title={
          title || formatMessage(intl)(R.prop('data.validation.menu', messages))
        }
        isOpen={isOpen}
        collapseChange={() => setIsOpen(!isOpen)}
      />
      <Collapse isOpen={isOpen}>
        {R.is(Function, onChangeContentType) && (
          <Menu
            icon={<ContentIcon />}
            labels={{
              title: formatMessage(intl)(R.prop('content.type', messages)),
              both: formatMessage(intl)(R.prop('content.both', messages)),
              data: formatMessage(intl)(R.prop('content.data', messages)),
              metadata: formatMessage(intl)(
                R.prop('content.metadata', messages),
              ),
            }}
            onChange={onChangeContentType}
            options={['both', 'data', 'metadata']}
            value={contentType}
          />
        )}
        <PITManagement
          date={date}
          hasSource={hasSource}
          onChangeDate={onChangeDate}
          onChangeWithRestoration={onChangeWithRestoration}
          onChangeSourceVersion={onChangeSourceVersion}
          onChangeTargetVersion={onChangeTargetVersion}
          sourceVersion={sourceVersion}
          targetVersion={targetVersion}
          validationType={validationType}
          withRestoration={withRestoration}
          intl={intl}
          minDate={minDate}
          link={links.pit}
        />
      </Collapse>
    </Container>
  );
};

DataValidationForm.propTypes = {
  hasSource: PropTypes.bool,
  intl: PropTypes.object,
  validationType: PropTypes.string,
  date: PropTypes.instanceOf(Date),
  onChangeDate: PropTypes.func.isRequired,
  onChangeWithRestoration: PropTypes.func.isRequired,
  onChangeSourceVersion: PropTypes.func.isRequired,
  onChangeTargetVersion: PropTypes.func.isRequired,
  onChangeContentType: PropTypes.func,
  links: PropTypes.object,
  sourceVersion: PropTypes.string,
  targetVersion: PropTypes.string,
  title: PropTypes.string,
  withRestoration: PropTypes.bool,
  contentType: PropTypes.string,
};

export default DataValidationForm;
