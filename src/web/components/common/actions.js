import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { styled } from '@mui/material/styles';
import { Classes, Button } from '@blueprintjs/core';
import { map, pick } from 'ramda';

const ActionsStyled = styled('div')({
  display: 'flex !important',
  justifyContent: 'center',
});

const Actions = ({ actions = [] }) => {
  return (
    <ActionsStyled className={cx(Classes.BUTTON_GROUP, Classes.MINIMAL)}>
      {map(
        action => (
          <Button
            className={Classes.MINIMAL}
            key={action.id}
            {...pick(
              ['disabled', 'icon', 'intent', 'loading', 'onClick'],
              action,
            )}
          >
            {action.label}
          </Button>
        ),
        actions,
      )}
    </ActionsStyled>
  );
};

Actions.propTypes = {
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      disabled: PropTypes.bool,
      icon: PropTypes.string,
      id: PropTypes.string.isRequired,
      intent: PropTypes.number,
      loading: PropTypes.bool,
      onClick: PropTypes.func.isRequired,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    }),
  ),
};

export default Actions;
