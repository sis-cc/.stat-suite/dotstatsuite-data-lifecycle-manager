import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Tooltip from '@mui/material/Tooltip';
import { injectIntl } from 'react-intl';
import cx from 'classnames';
import i18nMessages from '../messages';

export const ItemLog = ({ id, icon, intl, log, title, classes }) => {
  const type = R.prop('type', log);
  if (!type) return null;

  const messages = R.pipe(
    R.ifElse(
      R.always(R.has(log.statusKey, i18nMessages) && !R.isNil(intl)),
      ({ statusKey }) => intl.formatMessage(i18nMessages[statusKey]),
      R.prop('message'),
    ),
    R.when(R.complement(R.is)(Array), R.of),
  )(log);
  return (
    <Tooltip
      title={
        <div>
          <span>{title}</span>
          {R.isEmpty(messages) ? null : (
            <div>
              {R.addIndex(R.map)(
                (message, ind) => (
                  <p key={`${id}-${ind}`}>{message}</p>
                ),
                messages,
              )}
            </div>
          )}
        </div>
      }
      classes={{
        popper: classes.popper,
        tooltip: cx(classes[type], classes.tooltip),
      }}
      placement="left"
    >
      {icon}
    </Tooltip>
  );
};

ItemLog.propTypes = {
  id: PropTypes.string.isRequired,
  icon: PropTypes.element,
  intl: PropTypes.object,
  log: PropTypes.object,
  title: PropTypes.string,
  classes: PropTypes.object,
};

export default injectIntl(ItemLog);
