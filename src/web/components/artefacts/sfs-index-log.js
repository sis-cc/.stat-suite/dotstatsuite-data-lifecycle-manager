import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { useSelector } from 'react-redux';
import AddIcon from '@mui/icons-material/AddToPhotos';
import { formatMessage } from '../../modules/i18n';
import { ERROR, SUCCESS } from '../../modules/common/constants';
import { getArtefactIndexationLog } from '../../modules/sfsIndex/selectors';
import Dialog from './item-log-dialog';
import messages from '../messages';
import makeStyles from '@mui/styles/makeStyles';
import { getArtefact } from '../../modules/artefacts';

const useStyles = makeStyles(() => ({
  icon: {
    marginLeft: 4,
    marginRight: 4,
    fontSize: '20px !important',
  },
}));

const withIntlLogMessage = (intl, id) => log =>
  R.when(
    log => R.has('data', log) && R.propEq('type', SUCCESS, log),
    ({ type, data }) => {
      const pairs = R.pipe(R.map(R.toPairs), R.unnest)(data);
      const datasources = R.reduce(
        (acc, [dId, info]) => {
          if (R.path(['dataflows', 'indexed'], info) >= 1) {
            return R.append(dId, acc);
          }
          return acc;
        },
        [],
        pairs,
      );
      return {
        type,
        message: formatMessage(intl)(
          R.prop(`sfs.index.success.message`, messages),
          { id, datasources: R.join(',', datasources) },
        ),
      };
    },
    log,
  );

const SfsIndexLog = ({ artefactId, intl }) => {
  const classes = useStyles();
  const _log = useSelector(getArtefactIndexationLog(artefactId));
  const artefact = useSelector(getArtefact(artefactId));
  if (R.isNil(_log)) {
    return null;
  }
  const log = withIntlLogMessage(intl, artefact.sdmxId)(_log);
  const key = R.pipe(R.prop('type'), R.toLower)(log);
  return (
    <Dialog
      id="sfs-index"
      title={formatMessage(intl)(R.prop(`sfs.index.${key}`, messages))}
      log={log}
    >
      <AddIcon
        className={classes.icon}
        style={{ color: R.prop('type', log) === ERROR ? '#db3737' : 'green' }}
      />
    </Dialog>
  );
};

SfsIndexLog.propTypes = {
  artefactId: PropTypes.string.isRequired,
  intl: PropTypes.object,
};

export default SfsIndexLog;
