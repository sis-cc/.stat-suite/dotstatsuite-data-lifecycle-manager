import React from 'react';
import PropTypes from 'prop-types';
import { head, isEmpty, isNil, prop, values } from 'ramda';
import { compose, mapProps } from 'recompose';
import { AuthDialog } from '@sis-cc/dotstatsuite-visions';
import Space from '../common/space';
import { withExternalAuth } from '../../modules/external-auth';
import { FormattedMessage } from '../../modules/i18n';

const SpaceAuthDialog = ({ closeSpaceAuthMenu, hasFailed, space, setSpaceExtAuthOptions }) => {
  if (isNil(space)) {
    return null;
  }
  return (
    <AuthDialog
      isOpen={true}
      labels={{
        anonymous: <FormattedMessage id="space.auth.anonymous" />,
        error: hasFailed ? <FormattedMessage id="space.auth.failed" /> : null,
        header: (
          <FormattedMessage id="space.auth.header" values={{ space: <Space {...space} /> }} />
        ),
        password: <FormattedMessage id="space.auth.password" />,
        user: <FormattedMessage id="space.auth.user" />,
        submit: <FormattedMessage id="space.auth.login" />,
      }}
      onClose={() => closeSpaceAuthMenu(space.id)}
      onSubmit={options => setSpaceExtAuthOptions(space.id, options)}
    />
  );
};

SpaceAuthDialog.propTypes = {
  closeSpaceAuthMenu: PropTypes.func.isRequired,
  hasFailed: PropTypes.bool,
  space: PropTypes.object,
  setSpaceExtAuthOptions: PropTypes.func.isRequired,
};

export default compose(
  withExternalAuth,
  mapProps(
    ({ closeSpaceAuthMenu, failedSpaceIds, menuSpaceId, setSpaceExtAuthOptions, spaces }) => {
      let space = null;
      let hasFailed = false;
      if (menuSpaceId) {
        space = prop(menuSpaceId, spaces);
      } else if (!isEmpty(failedSpaceIds)) {
        hasFailed = true;
        space = prop(head(values(failedSpaceIds)), spaces);
      }
      return {
        closeSpaceAuthMenu,
        hasFailed,
        setSpaceExtAuthOptions,
        space,
      };
    },
  ),
)(SpaceAuthDialog);
