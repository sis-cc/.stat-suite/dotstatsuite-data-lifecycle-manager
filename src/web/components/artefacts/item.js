import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import { assoc, is, propOr, isNil } from 'ramda';
import { useSelector } from 'react-redux';
import { Checkbox, Classes, Colors, Icon, Spinner } from '@blueprintjs/core';
import DescriptionIcon from '@mui/icons-material/Description';
import AutorenewIcon from '@mui/icons-material/Autorenew';
import AddIcon from '@mui/icons-material/AddToPhotos';
import PowerIcon from '@mui/icons-material/Power';
import { FormattedMessage, getLocale } from '../../modules/i18n';
import Space from '../common/space';
import { ItemActions } from './actions';
import itemLogs from './item-logs';
import MoreButton from './more-button';
import { DataflowDetails } from './dataflow-details';
import { compose, mapProps } from 'recompose';
import { withTransferArtefact } from '../../modules/transfer-artefact';
import { withTransferData } from '../../modules/transfer-data';
import { getIsExportingData } from '../../modules/export-data';
import { withDataflowDetails } from '../../modules/dataflow-details';
import { withCategorisationState } from '../../modules/categorize';
import messages from '../messages';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import Link from '@mui/material/Link';
import { withAuthHeader } from '../../modules/common/withauth';
import apiManager from '../../apiManager';
import {
  getDsdLinkLog,
  getInitDataflowsLog,
} from '../../modules/defineMSD/selectors';
import { ExportDataDialog } from '../dialog-menus';
import { getArtefactIsIndexing } from '../../modules/sfsIndex/selectors';
import { useActivateDataflows } from '../../hooks/useActivateDataflows';
import { getDfDataExplorerUrl } from '../../lib/artefacts';

const ActivateIcon = styled(PowerIcon)({
  marginLeft: 10,
  color: '#5c7080',
  fontSize: '20px !important',
});

const MSDLinkIcon = styled(DescriptionIcon)({
  marginLeft: 10,
  color: '#5c7080',
  fontSize: '20px !important',
});

const InitDfsIcon = styled(AutorenewIcon)({
  marginLeft: 10,
  color: '#5c7080',
  fontSize: '20px !important',
});

const IndexIcon = styled(AddIcon)({
  marginLeft: 10,
  color: '#5c7080',
  fontSize: '20px !important',
});

const StyledCheckbox = styled(Checkbox)({
  marginBottom: '0 !important',
});

const Container = styled('div')({
  marginTop: 10,
  paddingTop: 10,
  borderTop: '1px solid #ccc',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
});

const ItemStyled = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
});

const StyledPending = styled('div')({
  marginTop: 10,
  paddingTop: 10,
  borderTop: '1px solid #ccc',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

const Resume = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
});

const Interface = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
});

//--------------------------------------------------------------------------------------------------
export const IsFinal = ({ isFinal }) => {
  if (!isFinal) return null;

  return (
    <span>
      <span className={Classes.NAVBAR_DIVIDER}></span>
      <Icon icon="tick" />
      <span marginLeft={5}>
        <FormattedMessage id="artefact.is.final" />
      </span>
    </span>
  );
};

IsFinal.propTypes = {
  isFinal: PropTypes.bool,
};

//--------------------------------------------------------------------------------------------------

const IconStyled = styled(Icon)({ marginLeft: 10, marginRight: 10 });
const SpinnerStyled = styled(Spinner)({ marginRight: 10 });

//--------------------------------------------------------------------------------------------------
const ItemPending = ({ icon, message, children }) => {
  return (
    <StyledPending>
      <SpinnerStyled />
      {icon ? <Icon icon={icon} style={{ color: Colors.GRAY1 }} /> : null}
      <span marginLeft={10}>{message}</span>
      {children}
    </StyledPending>
  );
};

ItemPending.propTypes = {
  icon: PropTypes.string,
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

//--------------------------------------------------------------------------------------------------
const ItemView = props => {
  const {
    id,
    isDeletable,
    label,
    type,
    code,
    version,
    isFinal,
    agencyId,
    space,
    isNew,
    error,
    handlers,
    isSelected,
    onClick,
    hasData,
    dataExplorerUrl,
    requestDetails,
    categories,
    observations,
    isDetailsExpanded,
    deleteDetails,
    isCategorizing,
    annotations,
    sdmxId,
    isActivating,
    activateLog,
    links,
  } = props;
  const transferArtefact = props.transferArtefact;
  const msdLinkLog = useSelector(getDsdLinkLog(id));
  const initDfsLog = useSelector(getInitDataflowsLog(id));
  const [isOpenExportDataMenu, setIsOpenExportDataMenu] = useState(false);
  const isIndexing = useSelector(getArtefactIsIndexing(id));
  const isExportingData = useSelector(getIsExportingData(id));

  const { activate } = useActivateDataflows([{ id, sdmxId, space, type }]);

  if (isIndexing) {
    return (
      <ItemPending message={<FormattedMessage id="sfs.is.indexing" />}>
        <IndexIcon />
      </ItemPending>
    );
  }

  if (isActivating) {
    return (
      <ItemPending
        message={<FormattedMessage id="activate.dataflow.is.activating" />}
      >
        <ActivateIcon />
      </ItemPending>
    );
  }

  if (propOr(false, 'isLinking', msdLinkLog)) {
    return (
      <ItemPending message={<FormattedMessage id="is.linking.msd" />}>
        <MSDLinkIcon />
      </ItemPending>
    );
  }

  if (propOr(false, 'isInitializing', initDfsLog)) {
    return (
      <ItemPending message={<FormattedMessage id="is.initializing.dfs" />}>
        <InitDfsIcon />
      </ItemPending>
    );
  }

  if (transferArtefact.isTransfering) {
    return (
      <ItemPending
        message={<FormattedMessage id="artefact.is.transfering" />}
        icon="add-to-artifact"
      />
    );
  }

  if (isExportingData) {
    return (
      <ItemPending
        message={<FormattedMessage id="artefact.is.exporting.data" />}
        icon="download"
      />
    );
  }

  if (isCategorizing) {
    return (
      <ItemPending
        message={<FormattedMessage id="artefact.categorising" />}
        icon="download"
      />
    );
  }

  const transferData = props.transferData;
  if (transferData.isTransfering) {
    return (
      <ItemPending
        message={<FormattedMessage id="artefact.is.transfering.data" />}
      >
        <IconStyled icon="arrow-right" style={{ color: Colors.GRAY1 }} />
        <Space {...transferData.destinationSpace} />
      </ItemPending>
    );
  }

  const ItemLogs = itemLogs(id);

  return (
    <Container>
      {isOpenExportDataMenu && (
        <ExportDataDialog
          onClose={() => setIsOpenExportDataMenu(false)}
          selection={[id]}
        />
      )}
      <StyledCheckbox
        checked={isSelected}
        onChange={() => handlers.toggleArtefact(id)}
      />
      <ItemStyled>
        <Resume>
          <div>
            <div style={{ marginBottom: 10 }}>
              <Space
                {...space}
                label={
                  <FormattedMessage
                    {...propOr({ id: type }, `artefact.type.${type}`, messages)}
                  />
                }
              />
              <span style={{ marginLeft: 10 }}>
                <Link onClick={onClick}>
                  {isNil(label) ? (
                    <FormattedMessage id="artefact.no.label" />
                  ) : (
                    label
                  )}
                </Link>
              </span>
            </div>
            <div>
              <span>[{code}]</span>
              <span className={Classes.NAVBAR_DIVIDER}></span>
              <span>[{version}]</span>
              <IsFinal isFinal={isFinal} />
              <span className={Classes.NAVBAR_DIVIDER}></span>
              <span>{agencyId}</span>
              {hasData ? (
                <span className={Classes.NAVBAR_DIVIDER}></span>
              ) : null}
              {hasData && is(Function, requestDetails) ? (
                <MoreButton
                  id={id}
                  isActive={isDetailsExpanded}
                  request={isDetailsExpanded ? deleteDetails : requestDetails}
                />
              ) : null}
            </div>
          </div>
          <Interface>
            {hasData && !isNil(dataExplorerUrl) ? (
              <a
                href={dataExplorerUrl}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon
                  icon="eye-open"
                  style={{
                    color: Colors.GRAY1,
                    marginLeft: 10,
                    marginRight: 10,
                  }}
                />
              </a>
            ) : null}
            <ItemLogs
              artefactId={id}
              isNew={isNew}
              error={error}
              space={space}
              activateLog={activateLog}
            />
            <ItemActions
              activate={activate}
              artefact={{
                id,
                agencyId,
                code,
                space,
                type,
                version,
                annotations,
                isFinal,
                sdmxId,
                links,
              }}
              artefactId={id}
              artefactDelete={handlers.artefactDelete}
              artefactExport={handlers.artefactExportStructure}
              manageResources={handlers.selectResourcesMenuDF}
              dataExport={() => setIsOpenExportDataMenu(true)}
              hasData={hasData}
              isDeletable={isDeletable}
            />
          </Interface>
        </Resume>
        {hasData ? (
          <DataflowDetails
            categories={categories}
            id={id}
            details={[
              [
                <i key="dataflow.space">
                  <FormattedMessage id="dataflow.space" />:{' '}
                </i>,
                [space.id],
              ],
              [
                <i key="dataflow.flavours">
                  <FormattedMessage id="dataflow.flavours" />:{' '}
                </i>,
                [
                  `${agencyId}:${code}(${version})`,
                  `${agencyId}/${code}/${version}`,
                ],
              ],
            ]}
            isExpanded={isDetailsExpanded}
            observations={observations}
            requestDetails={requestDetails}
          />
        ) : null}
      </ItemStyled>
    </Container>
  );
};

ItemView.propTypes = {
  activateProps: PropTypes.object,
  dataExplorerUrl: PropTypes.string,
  hasData: PropTypes.bool,
  locale: PropTypes.string,
  id: PropTypes.string.isRequired,
  isDeletable: PropTypes.bool,
  exportData: PropTypes.func,
  onClick: PropTypes.func,
  label: PropTypes.string,
  type: PropTypes.string,
  code: PropTypes.string,
  version: PropTypes.string,
  isFinal: PropTypes.bool,
  agencyId: PropTypes.string,
  isNew: PropTypes.bool,
  error: PropTypes.object,
  handlers: PropTypes.object,
  space: PropTypes.shape({
    color: PropTypes.string,
  }).isRequired,
  isSelectable: PropTypes.bool,
  isSelected: PropTypes.bool,
  sdmxId: PropTypes.string,
  isActivating: PropTypes.bool,
  activateLog: PropTypes.object,
};

//--------------------------------------------------------------------------------------------------
const itemProxy = id =>
  compose(
    withTransferArtefact(id),
    mapProps(
      ({
        agencyId,
        code,
        error,
        space,
        type,
        version,
        locale,
        links,
        ...rest
      }) => {
        const { url, headers } = getRequestArgs({
          identifiers: { agencyId, code, version },
          type,
          format: 'xml',
          datasource: assoc('url', space.endpoint, space),
          withUrn: false,
        });
        const onClick = () => {
          apiManager
            .get(url, { headers: withAuthHeader(space)(headers) })
            .then(response => response.data)
            .then(xml => {
              let blob = new Blob([xml], { type: 'text/xml' });
              let url = URL.createObjectURL(blob);
              window.open(url, '_blank');
              URL.revokeObjectURL(url);
            });
        };
        return {
          ...rest,
          agencyId,
          artefactError: error,
          code,
          links,
          hasData: type === 'dataflow',
          dataExplorerUrl:
            type === 'dataflow'
              ? getDfDataExplorerUrl({ agencyId, code, version, space }, locale)
              : null,
          isDeletable: !isNil(space.transferUrl),
          onClick,
          space,
          type,
          version,
        };
      },
    ),
    mapProps(({ isTransfering, isUpdating, destinationSpace, ...rest }) => ({
      ...rest,
      transferArtefact: { isTransfering, isUpdating, destinationSpace },
    })),
    withTransferData(id),
    mapProps(({ isTransfering, isUpdating, destinationSpace, ...rest }) => ({
      ...rest,
      transferData: { isTransfering, isUpdating, destinationSpace },
    })),
    withDataflowDetails(id),
    withCategorisationState(id),
  )(ItemView);

//--------------------------------------------------------------------------------------------------
export const Item = ({ id, isDeleting, isExporting, ...rest }) => {
  const locale = useSelector(getLocale());

  if (isDeleting)
    return (
      <ItemPending
        message={<FormattedMessage id="artefact.is.deleting" />}
        icon="trash"
      />
    );
  if (isExporting)
    return (
      <ItemPending
        message={<FormattedMessage id="artefact.is.exporting.structure" />}
        icon="download"
      />
    );
  const ItemProxy = itemProxy(id);

  return <ItemProxy id={id} locale={locale} {...rest} />;
};

Item.propTypes = {
  id: PropTypes.string.isRequired,
  isDeleting: PropTypes.bool,
  isExporting: PropTypes.bool,
};
