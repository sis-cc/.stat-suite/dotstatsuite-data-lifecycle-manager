export const artefactTypesFMR = {
  agencyscheme: {
    artefactType: 'organisations/agencies',
    artefactTypeWizard: 'organisations/agency-wizard',
  },
  categoryscheme: {
    artefactType: 'items/categoryscheme',
    artefactTypeWizard: 'items/categoryscheme-wizard',
  },
  conceptscheme: {
    artefactType: 'items/conceptscheme',
    artefactTypeWizard: 'items/conceptscheme-wizard',
  },
  codelist: {
    artefactType: 'items/codelist',
    artefactTypeWizard: 'items/codelist-wizard',
  },
  dataprovider: {
    artefactType: 'organisations/dataproviders',
    artefactTypeWizard: 'organisations/dataProvider-wizard',
  },
  dataconsumer: {
    artefactType: 'organisations/dataConsumers',
    artefactTypeWizard: 'organisations/dataConsumer-wizard',
  },
  dataflow: {
    artefactType: 'data/overview',
    artefactTypeWizard: 'data/dataflow-wizard',
  },
  dataprovisionagreement: {
    artefactType: 'data/provisionAgreements',
    artefactTypeWizard: 'data/ProvisionAgreement-wizard',
  },
  datastructure: {
    artefactType: 'data/datastructure',
    artefactTypeWizard: 'data/dsd-wizard',
  },
  metadatastructure: {
    artefactType: 'metadata/metadatastructure',
    artefactTypeWizard: 'metadata/msd-wizard',
  },
  metadataflow: {
    artefactType: 'metadata/metadataflow',
    artefactTypeWizard: 'metadata/metadataflow-wizard',
  },
};
