import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import { FormattedMessage } from '../../modules/i18n';
import * as R from 'ramda';
import { Classes, Menu, MenuItem } from '@blueprintjs/core';
import MenuReferencesSelector from './refs-selector';

//--------------------------------------------------------------------------------------------------
const SpaceMenuItem = ({
  label,
  color,
  backgroundColor,
  onTransfer,
  disabled,
}) => {
  const StyledMenuItem = styled(MenuItem)({
    ':hover': {
      color: `${color} !important`,
      backgroundColor: `${backgroundColor} !important`,
    },
  });

  return (
    <StyledMenuItem disabled={disabled} text={label} onClick={onTransfer} />
  );
};

SpaceMenuItem.propTypes = {
  label: PropTypes.string.isRequired,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  disabled: PropTypes.bool,
  onTransfer: PropTypes.func,
};

SpaceMenuItem.defaultProps = {
  color: 'white', // '#182026'
  backgroundColor: '#EE6290', // '#0965C1' '#e8ebee'
  disabled: false,
};

//--------------------------------------------------------------------------------------------------
const MenuHeader = ({ title }) => {
  return (
    <li className={Classes.MENU_HEADER}>
      <h6>{title}</h6>
    </li>
  );
};

MenuHeader.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

//--------------------------------------------------------------------------------------------------
const MenuTransfer = ({
  sourceSpaces,
  destinationSpaces,
  onTransfer,
  withReferences,
}) => {
  return (
    <Menu>
      <MenuHeader title={<FormattedMessage id="transfer.source" />} />
      {R.map(
        space => (
          <SpaceMenuItem {...space} key={space.id} disabled />
        ),
        sourceSpaces,
      )}
      <MenuHeader title={<FormattedMessage id="transfer.destination" />} />
      {R.map(
        space => (
          <SpaceMenuItem
            {...space}
            key={space.id}
            onTransfer={onTransfer(space, withReferences)}
          />
        ),
        destinationSpaces,
      )}
    </Menu>
  );
};

MenuTransfer.propTypes = {
  destinationSpaces: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  ),
  sourceSpaces: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  ),
  onTransfer: PropTypes.func.isRequired,
  withReferences: PropTypes.string,
};

//--------------------------------------------------------------------------------------------------
export const MenuReferencesTransfer = props => {
  return (
    <MenuReferencesSelector>
      <MenuTransfer {...props} />
    </MenuReferencesSelector>
  );
};

MenuReferencesTransfer.propTypes = MenuTransfer.propTypes;
