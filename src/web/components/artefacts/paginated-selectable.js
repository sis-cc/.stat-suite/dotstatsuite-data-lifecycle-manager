import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import * as R from 'ramda';
import {
  getDeleteLog,
  getFilteredArtefacts,
  getIsFetching,
  getSpotlight,
} from '../../modules/artefacts/selectors';
import {
  spotlightHandler,
  getSortDirection,
  getSortFields,
  sortListHandler,
  setPaginationHandler,
} from '../list';
import { Artefacts, ArtefactsBlank, ArtefactsSpinner } from './index';
import {
  getSelectedSpaces,
  getSelectedTypes,
} from '../../modules/filters/selectors';
import {
  changeSpotlight,
  resetLog,
} from '../../modules/artefacts/action-creators';
import { useActivateDataflows } from '../../hooks/useActivateDataflows';

const PAGE_SIZE = 12;
const PAGE_BUFFER = 3;

const sortFields = [
  {
    id: 'type',
    accessor: 'type',
    isSelected: true,
    index: 0,
    direction: 'asc',
  },
  {
    id: 'label',
    accessor: 'label',
    isSelected: true,
    index: 1,
    direction: 'asc',
  },
  { id: 'code', accessor: 'code' },
  { id: 'version', accessor: 'version' },
  { id: 'is.final', accessor: 'isFinal' },
  { id: 'owner', accessor: 'agencyId' },
];

const PaginatedSelectableArtefacts = ({ artefacts, deleteLog, resetLog }) => {
  const dispatch = useDispatch();
  const spotlight = useSelector(getSpotlight);
  const [sort, setSortFields] = useState(sortFields);
  const [{ page, pageSize, pageBuffer }, setPagination] = useState({
    page: 0,
    pageSize: PAGE_SIZE,
    pageBuffer: PAGE_BUFFER,
  });
  const [selection, setSelection] = useState({});

  const _sortFields = getSortFields(sort);

  const selectList = R.pipe(
    R.values,
    R.map(art => ({ ...art, isSelected: R.has(art.id, selection) })),
    items => ({ items }),
  )(artefacts);
  const spotlightedList = spotlightHandler(spotlight)(selectList);
  const sortedList = sortListHandler({ fields: sort })(spotlightedList);

  const status = {
    total: R.length(R.values(artefacts)),
    current: R.length(spotlightedList.items),
  };

  const pages = Math.ceil(R.length(artefacts) / pageSize);
  const paginatedList =
    pages < 2
      ? sortedList
      : R.over(
          R.lensProp('items'),
          R.slice(page * pageSize, page * pageSize + pageSize),
        )(sortedList);

  const spotlightValueChange = event => {
    event.preventDefault();
    const value = event.target.value || '';
    dispatch(changeSpotlight({ ...spotlight, value }));
    setPagination({ page: 0, pageSize, pageBuffer });
  };

  const spotlightFieldChange = field => event => {
    event.preventDefault();
    dispatch(
      changeSpotlight(
        R.over(
          R.lensPath(['fields', field.id, 'isSelected']),
          R.not,
        )(spotlight),
      ),
    );
    setPagination({ page: 0, pageSize, pageBuffer });
  };

  const sortFieldChange = field => event => {
    event.preventDefault();
    setSortFields(
      R.map(
        _field => ({
          ..._field,
          direction: getSortDirection(field, _field),
          isSelected: _field.id === field.id,
          index:
            _field.id === field.id
              ? 0
              : _field.index === 0
              ? null
              : _field.index,
        }),
        sort,
      ),
    );
  };

  const paginationPageChange = page => event => {
    event.preventDefault();
    setPagination({ pageSize, pageBuffer, page });
  };

  const deselect = ids => {
    setSelection(R.omit(R.values(ids), selection));
  };

  const selectAll = () => {
    const ids = R.pipe(
      R.prop('items'),
      R.pluck('id'),
      R.indexBy(R.identity),
    )(spotlightedList);
    setSelection({ ...selection, ...ids });
  };
  const deselectAll = () => {
    const ids = R.pipe(R.prop('items'), R.pluck('id'))(spotlightedList);
    setSelection(R.omit(ids, selection));
  };
  const selectPage = () => {
    const pageIds = R.pipe(
      R.prop('items'),
      R.pluck('id'),
      R.indexBy(R.identity),
    )(paginatedList);
    setSelection({ ...selection, ...pageIds });
  };
  const toggleArtefact = id => {
    if (R.has(id, selection)) {
      setSelection(R.dissoc(id, selection));
    } else {
      setSelection(R.assoc(id, id, selection));
    }
  };

  const isAllSelected = R.pipe(
    R.prop('items'),
    R.reject(art => R.has(art.id, selection)),
    R.isEmpty,
  )(spotlightedList);

  const isAllPageSelected = R.pipe(
    R.prop('items'),
    R.reject(art => R.has(art.id, selection)),
    R.isEmpty,
  )(paginatedList);

  const selectedArtefacts = R.filter(
    art => R.has(art.id, selection),
    R.propOr([], 'items', sortedList),
  );
  const { activate } = useActivateDataflows(selectedArtefacts, () =>
    deselect(R.pluck('id', selectedArtefacts)),
  );
  const paginationProps = setPaginationHandler({ page, pageSize, pageBuffer })(
    sortedList,
  );
  const selectionProps = {
    activate,
    deselect,
    selection: selectedArtefacts,
    toggleArtefact,
    deselectAll: R.isEmpty(selection) ? null : deselectAll,
    selectAll: isAllSelected ? null : selectAll,
    selectPage:
      isAllPageSelected || paginationProps.pages < 2 ? null : selectPage,
    hasPages: paginationProps.pages >= 2,
  };
  return (
    <Artefacts
      deleteLog={deleteLog}
      resetLog={resetLog}
      list={paginatedList}
      pagination={paginationProps}
      pageChange={paginationPageChange}
      selection={selectionProps}
      sort={{ fields: sort, field: R.head(_sortFields) }}
      sortFieldChange={sortFieldChange}
      spotlight={spotlight}
      spotlightFieldChange={spotlightFieldChange}
      spotlightValueChange={spotlightValueChange}
      status={status}
    />
  );
};

PaginatedSelectableArtefacts.propTypes = {
  artefacts: PropTypes.object,
  deleteLog: PropTypes.array,
  resetLog: PropTypes.func,
};

export const ArtefactsMain = () => {
  const dispatch = useDispatch();
  const selectedSpaces = useSelector(getSelectedSpaces());
  const selectedTypes = useSelector(getSelectedTypes());
  const isFetching = useSelector(getIsFetching());
  const deleteLog = useSelector(getDeleteLog());
  const artefacts = useSelector(getFilteredArtefacts());

  if (isFetching && R.isEmpty(artefacts)) {
    return <ArtefactsSpinner />;
  }

  if (R.isEmpty(artefacts)) {
    return (
      <ArtefactsBlank
        deleteLog={deleteLog}
        noSpaces={R.isEmpty(selectedSpaces)}
        noTypes={R.isEmpty(selectedTypes)}
        resetLog={id => dispatch(resetLog(id))}
      />
    );
  }

  return (
    <PaginatedSelectableArtefacts
      deleteLog={deleteLog}
      artefacts={artefacts}
      resetLog={id => dispatch(resetLog(id))}
    />
  );
};
