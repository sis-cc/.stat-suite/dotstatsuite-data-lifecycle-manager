import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { useIntl } from 'react-intl';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import {
  Button,
  Checkbox,
  Classes,
  Dialog,
  Icon,
  Intent,
  Spinner,
} from '@blueprintjs/core';
import Card from '@mui/material/Card';
import DeleteForever from '@mui/icons-material/DeleteForever';
import RemoveCircle from '@mui/icons-material/RemoveCircle';
import ReportProblem from '@mui/icons-material/ReportProblem';
import Link from '@mui/material/Link';
import makeStyles from '@mui/styles/makeStyles';
import cx from 'classnames';
import { ItemLog } from './item-log';
import { formatMessage } from '../../modules/i18n';
import { ERROR, WARNING } from '../../modules/common/constants';
import { withAuthHeader } from '../../modules/common/withauth';
import { useGetUserRights } from '../../hooks/user-rights';
import { isArtefactDeleteAuthorized } from '../../lib/permissions';
import Space from '../common/space';
import messages from '../messages';
import apiManager from '../../apiManager';
import {
  getArtefactRelativesTree,
  getCleanUpLogs,
  getDeleteLogs,
  getDeletingIds,
  getErrors,
  getIsError,
  getIsFetching,
  getIsOpenRelatives,
  getMode,
  getSelection,
} from '../../modules/relatives/selectors';
import {
  cancelRelativesDelete,
  deleteRelativesArtefacts,
  cleanArtefactRelatives,
} from '../../modules/relatives/action-creators';

const useStyles = makeStyles(theme => ({
  dialog: {
    width: 'auto !important',
  },
  logErrorIcon: {
    fontSize: '20px !important',
    color: '#db3737',
  },
  logSuccessIcon: {
    fontSize: '20px !important',
    color: 'green',
  },
  footer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  forbidden: {
    marginRight: 8,
    fontSize: '18px !important',
    color: '#f44336',
  },
  warning: {
    marginRight: 8,
    fontSize: '18px !important',
    color: 'orange',
  },
  logs: {
    marginRight: 6,
  },
  checkBox: {
    marginBottom: '0 !important',
  },
  details: {
    alignItems: 'center',
    display: 'flex',
    marginBottom: 5,
    color: '#0549ab',
  },
  mainDetails: {
    alignItems: 'center',
    display: 'flex',
    marginBottom: 5,
    color: '#0549ab',
    fontWeight: 'bold',
  },
  errorCard: {
    backgroundColor: theme.palette.error.main,
    color: 'white',
    marginBottom: 20,
    padding: 5,
  },
  messages: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  tooltip: {
    fontSize: 14,
    boxShadow: theme.shadows[2],
    maxWidth: '80vw',
  },
  WARNING: {
    backgroundColor: theme.palette.warning.light,
    color: 'black',
  },
  ERROR: {
    backgroundColor: theme.palette.error.main,
    color: 'white',
  },
  SUCCESS: {
    backgroundColor: theme.palette.success.dark,
    color: 'white',
  },
  popper: {
    opacity: 1,
  },
}));

const ErrorLogs = ({ errors, intl, classes }) => {
  if (R.isNil(errors)) {
    return null;
  }
  const getErrorMessage = error => {
    if (error.operation === 'getChildren') {
      return formatMessage(intl)(
        R.prop('artefact.relatives.error.children', messages),
        {
          type: R.pathOr('artefact', ['artefact', 'type'], error),
          id: R.path(['artefact', 'sdmxId'], error),
        },
      );
    }
    if (error.operation === 'getParents') {
      return formatMessage(intl)(
        R.prop('artefact.relatives.error.parents', messages),
        {
          type: R.pathOr('artefact', ['artefact', 'type'], error),
          id: R.path(['artefact', 'sdmxId'], error),
        },
      );
    }
    if (error.operation === 'parseMsdReference') {
      return formatMessage(intl)(
        R.prop('artefact.relatives.error.msd.ref', messages),
        {
          ref: R.prop('msdRef', error),
          id: R.path(['artefact', 'sdmxId'], error),
        },
      );
    }
    return error.message;
  };
  return (
    <Card className={classes.errorCard}>
      <b>{formatMessage(intl)(R.prop('artefact.relatives.error', messages))}</b>
      <br />
      {R.map(error => getErrorMessage(error), errors)}
    </Card>
  );
};
ErrorLogs.propTypes = {
  errors: PropTypes.array,
  intl: PropTypes.object,
  classes: PropTypes.object,
};

const getNonSelectableLog = (intl, artefact) => {
  const {
    sdmxParents = [],
    type,
    children = [],
    isError = false,
    isMsdLinked,
    nonSelectableChild,
  } = artefact;
  if (!R.isEmpty(sdmxParents)) {
    const [firstParents, rest] = R.splitAt(3, sdmxParents);
    const restCount = R.length(rest);
    const formattedParents = R.pipe(
      R.map(parent => `${parent.type} ${parent.sdmxId}`),
      R.join(', '),
    )(firstParents);
    return formatMessage(intl)(
      R.prop('artefact.relative.non.selectable', messages),
      {
        parents: formattedParents,
        others: restCount,
      },
    );
  }
  if (isMsdLinked) {
    return formatMessage(intl)(
      R.prop('artefact.relative.dsd.warning', messages),
    );
  }
  if (
    type === 'metadatastructure' &&
    R.find(R.propEq('isMsdLinked', true), children)
  ) {
    return formatMessage(intl)(R.prop('artefact.relative.msd.dsds', messages));
  }
  if (type === 'metadatastructure' && isError) {
    return formatMessage(intl)(
      R.prop('artefact.relative.msd.missing.warning', messages),
    );
  }
  if (!R.isNil(nonSelectableChild)) {
    return formatMessage(intl)(
      R.prop('artefact.relative.child.non.deletable', messages),
      {
        id: nonSelectableChild.id,
      },
    );
  }
  return null;
};

const getWarningLog = (intl, artefact, selection) => {
  const { parentsInTree = [] } = artefact;
  const nonSelectedParents = R.reject(
    a => R.includes(a.id, selection),
    parentsInTree,
  );
  if (R.isEmpty(nonSelectedParents)) {
    return null;
  }
  const formattedParents = R.pipe(
    R.map(parent => `${parent.type} ${parent.sdmxId}`),
    R.join(', '),
  )(nonSelectedParents);
  return formatMessage(intl)(R.prop('artefact.relative.dependency', messages), {
    parents: formattedParents,
  });
};

const ArtefactDeleteState = props => {
  const {
    classes,
    cleanUpLog,
    deleteLog,
    intl,
    isDeleteAuthorized,
    isDeleting,
    isSelected,
    nonSelectableLog,
    toggleArtefact,
    warningLog,
    space,
  } = props;
  if (!toggleArtefact) {
    return null;
  }
  if (isDeleting) {
    return <Spinner className={Classes.SMALL} />;
  }
  if (deleteLog) {
    return (
      <div className={classes.logs}>
        {cleanUpLog && (
          <ItemLog
            id="itemLog-cleanUpLog"
            icon={
              <DeleteForever
                className={
                  R.prop('type', cleanUpLog) === ERROR
                    ? classes.logErrorIcon
                    : classes.logSuccessIcon
                }
              />
            }
            title={formatMessage(intl)(
              R.prop(
                `artefact.cleanup.${R.toLower(
                  R.propOr('', 'type', cleanUpLog),
                )}`,
                messages,
              ),
              { label: R.prop('label', space) },
            )}
            log={cleanUpLog}
            classes={classes}
          />
        )}
        <ItemLog
          id="itemLog-deleteLog"
          icon={
            <DeleteForever
              className={
                R.prop('type', deleteLog) === ERROR
                  ? classes.logErrorIcon
                  : classes.logSuccessIcon
              }
            />
          }
          title={formatMessage(intl)(
            R.prop(
              `artefact.delete.${R.toLower(R.propOr('', 'type', deleteLog))}`,
              messages,
            ),
            { label: R.prop('label', space) },
          )}
          log={deleteLog}
          classes={classes}
        />
      </div>
    );
  }
  if (nonSelectableLog) {
    return (
      <ItemLog
        id="itemLog-nonSelectableLog"
        icon={<RemoveCircle className={classes.forbidden} />}
        title={null}
        log={{ message: nonSelectableLog, type: ERROR }}
        classes={classes}
      />
    );
  }
  if (warningLog) {
    return (
      <ItemLog
        id="itemLog-warningLog"
        icon={<ReportProblem className={classes.warning} />}
        title={null}
        log={{ message: warningLog, type: WARNING }}
        classes={classes}
      />
    );
  }
  if (!isDeleteAuthorized) {
    return (
      <ItemLog
        id="itemLog-nonSelectableLog"
        icon={<RemoveCircle className={classes.forbidden} />}
        title={null}
        log={{
          message: formatMessage(intl)(
            R.prop('artefact.relative.non.authorized', messages),
          ),
          type: ERROR,
        }}
        classes={classes}
      />
    );
  }
  return (
    <Checkbox
      className={classes.checkBox}
      checked={isSelected}
      onChange={toggleArtefact}
    />
  );
};
ArtefactDeleteState.propTypes = {
  classes: PropTypes.object,
  cleanUpLog: PropTypes.object,
  deleteLog: PropTypes.object,
  intl: PropTypes.object,
  isDeleteAuthorized: PropTypes.bool,
  isDeleting: PropTypes.bool,
  isSelected: PropTypes.bool,
  nonSelectableLog: PropTypes.string,
  toggleArtefact: PropTypes.func,
  warningLog: PropTypes.string,
  space: PropTypes.object,
};

const Artefact = ({
  intl,
  classes,
  artefact = {},
  isDeleteAuthorized,
  toggleArtefact,
  userRights,
  mode,
  selection,
  deletingIds,
  deleteLogs,
  cleanUpLogs,
}) => {
  const {
    isMainArtefact,
    label,
    space,
    type,
    code,
    agencyId,
    version,
    isFinal,
    children = [],
    isError = false,
  } = artefact;
  const { url, headers } = getRequestArgs({
    identifiers: { agencyId, code, version },
    type,
    format: 'xml',
    datasource: R.assoc('url', space.endpoint, space),
    withUrn: false,
  });
  const onClick = () => {
    apiManager
      .get(url, { headers: withAuthHeader(space)(headers) })
      .then(response => response.data)
      .then(xml => {
        let blob = new Blob([xml], { type: 'text/xml' });
        let url = URL.createObjectURL(blob);
        window.open(url, '_blank');
        URL.revokeObjectURL(url);
      });
  };
  return (
    <div>
      <div
        className={isMainArtefact ? classes.mainDetails : classes.details}
        isMainArtefact={isMainArtefact}
      >
        {mode === 'delete' && (
          <ArtefactDeleteState
            classes={classes}
            cleanUpLog={R.prop(artefact.id, cleanUpLogs || {})}
            deleteLog={R.prop(artefact.id, deleteLogs || {})}
            isDeleteAuthorized={isDeleteAuthorized}
            intl={intl}
            isDeleting={R.has(artefact.id, deletingIds)}
            isSelected={R.includes(artefact.id, selection)}
            nonSelectableLog={getNonSelectableLog(intl, artefact)}
            warningLog={getWarningLog(intl, artefact, selection)}
            toggleArtefact={
              R.isNil(toggleArtefact) ? null : () => toggleArtefact(artefact)
            }
            space={space}
          />
        )}
        {isError && mode === 'visualisation' && (
          <ItemLog
            id="itemLog-warningLog"
            icon={<ReportProblem className={classes.warning} />}
            title={null}
            log={{
              message: formatMessage(intl)(
                R.prop('artefact.relative.msd.missing.warning', messages),
              ),
              type: WARNING,
            }}
            classes={classes}
          />
        )}
        <Space
          {...space}
          label={formatMessage(intl)(
            R.propOr({ id: type }, `artefact.type.${type}`, messages),
          )}
        />
        <span style={{ marginLeft: 5 }}>
          {isError ? (
            label
          ) : (
            <Link onClick={onClick} underline="none">
              {label}
            </Link>
          )}
        </span>
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <span>[{code}]</span>
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <span>[{version}]</span>
        {isFinal && (
          <span>
            <span className={Classes.NAVBAR_DIVIDER}></span>
            <Icon icon="tick" />
            <span style={{ marginLeft: 5 }}>
              {formatMessage(intl)(messages['artefact.is.final'])}
            </span>
          </span>
        )}
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <span>{agencyId}</span>
      </div>
      <div style={{ marginLeft: 25 }}>
        {R.map(
          child => (
            <Artefact
              artefact={child}
              intl={intl}
              classes={classes}
              key={child.id}
              toggleArtefact={toggleArtefact}
              isDeleteAuthorized={isArtefactDeleteAuthorized(child, userRights)}
              mode={mode}
              userRights={userRights}
              selection={selection}
              deletingIds={deletingIds}
              deleteLogs={deleteLogs}
              cleanUpLogs={cleanUpLogs}
            />
          ),
          children,
        )}
      </div>
    </div>
  );
};

const getIsSelectable = artefact => {
  return (
    !R.propOr(false, 'isError', artefact) &&
    R.isEmpty(R.propOr([], 'sdmxParents', artefact)) &&
    !R.propOr(false, 'isMsdLinked', artefact) &&
    !R.prop('nonSelectableChild', artefact)
  );
};

export const getAllChildrenIds = (artefacts, deleteLogs = {}) =>
  R.reduce(
    (acc, artefact) => {
      const childrenIds = getAllChildrenIds(
        R.propOr([], 'children', artefact),
        deleteLogs,
      );
      if (!getIsSelectable(artefact) || R.has(artefact.id, deleteLogs)) {
        return R.concat(acc, childrenIds);
      }
      const parentsIds = R.pluck('id', R.propOr([], 'parentsInTree', artefact));
      if (R.isEmpty(R.difference(parentsIds, childrenIds))) {
        return R.concat(acc, R.prepend(artefact.id, childrenIds));
      }
      return R.concat(acc, R.prepend(artefact.id, childrenIds));
    },
    [],
    artefacts,
  );

export default () => {
  const intl = useIntl();
  const classes = useStyles();
  const dispatch = useDispatch();
  const { isLoading, rights } = useGetUserRights({ queryKey: 'my-rights' });
  const errors = useSelector(getErrors);
  const artefactsTree = useSelector(getArtefactRelativesTree);
  const isOpenRelatives = useSelector(getIsOpenRelatives);
  const isFetchingRelatives = useSelector(getIsFetching);
  const isError = useSelector(getIsError);
  const deletingIds = useSelector(getDeletingIds);
  const mode = useSelector(getMode);
  const deleteLogs = useSelector(getDeleteLogs);
  const cleanUpLogs = useSelector(getCleanUpLogs);
  const defaultSelectionIds = useSelector(getSelection);
  const [selection, setSelection] = useState([]);

  useEffect(() => {
    if (!R.isNil(artefactsTree) && !R.isEmpty(artefactsTree)) {
      setSelection(R.values(defaultSelectionIds));
    }
  }, [artefactsTree, defaultSelectionIds]);

  useEffect(() => {
    if (!R.isNil(deletingIds) && !R.isEmpty(deletingIds)) {
      const nextIds = R.difference(selection, deletingIds);
      setSelection(nextIds);
    }
  }, [deletingIds]);
  const title =
    mode === 'delete'
      ? formatMessage(intl)(messages['artefact.delete.relatives'])
      : formatMessage(intl)(messages['view.artefact.relatives']);
  const selectAll = () => {
    const ids = getAllChildrenIds(artefactsTree, deleteLogs);
    setSelection(ids);
  };
  const deselectAll = () => {
    setSelection([]);
  };
  const toggleArtefact = artefact => {
    const childrenIds = getAllChildrenIds(artefact.children || []);
    if (R.includes(artefact.id, selection)) {
      const nextIds = R.difference(
        selection,
        R.append(artefact.id, childrenIds),
      );
      setSelection(nextIds);
    } else {
      const nextIds = R.concat(selection, R.append(artefact.id, childrenIds));
      setSelection(nextIds);
    }
  };
  const isDeleting = !R.isEmpty(deletingIds);
  return (
    <Dialog
      className={classes.dialog}
      icon={mode === 'delete' ? 'trash' : 'layout-hierarchy'}
      isOpen={isOpenRelatives}
      onClose={() => dispatch(cleanArtefactRelatives())}
      title={title}
    >
      <div className={Classes.DIALOG_BODY}>
        <ErrorLogs errors={errors} intl={intl} classes={classes} />
        {(isFetchingRelatives || isLoading) && <Spinner />}
        {!isFetchingRelatives &&
          isError &&
          (R.isNil(artefactsTree) || R.isEmpty(artefactsTree)) && (
            <span>
              {formatMessage(intl)(messages['artefact.relatives.error'])}
            </span>
          )}
        {!R.isNil(artefactsTree) &&
          !R.isEmpty(artefactsTree) &&
          R.map(
            artefact => (
              <Artefact
                artefact={artefact}
                classes={classes}
                intl={intl}
                key={artefact.id}
                toggleArtefact={toggleArtefact}
                userRights={rights}
                isDeleteAuthorized={isArtefactDeleteAuthorized(
                  artefact,
                  rights,
                )}
                mode={mode}
                selection={selection}
                deletingIds={deletingIds}
                deleteLogs={deleteLogs}
                cleanUpLogs={cleanUpLogs}
              />
            ),
            artefactsTree,
          )}
      </div>
      <div className={cx(classes.footer, Classes.DIALOG_FOOTER)}>
        {mode === 'delete' && !isFetchingRelatives && (
          <Button
            onClick={!R.isEmpty(selection) ? deselectAll : selectAll}
            text={
              !R.isEmpty(selection)
                ? formatMessage(intl)(
                    messages['artefact.relatives.deselect.all'],
                  )
                : formatMessage(intl)(messages['artefact.relatives.select.all'])
            }
          />
        )}
        <div>
          {mode === 'delete' && !isFetchingRelatives && (
            <Button
              style={{ marginRight: '10px' }}
              disabled={R.isEmpty(selection)}
              intent={Intent.PRIMARY}
              onClick={() => dispatch(deleteRelativesArtefacts(selection))}
              text={formatMessage(intl)(messages['action.delete'])}
            />
          )}
          {!isFetchingRelatives && (
            <Button
              onClick={
                isDeleting
                  ? () => dispatch(cancelRelativesDelete())
                  : () => dispatch(cleanArtefactRelatives())
              }
              text={
                isDeleting
                  ? formatMessage(intl)(messages['action.cancel'])
                  : formatMessage(intl)(messages['action.close'])
              }
            />
          )}
        </div>
      </div>
    </Dialog>
  );
};
