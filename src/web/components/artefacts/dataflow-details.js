import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { FormattedMessage } from '../../modules/i18n';
import { Button, Classes, Colors, Spinner } from '@blueprintjs/core';
import { styled } from '@mui/material/styles';

const Container = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
});

const ColumnContainer = styled('div')({
  display: 'flex',
  flexDirection: 'column',
});

const DetailsContainer = styled('div')({
  marginBottom: '4px',
});

const RightEntry = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
});

const withSpinner = Component => ({ isFetching, ...rest }) =>
  isFetching ? <Spinner className={Classes.SMALL} /> : <Component {...rest} />;

const Observations = ({ observations, series }) => (
  <div>
    {R.isNil(observations) && R.isNil(series) && (
      <FormattedMessage id="artefact.unknown.observations" />
    )}
    {!R.isNil(observations) && (
      <FormattedMessage id="artefact.observations" values={{ observations }} />
    )}
    {!R.isNil(series) && (
      <FormattedMessage id="artefact.series" values={{ series }} />
    )}
  </div>
);

Observations.propTypes = {
  observations: PropTypes.number,
  series: PropTypes.number,
};

const toLabels = R.map(({ id, name }) => (name ? name : id));

const Details = ({
  details,
  label,
  message,
  accessor,
  valueSeparator,
  color,
}) => {
  if (R.isEmpty(details) || R.isNil(details)) return message;
  return (
    <DetailsContainer>
      <b style={{ color }}>{label}</b>
      {R.pipe(R.map(accessor), R.join(valueSeparator))(details)}
    </DetailsContainer>
  );
};

Details.defaultProps = {
  accessor: R.identity,
  valueSeparator: '; ',
};

Details.propTypes = {
  details: PropTypes.array,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  accessor: PropTypes.func,
  valueSeparator: PropTypes.string,
  color: PropTypes.string,
};

export const DataflowDetails = ({
  categories,
  id,
  isExpanded,
  observations,
  requestDetails,
  details,
}) => {
  if (!isExpanded) {
    return null;
  }

  return (
    <div>
      <Container>
        {withSpinner(Details)({
          isFetching: R.prop('isFetching')(categories),
          details: R.prop('categories')(categories),
          message: (
            <i>
              <FormattedMessage id="dataflow.no.categories" />
            </i>
          ),
          label: (
            <i>
              <FormattedMessage id="dataflow.categories" />:{' '}
            </i>
          ),
          accessor: hierarchy => R.join(' > ', toLabels(hierarchy)),
        })}
        <RightEntry>
          {withSpinner(Observations)(observations)}
          <Button
            className={Classes.MINIMAL}
            icon="refresh"
            style={{
              color: Colors.GRAY1,
            }}
            onClick={() => requestDetails(id)}
          />
        </RightEntry>
      </Container>
      <ColumnContainer>
        {R.addIndex(R.map)((detail, index) => {
          const [label, values] = detail;
          return (
            <Details
              key={`detail-${index}`}
              label={label}
              details={values}
              color={Colors.GRAY2}
              valueSeparator=", "
            />
          );
        })(details)}
      </ColumnContainer>
    </div>
  );
};

DataflowDetails.propTypes = {
  details: PropTypes.array,
  categories: PropTypes.object,
  id: PropTypes.string,
  isExpanded: PropTypes.bool,
  observations: PropTypes.object,
  requestDetails: PropTypes.func.isRequired,
};
