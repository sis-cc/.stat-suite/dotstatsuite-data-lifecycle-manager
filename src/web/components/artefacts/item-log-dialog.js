import {
  Dialog,
  DialogContent,
  DialogContentText,
  IconButton,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { injectIntl } from 'react-intl';
import makeStyles from '@mui/styles/makeStyles';
import i18nMessages from '../messages';

const useStyles = makeStyles(theme => ({
  ERROR: {
    backgroundColor: theme.palette.error.main,
    color: 'white',
    fontSize: 'medium',
  },
  WARNING: {
    backgroundColor: theme.palette.warning.light,
    color: 'black',
    fontSize: 'medium',
  },
  SUCCESS: {
    backgroundColor: theme.palette.success.dark,
    color: 'white',
    fontSize: 'medium',
  },
}));

const ItemLogDialog = ({ id, ariaLabel, title, intl, log, children }) => {
  const classes = useStyles();
  const type = R.prop('type', log);
  const [openDialog, setOpenDialog] = React.useState(false);
  const messages = R.pipe(
    R.ifElse(
      R.always(R.has(log.statusKey, i18nMessages) && !R.isNil(intl)),
      ({ statusKey }) => intl.formatMessage(i18nMessages[statusKey]),
      R.prop('message'),
    ),
    R.when(R.complement(R.is)(Array), R.of),
  )(log);
  return (
    <div>
      <IconButton
        size="small"
        onClick={() => setOpenDialog(true)}
        disableRipple
      >
        {children}
      </IconButton>
      <Dialog
        open={openDialog}
        fullWidth
        maxWidth={'md'}
        onClose={() => setOpenDialog(false)}
        aria-labelledby={ariaLabel}
        scroll={'paper'}
      >
        <DialogContent
          className={classes[type]}
          style={{ margin: '0px 0px 0px 0px', padding: '4px 4px 0px 6px' }}
        >
          <IconButton
            style={{ float: 'right' }}
            size="small"
            color="primary"
            onClick={() => setOpenDialog(false)}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
          <span>{title}</span>
          {R.isEmpty(messages) ? null : (
            <DialogContentText className={classes[type]}>
              {R.addIndex(R.map)((message, idx) => (
                <span style={{ display: 'block' }} key={`${id}-${idx}`}>
                  {message}
                </span>
              ))(messages)}
            </DialogContentText>
          )}
        </DialogContent>
      </Dialog>
    </div>
  );
};
ItemLogDialog.propTypes = {
  id: PropTypes.string.isRequired,
  ariaLabel: PropTypes.string,
  intl: PropTypes.object,
  log: PropTypes.object,
  title: PropTypes.string,
  children: PropTypes.node,
};
export default injectIntl(ItemLogDialog);
