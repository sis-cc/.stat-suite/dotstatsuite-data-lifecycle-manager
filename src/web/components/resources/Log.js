import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Alert, AlertTitle } from '@mui/material';
import { FormattedMessage } from '../../modules/i18n';
import i18nMessages from '../messages';

export const Log = ({ type, messages }) => (
  <Alert severity={type} variant="filled">
    <Fragment>
      <AlertTitle>
        <FormattedMessage {...R.prop(`resources.${type}.log`, i18nMessages)} />
      </AlertTitle>
      {R.addIndex(R.map)(
        (t, i) => (
          <span key={i}>{t}</span>
        ),
        messages,
      )}
    </Fragment>
  </Alert>
);

Log.propTypes = {
  type: PropTypes.string,
  messages: PropTypes.array,
};
