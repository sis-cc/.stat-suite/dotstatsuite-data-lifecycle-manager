import * as R from 'ramda';

export const validateNotEmpty = value => {
  if (R.isNil(value) || R.isEmpty(value)) {
    return 'empty';
  }
  return undefined;
};

const getFieldsLocales = list =>
  R.pipe(R.pluck('locales'), ls => R.unnest(ls || []))(list);

export const validateResourceLocales = ({ links = [], labels = [] }) => {
  const linksLocales = getFieldsLocales(links);
  const linksLocalesSet = new Set(linksLocales);
  const labelsLocales = getFieldsLocales(labels);
  const labelsLocalesSet = new Set(labelsLocales);
  const missingLabelsLocales = R.reject(
    l => labelsLocalesSet.has(l),
    linksLocales,
  );
  const missingLinksLocales = R.reject(
    l => linksLocalesSet.has(l),
    labelsLocales,
  );

  return R.isEmpty(missingLabelsLocales) && R.isEmpty(missingLinksLocales)
    ? null
    : { missingLabelsLocales, missingLinksLocales };
};
