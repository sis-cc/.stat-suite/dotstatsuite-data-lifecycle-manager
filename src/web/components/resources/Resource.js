import React, { Fragment } from 'react';
import * as R from 'ramda';
import { Collapse, Divider } from '@mui/material';
import { LocalisedField } from './LocalizedField';
import { ResourceOverview } from './ResourceOverview';
import PropTypes from 'prop-types';
import { styled } from '@mui/styles';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';

const SCollapse = styled(Collapse)({
  border: '1px solid lightgrey',
  borderRadius: 10,
  backgroundColor: 'white',
  padding: 10,
});

export const Resource = ({
  resource,
  dragHandleProps,
  intl,
  onDelete,
  onEdit,
  locale,
  name,
  error,
  isOpen,
}) => (
  <Fragment>
    <ResourceOverview
      dragHandleProps={dragHandleProps}
      resource={resource}
      onDelete={onDelete}
      onEdit={onEdit}
      locale={locale}
    />
    <SCollapse in={isOpen}>
      <LocalisedField
        name={`${name}.labels`}
        prop="labels"
        missingLocales={R.prop('missingLabelsLocales', error)}
        intl={intl}
      />
      <Divider />
      <LocalisedField
        name={`${name}.links`}
        prop="links"
        missingLocales={R.prop('missingLinksLocales', error)}
        intl={intl}
      />
      <Divider />
      <LocalisedField
        helpMessage={formatMessage(intl)(
          R.prop('resource.icons.help', messages),
        )}
        name={`${name}.icons`}
        prop="icons"
        options={R.pathOr([], ['resources', 'icons'], window.SETTINGS)}
        intl={intl}
      />
    </SCollapse>
  </Fragment>
);

Resource.propTypes = {
  resource: PropTypes.object,
  dragHandleProps: PropTypes.object,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  locale: PropTypes.string,
  name: PropTypes.string,
  intl: PropTypes.object,
  error: PropTypes.object,
  isOpen: PropTypes.bool,
};
