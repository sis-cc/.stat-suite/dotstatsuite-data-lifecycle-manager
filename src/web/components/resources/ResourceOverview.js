import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Button, DialogActions, Typography } from '@mui/material';
import PanToolIcon from '@mui/icons-material/PanTool';
import DoNotTouchIcon from '@mui/icons-material/DoNotTouch';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { FormattedMessage } from '../../modules/i18n';
import messages from '../messages';
import { styled } from '@mui/styles';

const SPanToolIcon = styled(PanToolIcon)({
  width: 20,
  height: 20,
  color: '#5C7080',
});
const SDoNotTouchIcon = styled(DoNotTouchIcon)({
  width: 20,
  height: 20,
  color: '#5C7080',
});

const Summary = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
});

const DragIcon = dragHandleProps => (
  <div {...dragHandleProps}>
    {R.isEmpty(dragHandleProps) ? <SDoNotTouchIcon /> : <SPanToolIcon />}
  </div>
);

export const ResourceOverview = ({
  dragHandleProps,
  resource,
  onDelete,
  onEdit,
  locale,
}) => {
  const label = R.pipe(
    R.find(({ locales }) => R.includes(locale, locales)),
    field => (R.isNil(field) ? R.head(resource.labels || []) : field),
    R.prop('value'),
  )(resource.labels || []);
  return (
    <Summary>
      <Fragment>
        <DragIcon {...dragHandleProps} />
        <Typography variant="subtitle1">
          {R.isNil(label) || R.isEmpty(label) ? (
            <FormattedMessage {...R.prop('resource.untitled', messages)} />
          ) : (
            label
          )}
        </Typography>
      </Fragment>
      <DialogActions>
        <Button startIcon={<EditIcon />} type="button" onClick={onEdit}>
          <FormattedMessage {...R.prop('action.edit', messages)} />
        </Button>
        <Button
          startIcon={<DeleteForeverIcon />}
          type="button"
          onClick={onDelete}
        >
          <FormattedMessage {...R.prop('action.delete', messages)} />
        </Button>
      </DialogActions>
    </Summary>
  );
};

ResourceOverview.propTypes = {
  resource: PropTypes.object,
  dragHandleProps: PropTypes.object,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  locale: PropTypes.string,
};
