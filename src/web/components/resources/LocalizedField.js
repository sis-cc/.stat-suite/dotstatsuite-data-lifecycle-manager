import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Field } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import {
  Alert,
  Button,
  IconButton,
  InputAdornment,
  Menu,
  MenuItem,
  TextField,
  Tooltip,
} from '@mui/material';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import FormatListBulletedOutlinedIcon from '@mui/icons-material/FormatListBulletedOutlined';
import HelpOutlineIcon from '@mui/icons-material/HelpOutlined';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { FormattedMessage, formatMessage } from '../../modules/i18n';
import messages from '../messages';
import { styled } from '@mui/styles';
import { validateNotEmpty } from './utils';

const Title = styled('div')({
  marginTop: 10,
  fontSize: 16,
  alignItems: 'center',
  display: 'flex',
});

const HelpIcon = styled(HelpOutlineIcon)({
  width: 20,
  height: 20,
  marginLeft: 10,
});

const Input = styled(TextField)({
  width: 500,
});

const LocalesInput = styled(TextField)({
  width: 150,
  marginLeft: 10,
});

const LField = styled('div')({
  margin: '10px 0px',
  display: 'flex',
  alignItems: 'center',
});

const Fields = styled('div')({
  display: 'flex',
  flexDirection: 'column',
});

const AddButton = styled(Button)({
  margin: '10px 0px',
  alignSelf: 'flex-end',
});

const Img = styled('img')({
  marginRight: 10,
  height: 20,
});

export const LocalisedField = ({
  name,
  prop,
  intl,
  options,
  missingLocales,
  helpMessage,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const locales = R.pipe(
    R.pathOr({}, ['i18n', 'locales']),
    R.keys,
  )(window.SETTINGS);

  const missingLocalesLabels = R.map(
    id => formatMessage(intl)(R.prop(id, messages)),
    missingLocales || [],
  );

  const iconDisplay = prop === 'icons';

  return (
    <div>
      <Title>
        <span>
          <FormattedMessage {...R.prop(`resource.${prop}`, messages)} />
        </span>
        {helpMessage && (
          <Tooltip title={helpMessage}>
            <HelpIcon />
          </Tooltip>
        )}
      </Title>
      <FieldArray
        name={name}
        validate={prop === 'icons' ? null : validateNotEmpty}
      >
        {({ fields, meta }) => {
          const currentSelectedLocalesSet = R.pipe(
            R.prop('value'),
            R.defaultTo([]),
            R.pluck('locales'),
            R.unnest,
            locales => new Set(locales),
          )(fields);
          const remaining = R.reject(
            locale => currentSelectedLocalesSet.has(locale),
            locales,
          );
          return (
            <Fields>
              {!R.isNil(meta.error) && R.equals('empty', meta.error) && (
                <Alert severity="error">
                  <FormattedMessage
                    {...R.prop(`resource.${prop}.empty`, messages)}
                  />
                </Alert>
              )}
              {prop === 'icons' &&
                (R.isNil(fields.value) || R.isEmpty(fields.value)) && (
                  <Alert severity="warning">
                    <FormattedMessage
                      {...R.prop(`resource.${prop}.empty`, messages)}
                    />
                  </Alert>
                )}
              {!R.isEmpty(missingLocales || []) && (
                <Alert severity="warning">
                  <FormattedMessage
                    {...R.prop(`resource.${prop}.missing.locales`, messages)}
                    values={{ locales: R.join(', ', missingLocalesLabels) }}
                  />
                </Alert>
              )}
              {fields.map((key, ind) => (
                <LField key={key}>
                  <Field
                    name={`${key}.value`}
                    validate={validateNotEmpty}
                    render={({ input, meta }) => (
                      <Fragment>
                        <Input
                          value={input.value}
                          error={!R.isNil(meta.error)}
                          onChange={input.onChange}
                          InputProps={{
                            startAdornment: iconDisplay && (
                              <InputAdornment position="start">
                                <Img src={input.value} />
                              </InputAdornment>
                            ),
                            endAdornment: R.isEmpty(options || []) ? null : (
                              <InputAdornment position="end">
                                <IconButton
                                  onClick={e =>
                                    setAnchorEl(
                                      anchorEl ? null : e.currentTarget,
                                    )
                                  }
                                >
                                  <FormatListBulletedOutlinedIcon />
                                </IconButton>
                              </InputAdornment>
                            ),
                          }}
                        />
                        <Menu
                          anchorEl={anchorEl}
                          open={!R.isNil(anchorEl) && !R.isEmpty(options || [])}
                          onClose={() => setAnchorEl(null)}
                        >
                          {R.map(
                            ({ id, link }) => (
                              <MenuItem
                                key={id}
                                onClick={() => {
                                  input.onChange(link);
                                  setAnchorEl(null);
                                }}
                              >
                                {iconDisplay ? <Img src={link} /> : link}
                              </MenuItem>
                            ),
                            options || [],
                          )}
                        </Menu>
                      </Fragment>
                    )}
                  />
                  <Field
                    name={`${key}.locales`}
                    validate={validateNotEmpty}
                    render={({ input, meta }) => {
                      const selectionSet = new Set(input.value);
                      const isAvailable = locale =>
                        selectionSet.has(locale) ||
                        !currentSelectedLocalesSet.has(locale);
                      const availableLocales = R.filter(isAvailable, locales);
                      return (
                        <LocalesInput
                          select
                          error={!R.isNil(meta.error)}
                          value={input.value}
                          onChange={e =>
                            input.onChange(
                              R.pipe(R.unnest, R.uniq)(e.target.value),
                            )
                          }
                          SelectProps={{
                            multiple: true,
                            renderValue: () => R.join(', ', input.value),
                          }}
                        >
                          <MenuItem
                            key="all"
                            value={availableLocales}
                            disabled={R.isEmpty(remaining)}
                          >
                            <FormattedMessage
                              {...R.prop(
                                'resource.field.all.locales',
                                messages,
                              )}
                            />
                          </MenuItem>
                          {locales.map(id => (
                            <MenuItem
                              key={id}
                              value={id}
                              disabled={!isAvailable(id)}
                            >
                              <FormattedMessage
                                {...R.propOr({ id }, id, messages)}
                              />
                            </MenuItem>
                          ))}
                        </LocalesInput>
                      );
                    }}
                  />
                  <IconButton onClick={() => fields.remove(ind)}>
                    <DeleteForeverIcon />
                  </IconButton>
                </LField>
              ))}
              <AddButton
                startIcon={<AddCircleOutlineIcon />}
                type="button"
                onClick={() => fields.push({ value: '', locales: [] })}
              >
                <FormattedMessage
                  {...R.prop(`resource.${prop}.add`, messages)}
                />
              </AddButton>
            </Fields>
          );
        }}
      </FieldArray>
    </div>
  );
};
LocalisedField.propTypes = {
  name: PropTypes.string,
  prop: PropTypes.string,
  options: PropTypes.object,
  intl: PropTypes.object,
  missingLocales: PropTypes.array,
  helpMessage: PropTypes.string,
};
