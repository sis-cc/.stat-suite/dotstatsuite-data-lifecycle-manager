import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Field, Form } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import arrayMutators from 'final-form-arrays';
import { useIntl } from 'react-intl';
import { Draggable, DragDropContext, Droppable } from '@hello-pangea/dnd';
import { load } from 'xml-mapping';
import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import HelpOutlineIcon from '@mui/icons-material/HelpOutlined';
import { styled } from '@mui/styles';
import { Log } from './Log';
import useGetXMLArtefact from '../../hooks/useGetXMLArtefact';
import { usePostXMLArtefact } from '../../hooks/usePostXMLArtefact';
import { FormattedMessage, getLocale } from '../../modules/i18n';
import messages from '../messages';
import {
  addResourcesToXML,
  getCurrentResources,
} from '../../lib/artefacts/resources';
import { validateResourceLocales } from './utils';
import { Resource } from './Resource';

const StyledForm = styled('form')({
  width: 726,
});

const Spinner = styled(CircularProgress)({
  width: 100,
  height: 100,
});

const MiniSpinner = styled(CircularProgress)({
  width: 20,
  height: 20,
});

const TitleIcon = styled(AttachFileIcon)({
  width: 20,
  height: 20,
  color: '#5C7080',
  marginRight: 10,
});

const HelpIcon = styled(HelpOutlineIcon)({
  width: 20,
  height: 20,
  color: '#5C7080',
  marginLeft: 10,
});

const SDialogTitle = styled(DialogTitle)({
  display: 'flex',
  alignItems: 'center',
  padding: '0px 20px',
  minHeight: 40,
  fontSize: 18,
  fontWeight: 600,
});

const SDialogContent = styled(DialogContent)({
  backgroundColor: '#ebf1f5',
  padding: '20px 20px',
});

export const ManageResources = ({ dataflow = {}, onClose }) => {
  const { isLoading, data, refetch } = useGetXMLArtefact(dataflow, load);
  const mutation = usePostXMLArtefact();
  const locale = useSelector(getLocale());
  const intl = useIntl();
  const helpLink = R.path(['resources', 'helpLink'], window.SETTINGS);
  if (R.isNil(dataflow)) {
    return null;
  }
  const resources = getCurrentResources(data);
  const onSubmit = ({ resources }) => {
    const evolvedDF = addResourcesToXML(data, resources);
    mutation.mutate(
      {
        space: dataflow.space,
        xmlJson: evolvedDF,
        locale: 'en',
      },
      {
        onSuccess: () => refetch(),
      },
    );
  };

  const addResource = ({ values, mutators }) => {
    const length = R.length(R.propOr([], 'resources', values));
    const newResource = {
      id: `resource--${length}`,
      labels: [{ value: '', locales: [] }],
      links: [{ value: '', locales: [] }],
      icons: [],
    };
    return mutators.push('resources', newResource);
  };

  return (
    <Dialog open={true} onClose={onClose} maxWidth="false">
      <SDialogTitle>
        <TitleIcon />
        <FormattedMessage {...R.prop('resources.manage', messages)} />
        {!R.isNil(helpLink) && (
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={helpLink}
            style={{ width: '20px', height: '20px' }}
          >
            <HelpIcon variant="outlined" />
          </a>
        )}
      </SDialogTitle>
      <SDialogContent>
        {isLoading && <Spinner />}
        {!isLoading && (
          <Form
            initialValues={{ resources }}
            onSubmit={onSubmit}
            keepDirtyOnReinitialize={true}
            mutators={{
              ...arrayMutators,
            }}
            render={({ handleSubmit, form, values }) => {
              const [expandedKey, setExpandedKey] = useState(null);

              const onDragEnd = ({ destination, source }) => {
                form.mutators.move(
                  'resources',
                  source.index,
                  destination.index,
                );
              };

              const { valid, pristine } = form.getState();

              return (
                <StyledForm onSubmit={handleSubmit}>
                  {mutation.isSuccess && (
                    <Log
                      type="success"
                      messages={R.path(['data', 'messages'], mutation)}
                    />
                  )}
                  {mutation.isError && (
                    <Log
                      type="error"
                      messages={R.path(['error', 'messages'], mutation)}
                    />
                  )}
                  <DragDropContext onDragEnd={onDragEnd}>
                    <Droppable droppableId="droppable">
                      {drop => (
                        <div ref={drop.innerRef}>
                          {drop.placeholder}
                          <FieldArray name="resources">
                            {({ fields }) =>
                              fields.map((key, i) => (
                                <Draggable
                                  key={key}
                                  draggableId={key}
                                  index={i}
                                  isDragDisabled={!R.isNil(expandedKey)}
                                >
                                  {provided => (
                                    <div
                                      ref={provided.innerRef}
                                      {...provided.draggableProps}
                                    >
                                      <Field
                                        name={key}
                                        validate={validateResourceLocales}
                                        render={({ input, meta }) => (
                                          <Resource
                                            dragHandleProps={{
                                              ...(provided.dragHandleProps ||
                                                {}),
                                              ...(R.length(fields.value) < 2
                                                ? {
                                                    style: {
                                                      visibility: 'hidden',
                                                    },
                                                  }
                                                : {}),
                                            }}
                                            resource={input.value}
                                            onDelete={() =>
                                              form.mutators.remove(
                                                'resources',
                                                i,
                                              )
                                            }
                                            onEdit={() =>
                                              setExpandedKey(
                                                expandedKey === key
                                                  ? null
                                                  : key,
                                              )
                                            }
                                            locale={locale}
                                            intl={intl}
                                            name={key}
                                            error={meta.error}
                                            isOpen={expandedKey === key}
                                          />
                                        )}
                                      />
                                    </div>
                                  )}
                                </Draggable>
                              ))
                            }
                          </FieldArray>
                        </div>
                      )}
                    </Droppable>
                  </DragDropContext>
                  <DialogActions>
                    <Button
                      variant="contained"
                      disabled={mutation.isLoading || !valid || pristine}
                      startIcon={mutation.isLoading ? <MiniSpinner /> : null}
                      onClick={handleSubmit}
                    >
                      <FormattedMessage
                        {...R.prop(
                          mutation.isLoading ? 'action.saving' : 'action.save',
                          messages,
                        )}
                      />
                    </Button>
                    <Button onClick={onClose}>
                      <FormattedMessage
                        {...R.prop('action.cancel', messages)}
                      />
                    </Button>
                  </DialogActions>
                  <Button
                    startIcon={<AddCircleOutlineIcon />}
                    type="button"
                    onClick={() =>
                      addResource({ mutators: form.mutators, values })
                    }
                  >
                    <FormattedMessage {...R.prop('resource.add', messages)} />
                  </Button>
                </StyledForm>
              );
            }}
          />
        )}
      </SDialogContent>
    </Dialog>
  );
};

ManageResources.propTypes = {
  dataflow: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};
