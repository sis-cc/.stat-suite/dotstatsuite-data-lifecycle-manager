import React from 'react';
import RefreshIcon from '@mui/icons-material/Refresh';
import { IconButton } from '@mui/material';
import useTransferLogs from '../../../hooks/useTransferLogs';

const RefreshLogs = () => {
  const { refetch } = useTransferLogs();

  return (
    <IconButton size="small" aria-label="refresh" onClick={refetch}>
      <RefreshIcon />
    </IconButton>
  );
};

export default RefreshLogs;
