import makeStyles from '@mui/styles/makeStyles';
import React, { useMemo } from 'react';
import List from './list';
import useTransferLogs from '../../hooks/useTransferLogs';
import { Pagination, PaginationStatus, Status } from '../list';
import { setPaginationHandler } from '../list/index';
import * as R from 'ramda';
import { Grid } from '@mui/material';
import RefreshLogs from './list/refresh';
import BlankLogs from './list/blank';
import { useSelector } from 'react-redux';
import {
  getSelectedActionIds,
  getSelectedUserEmails,
} from '../../modules/logs/selectors';
import { getComparator, stableSort } from './list/utils';
import { ArtefactsSpinner } from '../artefacts';

const useStyles = makeStyles(theme => ({
  wrapper: {
    paddingTop: theme.spacing(1),
    borderTop: '2px solid #999',
  },
  subWrapper: {
    marginTop: theme.spacing(1),
    borderTop: '1px solid #999',
  },
}));

const Logs = () => {
  const classes = useStyles();

  const { logs, isLoading, isRefetching } = useTransferLogs();

  const pageSize = 25;
  const [page, setPage] = React.useState(0);
  const handleChangePage = newPage => event => {
    event.preventDefault();
    setPage(newPage);
  };

  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('submissionTime');
  const handleSort = prop => {
    const isAsc = orderBy === prop && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(prop);
  };

  const actionIds = useSelector(getSelectedActionIds);
  const selectedUserEmails = useSelector(getSelectedUserEmails);
  const filteredLogs = useMemo(() => {
    return R.filter(log => {
      // filter by user email
      let hasUserEmail =
        selectedUserEmails.size === 0 || selectedUserEmails.has(log.userEmail);

      // filter by action
      const hasActionId = actionIds.has(log.action) || actionIds.size === 0;

      // filter
      return hasActionId && hasUserEmail;
    }, logs);
  }, [logs, actionIds, selectedUserEmails]);

  const sortedLogs = useMemo(() => {
    return stableSort(filteredLogs, getComparator(order, orderBy));
  }, [filteredLogs, order, orderBy]);

  const { pagination, paginatedLogs, needPagination } = useMemo(() => {
    const nextPagination = setPaginationHandler({
      pageSize,
      pageBuffer: 3,
      page,
    })({
      items: sortedLogs,
    });
    return {
      pagination: nextPagination,
      paginatedLogs: sortedLogs.slice(
        nextPagination.page * pageSize,
        nextPagination.page * pageSize + pageSize,
      ),
      needPagination: nextPagination.pages > 1,
    };
  }, [sortedLogs, page, pageSize]);

  const isBlank = R.isEmpty(paginatedLogs);

  if (isLoading) return <ArtefactsSpinner />;
  if (isBlank) return <BlankLogs />;

  return (
    <div className={classes.wrapper}>
      <Grid container spacing={1} justifyContent="space-between">
        <Grid item>
          <RefreshLogs />
        </Grid>
        <Grid item>
          <Status
            messageKey="logs.list.status"
            current={R.length(paginatedLogs)}
            total={R.length(filteredLogs)}
            isFetching={isRefetching}
          />
        </Grid>
      </Grid>
      {needPagination && (
        <Grid container spacing={1} justifyContent="space-between">
          <Grid item>
            <Pagination
              {...pagination}
              page={page}
              pageChange={handleChangePage}
            />
          </Grid>
          <Grid item>
            <PaginationStatus pages={pagination.pages} />
          </Grid>
        </Grid>
      )}
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <div className={classes.subWrapper}>
            <List logs={paginatedLogs} sort={{ order, orderBy, handleSort }} />
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default Logs;
