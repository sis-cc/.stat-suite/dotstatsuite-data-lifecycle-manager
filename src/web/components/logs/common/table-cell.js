import { styled } from '@mui/styles';
import MUITableCell from '@mui/material/TableCell';

export const TableCell = styled(MUITableCell)(({ theme }) => ({
  borderBottom: 'none',
  padding: theme.spacing(0.5),
  paddingLeft: theme.spacing(1),
  paddingRight: theme.spacing(1),
}));

export const TableCellHeader = styled(TableCell)(() => ({
  fontWeight: 'bold',
}));

export const TableCellDetail = styled(TableCell)(({ theme }) => ({
  color: theme.palette.grey[600],
}));

export const TableCellDetailHeader = styled(TableCellHeader)(({ theme }) => ({
  color: theme.palette.grey[600],
}));
