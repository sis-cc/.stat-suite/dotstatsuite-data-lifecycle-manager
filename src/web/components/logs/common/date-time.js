import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { parseJSON, format } from 'date-fns';

const FORMAT = 'dd/MM/yyyy hh:mm:ssa';

const DateTime = ({ isodate }) => {
  if (R.isNil(isodate) || R.isEmpty(isodate)) return null;

  return <span>{format(parseJSON(isodate), FORMAT)}</span>;
};

DateTime.propTypes = {
  isodate: PropTypes.string,
};

export default DateTime;
