import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import TableRow from '@mui/material/TableRow';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import ExecutionStatus from '../log/execution-status';
import ExecutionOutcome from '../log/execution-outcome';
import DateTime from '../common/date-time';
import {
  TableCell,
  TableCellDetail,
  TableCellDetailHeader,
} from '../common/table-cell';
import Details from '../details';
import { useSelector } from 'react-redux';
import { getSpaces } from '../../../modules/config';
import Space from '../../common/space';
import { FormattedMessage } from '../../../modules/i18n';
const Log = ({ data, detailsSpan = 1 }) => {
  const [open, setOpen] = React.useState(false);
  const spaces = useSelector(getSpaces());
  const details = data?.logs;
  const source = data?.sourceDataspace
    ? `${data?.sourceDataspace}: ${data?.sourceData} `
    : data?.sourceData;
  const hasDetails = !R.isEmpty(details);
  const space = spaces[data?.destinationDataspace];
  const hasSpace = !R.isNil(space);

  return (
    <React.Fragment>
      <TableRow hover>
        <TableCell>
          <IconButton
            size="small"
            onClick={() => setOpen(!open)}
            style={{ visibility: hasDetails }}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell>{hasSpace && <Space {...space} />}</TableCell>
        <TableCell>{data?.requestId}</TableCell>
        <TableCell>{data?.action}</TableCell>
        <TableCell>{data?.userEmail}</TableCell>
        <TableCell>{data?.artefact}</TableCell>
        <TableCell>
          <DateTime isodate={data?.submissionTime} />
        </TableCell>
        <TableCell>
          <ExecutionStatus value={data?.executionStatus} />
        </TableCell>
        <TableCell>
          <ExecutionOutcome value={data?.outcome} />
        </TableCell>
        <TableCell>
          <DateTime isodate={data?.executionStart} />
        </TableCell>
        <TableCell>
          <DateTime isodate={data?.executionEnd} />
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ padding: 0 }} />
        <TableCell colSpan={detailsSpan}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            {source && (
              <div>
                <TableCellDetailHeader>
                  <FormattedMessage id={`logs.list.source`} />
                </TableCellDetailHeader>
                <TableCellDetail
                  sx={{ maxWidth: 'min(100vw, 800px)', whiteSpace: 'normal' }}
                >
                  {source}
                </TableCellDetail>
              </div>
            )}
            <Details data={details} />
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
};

Log.propTypes = {
  data: PropTypes.object,
  detailsSpan: PropTypes.number,
};

export default Log;
