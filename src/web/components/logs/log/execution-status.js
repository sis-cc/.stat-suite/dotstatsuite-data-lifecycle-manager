import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Tag, Classes } from '@blueprintjs/core';

const ExecutionStatus = ({ value }) => {
  if (R.isNil(value) || R.isEmpty(value)) return null;

  return <Tag className={Classes.MINIMAL}>{value}</Tag>;
};

ExecutionStatus.propTypes = {
  value: PropTypes.string,
};

export default ExecutionStatus;
