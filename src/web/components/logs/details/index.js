import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import Header from './header';
import Detail from './detail';

const Details = ({ data = [] }) => {
  if (R.isEmpty(data)) return null;

  return (
    <Table size="small" padding="none">
      <Header />
      <TableBody>
        {R.map(
          detail => (
            <Detail key={detail.date} data={detail} />
          ),
          data,
        )}
      </TableBody>
    </Table>
  );
};

Details.propTypes = {
  data: PropTypes.array,
};

export default Details;
