import * as R from 'ramda';
import {
  generateAnnotation,
  getAnnotations,
  getAnnotationTexts,
  getAnnotationType,
  getDataflow,
  parseLocalizedText,
} from '../../sdmx-lib/parsers/xml';

const toArray = content => (R.is(Array, content) ? content : [content]);

const isResourceAnnotation = annot =>
  getAnnotationType(annot) === 'EXT_RESOURCE';

const setLocalizedField = (value, locale) => acc =>
  R.has(value, acc)
    ? R.over(R.lensProp(value), R.append(locale), acc)
    : R.set(R.lensProp(value), [locale], acc);

const format = R.pipe(
  R.toPairs,
  R.map(([value, locales]) => ({ value, locales })),
);

export const getCurrentResources = R.pipe(
  getDataflow,
  getAnnotations,
  R.filter(isResourceAnnotation),
  R.addIndex(R.map)((a, index) =>
    R.pipe(getAnnotationTexts, texts =>
      R.pipe(
        R.reduce(
          (acc, t) => {
            const { locale, text } = parseLocalizedText(t);
            const split = R.split('|', text);
            if (R.length(split) < 2) {
              return acc;
            }
            const [label, link, icon = null] = split;
            return {
              ...acc,
              labels: setLocalizedField(label, locale)(acc.labels),
              links: setLocalizedField(link, locale)(acc.links),
              icons: R.isNil(icon)
                ? acc.icons
                : setLocalizedField(icon, locale)(acc.icons),
            };
          },
          { id: `resource--${index}`, labels: {}, links: {}, icons: {} },
        ),
        ({ id, labels, links, icons }) => ({
          id,
          labels: format(labels),
          links: format(links),
          icons: format(icons),
        }),
      )(texts),
    )(a),
  ),
);

// resource : [{ id, links: [{}], labels: [{}], icons: [{}] }]
export const aggregateFieldsByLocale = resource => {
  const indexInputValuesByLocale = (key, inputs) => acc =>
    R.reduce(
      (_acc, input) => {
        const { value, locales } = input;
        return R.reduce(
          (__acc, locale) => R.assocPath([locale, key], value, __acc),
          _acc,
          locales,
        );
      },
      acc,
      inputs,
    );

  return R.pipe(
    indexInputValuesByLocale('link', resource.links || []),
    indexInputValuesByLocale('label', resource.labels || []),
    indexInputValuesByLocale('icon', resource.icons || []),
  )({});
};

export const formatForAnnotation = resource =>
  R.pipe(
    R.toPairs,
    R.map(([locale, { label, link, icon }]) => {
      const text = R.join(
        '|',
        R.isNil(icon) ? [label, link] : [label, link, icon],
      );
      return { locale, text };
    }),
    texts =>
      generateAnnotation({
        type: 'EXT_RESOURCE',
        texts,
      }),
  )(resource);

export const formatResources = resources =>
  R.map(R.pipe(aggregateFieldsByLocale, formatForAnnotation), resources);

export const addResourcesToXML = (dfXML, resources = []) => {
  return R.over(
    R.lensPath([
      'message$Structure',
      'message$Structures',
      'structure$Dataflows',
      'structure$Dataflow',
      'common$Annotations',
      'common$Annotation',
    ]),
    R.pipe(toArray, R.reject(isResourceAnnotation), annotations =>
      R.concat(formatResources(resources), annotations),
    ),
  )(dfXML);
};
