export const getDfDataExplorerUrl = (artefact, locale) => {
  const { space = {}, agencyId, code, version } = artefact;
  if (!space.dataExplorerUrl) {
    return null;
  }
  const url = new URL(space.dataExplorerUrl);
  const tenant = url.searchParams.get('tenant');
  const origin = url.origin;

  const localeParam = `lc=${locale}`;
  const dataflowParams = `df[ds]=${space.id}&df[ag]=${agencyId}&df[id]=${code}&df[vs]=${version}`;
  const tenantParam = tenant ? `tenant=${tenant}&` : '';

  return `${origin}/vis?${tenantParam}${localeParam}&${dataflowParams}&av=true`;
};
