import { all, takeLatest, put } from 'redux-saga/effects';
import { map, values } from 'ramda';
import { EXPORT_ARTEFACTS, DELETE_ARTEFACTS, TRANSFER_ARTEFACTS } from './action-creators';
import { ARTEFACT_DELETE, ARTEFACT_EXPORT_STRUCTURE } from '../artefacts';
import { TRANSFER_ARTEFACT } from '../transfer-artefact';

export const workerFactory = baseActionType => {
  return function* worker(action) {
    const { ids, ...rest } = action.payload;

    yield all(map(id => put({ type: baseActionType, payload: { id, ...rest } }), values(ids)));
  };
};

export default function* saga() {
  yield takeLatest(DELETE_ARTEFACTS, workerFactory(ARTEFACT_DELETE));
  yield takeLatest(EXPORT_ARTEFACTS, workerFactory(ARTEFACT_EXPORT_STRUCTURE));
  yield takeLatest(TRANSFER_ARTEFACTS, workerFactory(TRANSFER_ARTEFACT));
}
