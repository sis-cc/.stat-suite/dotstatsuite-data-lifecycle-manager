import { createSelector } from 'reselect';
import { keys, path, pick, prop, propEq, reject, values } from 'ramda';
import { getArtefact } from '../common/selectors';
import { getInternalSpaces } from '../config';

export default getState => {
  const selectorHelper = (key, getSubState) => id =>
    createSelector([getSubState(id)], state => prop(key, state));

  const getTransfer = id => createSelector(getState, state => prop(id, state));

  return {
    getIds: () => createSelector(getState, state => keys(state)),
    getTransfer,
    getIsUpdating: () => () => false,
    getIsTransfering: selectorHelper('isTransfering', getTransfer),
    getLog: selectorHelper('log', getTransfer),
    getSpace: selectorHelper('space', getTransfer),
    getSourceSpace: selectorHelper('space', getArtefact),
    getTransferOptions: id =>
      createSelector([getTransfer(id)], transfer => pick(['space'], transfer)),
    getDestinationSpaces: id =>
      createSelector([getArtefact(id), getInternalSpaces()], (artefact, spaces) =>
        reject(propEq('id', path(['space', 'id'], artefact)), values(spaces)),
      ),
  };
};
