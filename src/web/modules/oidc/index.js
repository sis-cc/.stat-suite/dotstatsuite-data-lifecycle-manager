import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const oidcReducer = reducerFactory(model(), handlers);

//export { default as oidcSaga } from './saga';
