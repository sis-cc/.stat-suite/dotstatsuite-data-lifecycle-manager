export const USER_SIGNED_IN = '@@oidc/USER_SIGNED_IN';
export const USER_SIGNED_OUT = '@@oidc/USER_SIGNED_OUT';
export const REFRESH_TOKEN = '@@oidc/REFRESH_TOKEN';

export const userSignedIn = (token, user) => ({ type: USER_SIGNED_IN, token, user });
export const userSignedOut = () => ({ type: USER_SIGNED_OUT });
export const refreshToken = token => ({ type: REFRESH_TOKEN, token });

const userSignedInHandler = (state, action) => ({
  ...state,
  token: action.token,
  user: action.user,
});
const userSignedOutHandler = state => ({ ...state, user: null, token: null });
const refreshTokenHandler = (state, action) => ({ ...state, token: action.token });

export default {
  [USER_SIGNED_IN]: userSignedInHandler,
  [USER_SIGNED_OUT]: userSignedOutHandler,
  [REFRESH_TOKEN]: refreshTokenHandler,
};
