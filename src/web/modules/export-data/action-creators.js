import { propOr } from 'ramda';
import { MODULE } from './constants';
import model from './model';
import { USER_SIGNED_OUT } from '../oidc/action-creators';

export const EXPORT_DATA = `${MODULE}_EXPORT_DATA`;
export const exportData = ({ selection, contentType, dataquery }) => ({
  type: EXPORT_DATA,
  payload: { selection, contentType, dataquery },
});

export const EXPORT_DATAFLOW_DATA = `${MODULE}_EXPORT_DATAFLOW_DATA`;
export const exportDataflowData = id => ({
  type: EXPORT_DATAFLOW_DATA,
  payload: { id },
});
export const exportDataflowDataHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...propOr({}, action.payload.id, state),
    id: action.payload.id,
    isExportingData: true,
    dataError: null,
  },
});

export const EXPORT_DATAFLOW_DATA_SUCCESS = `${MODULE}_EXPORT_DATAFLOW_DATA_SUCCESS`;
export const exportDataflowDataSuccess = id => ({
  type: EXPORT_DATAFLOW_DATA_SUCCESS,
  payload: { id },
});
export const exportDataflowDataSuccessHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...propOr({}, action.payload.id, state),
    isExportingData: false,
    dataError: null,
  },
});

export const EXPORT_DATAFLOW_DATA_EMPTY = `${MODULE}_EXPORT_DATAFLOW_DATA_EMPTY`;
export const exportDataflowDataEmpty = id => ({
  type: EXPORT_DATAFLOW_DATA_EMPTY,
  payload: { id },
});
export const exportDataflowDataEmptyHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...propOr({}, action.payload.id, state),
    isExportingData: false,
    isDataEmpty: true,
  },
});

export const EXPORT_DATAFLOW_DATA_ERROR = `${MODULE}_EXPORT_DATAFLOW_DATA_ERROR`;
export const exportDataflowDataError = (id, error) => ({
  type: EXPORT_DATAFLOW_DATA_ERROR,
  payload: { id, error },
});

export const exportDataflowDataErrorHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...propOr({}, action.payload.id, state),
    isExportingData: false,
    dataError: { type: 'ERROR', ...action.payload.error },
  },
});

export const EXPORT_DATAFLOW_METADATA = `${MODULE}_EXPORT_DATAFLOW_METADATA`;
export const exportDataflowMetadata = id => ({
  type: EXPORT_DATAFLOW_METADATA,
  payload: { id },
});
export const exportDataflowMetadataHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...propOr({}, action.payload.id, state),
    id: action.payload.id,
    isExportingMetadata: true,
    metadataError: null,
  },
});

export const EXPORT_DATAFLOW_METADATA_SUCCESS = `${MODULE}_EXPORT_DATAFLOW_METADATA_SUCCESS`;
export const exportDataflowMetadataSuccess = id => ({
  type: EXPORT_DATAFLOW_METADATA_SUCCESS,
  payload: { id },
});
export const exportDataflowMetadataSuccessHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...propOr({}, action.payload.id, state),
    isExportingMetadata: false,
    metadataError: null,
  },
});

export const EXPORT_DATAFLOW_METADATA_EMPTY = `${MODULE}_EXPORT_DATAFLOW_METADATA_EMPTY`;
export const exportDataflowMetadataEmpty = id => ({
  type: EXPORT_DATAFLOW_METADATA_EMPTY,
  payload: { id },
});
export const exportDataflowMetadataEmptyHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...propOr({}, action.payload.id, state),
    isExportingMetadata: false,
    isMetadataEmpty: true,
  },
});

export const EXPORT_DATAFLOW_METADATA_ERROR = `${MODULE}_EXPORT_DATAFLOW_METADATA_ERROR`;
export const exportDataflowMetadataError = (id, error) => ({
  type: EXPORT_DATAFLOW_METADATA_ERROR,
  payload: { id, error },
});

export const exportDataflowMetadataErrorHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...propOr({}, action.payload.id, state),
    isExportingMetadata: false,
    metadataError: { type: 'ERROR', ...action.payload.error },
  },
});

export default {
  [EXPORT_DATAFLOW_DATA]: exportDataflowDataHandler,
  [EXPORT_DATAFLOW_DATA_ERROR]: exportDataflowDataErrorHandler,
  [EXPORT_DATAFLOW_DATA_SUCCESS]: exportDataflowDataSuccessHandler,
  [EXPORT_DATAFLOW_DATA_EMPTY]: exportDataflowDataEmptyHandler,
  [EXPORT_DATAFLOW_METADATA]: exportDataflowMetadataHandler,
  [EXPORT_DATAFLOW_METADATA_ERROR]: exportDataflowMetadataErrorHandler,
  [EXPORT_DATAFLOW_METADATA_SUCCESS]: exportDataflowMetadataSuccessHandler,
  [EXPORT_DATAFLOW_METADATA_EMPTY]: exportDataflowMetadataEmptyHandler,
  [USER_SIGNED_OUT]: () => model(),
};
