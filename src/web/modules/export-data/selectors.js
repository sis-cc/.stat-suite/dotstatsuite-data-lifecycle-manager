import { createSelector } from 'reselect';
import { propOr } from 'ramda';

const getState = state => state.exportData;

export const getExportData = id => createSelector([getState], state => propOr({}, id, state));

export const getIsExportingData = id =>
  createSelector(
    [getExportData(id)],
    exportState =>
      propOr(false, 'isExportingData', exportState) ||
      propOr(false, 'isExportingMetadata', exportState),
  );

export const getExportDataLog = id =>
  createSelector([getExportData(id)], exportState => {
    if (exportState.isDataEmpty) {
      return { type: 'WARNING', statusKey: 'export.no.data' };
    }
    return propOr(null, 'dataError', exportState);
  });

export const getExportMetadataLog = id =>
  createSelector([getExportData(id)], exportState => {
    if (exportState.isMetadataEmpty) {
      return { type: 'WARNING', statusKey: 'export.no.metadata' };
    }
    return propOr(null, 'metadataError', exportState);
  });
