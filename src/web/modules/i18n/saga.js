import { all, call, put, select, take, takeLatest } from 'redux-saga/effects';
import { isEmpty } from 'ramda';
import { CHANGE_LOCALE } from './action-creators';
import { getArtefacts } from '../artefacts';
import {
  ARTEFACTS_FETCH,
  REFRESH_ARTEFACTS,
} from '../artefacts/action-creators';
import {
  CATEGORIES_FETCH_DONE,
  REFRESH_CATEGORIES,
} from '../categories/action-creators';
import { getSelectionIds } from '../filters';

function* categoriesWatcher() {
  const selectedCategoryIds = yield select(getSelectionIds('categoryIds'));
  if (isEmpty(selectedCategoryIds)) {
    yield put({ type: REFRESH_CATEGORIES });
  } else {
    yield put({ type: REFRESH_CATEGORIES });
    yield take(CATEGORIES_FETCH_DONE);
  }
}

function* changeLocaleWorker() {
  const artefacts = yield select(getArtefacts());
  if (!isEmpty(artefacts)) {
    yield put({ type: ARTEFACTS_FETCH });
  }
  yield all([call(categoriesWatcher)]);
  yield put({ type: REFRESH_ARTEFACTS });
}

export default function* saga() {
  yield takeLatest(CHANGE_LOCALE, changeLocaleWorker);
}
