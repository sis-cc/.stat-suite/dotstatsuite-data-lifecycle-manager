import * as R from 'ramda';
import numeral from 'numeral';
import React from 'react';
import { FormattedMessage as RIFormattedMessage } from 'react-intl';
import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';
import Link from '@mui/material/Link';
export const i18nReducer = reducerFactory(model(), handlers);

export { default as I18nProvider } from './provider';
export { getLocale } from './selectors';
export { CHANGE_LOCALE } from './action-creators';

const modelLocale = locale => `${locale}/${locale}`;

export const setLocale = locale => numeral.locale(modelLocale(locale));

export default ({ locales = [], localeId = 'en' }) => {
  setLocale(localeId);

  R.forEach(locale => {
    const delimiters = R.prop('delimiters')(locale);
    if (R.isNil(delimiters)) return;
    numeral.register('locale', modelLocale(R.prop('id')(locale)), {
      delimiters,
    });
  }, R.values(locales));
};

const richValues = {
  br: <br />, // new line -> {br}
  i: chunks => <i>{chunks}</i>, // italic   -> <i>...</i>
  b: chunks => <b>{chunks}</b>, // bold     -> <b>...</b>
  a: (chunks = []) => {
    const [anchor, href] = R.split('|', R.head(chunks));
    return (
      <Link
        href={href}
        target="_blank"
        rel="noopener noreferrer"
        underline="none"
      >
        {anchor}
      </Link>
    );
  }, // link     -> <a>...</a>
};

export const FormattedMessage = ({ values = {}, ...rest }) => (
  <RIFormattedMessage {...rest} values={{ ...values, ...richValues }} />
);

export const formatMessage = intl => (message, values = {}) =>
  intl.formatMessage(message, { ...richValues, ...values });

export { default as i18nSaga } from './saga';
