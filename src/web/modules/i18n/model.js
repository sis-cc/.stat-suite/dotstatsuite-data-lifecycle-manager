import * as R from 'ramda';

const makeDefaultLocale = ({ locales, localeId }) => {
  const ids = R.keys(locales);
  if (R.includes(localeId, ids)) return localeId;
  return R.head(ids);
};

const locale = makeDefaultLocale(window.SETTINGS.i18n);

// export function: model can be configurable and won't be modified if recalled
export default () => ({
  // current locale
  locale,

  // timezone, useful for dates
  timezone: null,

  // currency, useful for formats
  currency: null,

  // font direction, useful for languages such as arabic
  direction: 'rtl',
});
