import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { IntlProvider } from 'react-intl';
import { getLocale } from './selectors';

const Provider = ({ localeId, messages, children }) => (
  <IntlProvider locale={localeId} key={localeId} messages={messages[localeId]}>
    {React.Children.only(children)}
  </IntlProvider>
);

Provider.propTypes = {
  localeId: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired,
};

export default connect(createStructuredSelector({ localeId: getLocale() }))(Provider);
