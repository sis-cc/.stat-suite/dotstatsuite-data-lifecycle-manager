import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const logsReducer = reducerFactory(model(), handlers);
