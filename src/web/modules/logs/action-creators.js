import { USER_SIGNED_IN } from '../oidc/action-creators';
import { toggleIdHandler } from './utils';

const CHANGE_REQUEST_ID = '@@logs/CHANGE_REQUEST_ID';
export const changeRequestId = id => ({ type: CHANGE_REQUEST_ID, id });
const changeRequestIdHandler = (state, action) => ({
  ...state,
  requestId: action.id,
});

const TOGGLE_ACTION_ID = '@@logs/TOGGLE_ACTION_ID';
export const toggleActionId = id => ({ type: TOGGLE_ACTION_ID, id });

const CHANGE_EXECUTION_STATUS_ID = '@@logs/CHANGE_EXECUTION_STATUS_ID';
export const changeExecutionStatusId = id => ({ type: CHANGE_EXECUTION_STATUS_ID, id });
const changeExecutionStatusIdHandler = (state, action) => ({
  ...state,
  executionStatusId: action.id,
});

const CHANGE_EXECUTION_OUTCOME_ID = '@@logs/CHANGE_EXECUTION_OUTCOME_ID';
export const changeExecutionOutcomeId = id => ({ type: CHANGE_EXECUTION_OUTCOME_ID, id });
const changeExecutionOutcomeIdHandler = (state, action) => ({
  ...state,
  executionOutcomeId: action.id,
});

const CHANGE_STRUCTURE_ID = '@@logs/CHANGE_STRUCTURE_ID';
export const changeStructureId = id => ({ type: CHANGE_STRUCTURE_ID, id });
const changeStructureIdHandler = (state, action) => ({
  ...state,
  structureId: action.id,
});

const TOGGLE_USER_EMAIL = '@@logs/TOGGLE_USER_EMAIL';
export const toggleUserEmail = email => ({ type: TOGGLE_USER_EMAIL, email });
const SET_USER_EMAILS = '@@logs/SET_USER_EMAILS';
export const setUserEmails = emails => ({ type: SET_USER_EMAILS, emails });
export const setUserEmailsHandler = (state, action) => ({
  ...state,
  selectedUserEmails: new Set(action.emails),
});

const CHANGE_SUBMISSION_PERIOD = '@@logs/CHANGE_SUBMISSION_PERIOD';
export const changeSubmissionPeriod = period => ({ type: CHANGE_SUBMISSION_PERIOD, period });
const changeSubmissionPeriodHandler = (state, action) => ({
  ...state,
  submissionPeriod: action.period,
});

export default {
  [CHANGE_REQUEST_ID]: changeRequestIdHandler,
  [TOGGLE_ACTION_ID]: toggleIdHandler('selectedActionIds'),
  [CHANGE_EXECUTION_STATUS_ID]: changeExecutionStatusIdHandler,
  [CHANGE_EXECUTION_OUTCOME_ID]: changeExecutionOutcomeIdHandler,
  [CHANGE_STRUCTURE_ID]: changeStructureIdHandler,
  [TOGGLE_USER_EMAIL]: toggleIdHandler('selectedUserEmails', 'email'),
  [CHANGE_SUBMISSION_PERIOD]: changeSubmissionPeriodHandler,
  // no need of signout event because signin only set current user
  [USER_SIGNED_IN]: (state, action) =>
    setUserEmailsHandler(state, { emails: [action.user?.email] }),
  [SET_USER_EMAILS]: setUserEmailsHandler,
};
