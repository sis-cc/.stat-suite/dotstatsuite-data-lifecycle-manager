import * as R from 'ramda';

export const toggleIdHandler = (propertyIds, propertyId = 'id') => (state, action) => {
  const id = R.prop(propertyId, action);
  if (R.isNil(id)) return { ...state, [propertyIds]: new Set() }; // clear all

  const currentSet = R.prop(propertyIds, state);
  const nextSet = new Set(currentSet);

  if (currentSet.has(id)) nextSet.delete(id);
  else nextSet.add(id);

  return { ...state, [propertyIds]: nextSet };
};
