export default () => ({
  requestId: undefined,
  selectedActionIds: new Set(),
  structureId: undefined,
  selectedUserEmails: new Set(),
  submissionPeriod: undefined,
  executionStatusId: undefined,
  executionOutcomeId: undefined,
});

export const ACTION_DATA_CHANGE = 'data change';
export const ACTION_MAINTENANCE = 'maintenance';
export const ACTION_IDS = [ACTION_DATA_CHANGE, ACTION_MAINTENANCE];

export const EXECUTION_STATUS_IDS = ['Queued', 'InProgress', 'Completed', 'TimedOut', 'Canceled'];

export const EXECUTION_OUTCOME_IDS = ['Success', 'Warning', 'Error', 'None'];
