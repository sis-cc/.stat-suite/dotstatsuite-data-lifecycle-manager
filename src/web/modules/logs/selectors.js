import { createSelector } from 'reselect';
import * as R from 'ramda';

const getState = R.prop('logs');
export const getRequestId = createSelector(getState, R.prop('requestId'));
export const getSelectedActionIds = createSelector(getState, R.prop('selectedActionIds'));
export const getSelectedUserEmails = createSelector(getState, R.prop('selectedUserEmails'));
export const getExecutionStatusId = createSelector(getState, R.prop('executionStatusId'));
export const getExecutionOutcomeId = createSelector(getState, R.prop('executionOutcomeId'));
export const getStructureId = createSelector(getState, R.prop('structureId'));
export const getSubmissionPeriod = createSelector(getState, R.prop('submissionPeriod'));
