import { all, takeEvery, takeLatest, call, put, select } from 'redux-saga/effects';
import { getLocale } from '../i18n';
import {
  artefactFetch,
  artefactFetchSuccess,
  artefactFetchError,
  artefactsFetchDone,
  artefactsFetch,
  artefactsFetchSuccess,
  artefactsFetchError,
  artefactsClear,
  ARTEFACT_DELETE,
  artefactDeleteSuccess,
  artefactDeleteError,
  ARTEFACT_EXPORT_STRUCTURE,
  artefactExportStructureSuccess,
  artefactExportStructureError,
  artefactCleanUp,
  REFRESH_ARTEFACTS,
  REFRESH_ARTEFACT,
} from './action-creators';
import { artefactExportStructure, requestArtefacts } from '../../api/artefacts';
import { dataCleanup } from '../../api/cleanup';
import { getSelection, FILTERS_TOGGLE_IS_FINAL, FILTERS_SPACE_TOGGLE } from '../filters';
import { getArtefactOptions, getTransferArtefactOptions } from '../common/selectors';
import { ERROR, SUCCESS } from '../common/constants';
import { TRANSFER_ARTEFACT_SUCCESS, TRANSFER_ARTEFACT_WARNING } from '../transfer-artefact';
import { deleteStructureRequest } from '../../sdmx-lib';
import { getArtefactsListsRequestsArgs, getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import * as R from 'ramda';
import { withAuthHeader } from '../common/withauth';
import { SET_SPACE_EXT_AUTH_OPTIONS, spaceExtAuthFailure } from '../external-auth/action-creators';

export function* fetchArtefactWorker({ payload }) {
  const id = payload.id;
  yield put(artefactFetch(id));

  try {
    const locale = yield select(getLocale());
    const { url, headers, params } = getRequestArgs({
      identifiers: R.pick(['agencyId', 'code', 'version'], payload),
      datasource: R.assoc('url', R.path(['space', 'endpoint'], payload), payload.space),
      type: payload.type,
      locale,
    });
    const data = yield call(requestArtefacts, {
      url,
      headers: withAuthHeader(payload.space)(headers),
      params,
      locale,
      parsing: { space: payload.space },
    });
    const artefact = R.find(R.propEq('type', payload.type), R.values(data));
    yield put(artefactFetchSuccess(id, artefact));
  } catch (error) {
    yield put(artefactFetchError(id, error));
  }
}

export function* fetchTransferedArtefactWorker(action) {
  const id = action.payload.id;
  const artefactOptions = yield select(getTransferArtefactOptions(id));
  yield call(fetchArtefactWorker, { payload: { id, ...artefactOptions } });
}

export function* fetchArtefactsListWorker(requestArgs) {
  try {
    const artefacts = yield call(requestArtefacts, requestArgs);
    yield put(artefactsFetchSuccess(artefacts, requestArgs.parsing.space));
  } catch (error) {
    if (
      !R.isNil(R.path(['parsing', 'space', 'extAuthOptions'], requestArgs)) &&
      R.pathEq(['response', 'status'], 401, error)
    ) {
      yield put(spaceExtAuthFailure(requestArgs.parsing.space.id));
    }
    yield put(artefactsFetchError(error, requestArgs.parsing.space));
  }
}

export function* fetchAllArtefactsWorker() {
  yield put(artefactsClear());
  const selection = yield select(getSelection());
  if (R.isEmpty(selection.spaces) || R.isEmpty(selection.types)) {
    return;
  }
  yield put(artefactsFetch());
  const locale = yield select(getLocale());
  const requestsArgs = getArtefactsListsRequestsArgs({
    ...selection,
    withoutReferences: true,
    locale,
  });
  yield all(
    R.map(({ headers, parsing, ...args }) => {
      return call(fetchArtefactsListWorker, {
        headers: withAuthHeader(parsing.space)(headers),
        parsing,
        ...args,
        locale,
      });
    }, requestsArgs),
  );
  yield put(artefactsFetchDone());
}

export function* artefactExportStructureWorker(action) {
  const id = action.payload.id;
  const withReferences = action.payload.withReferences;

  try {
    const artefactOptions = yield select(getArtefactOptions(id));
    yield call(artefactExportStructure, artefactOptions, !!withReferences);
    yield put(artefactExportStructureSuccess(id));
  } catch (error) {
    yield put(artefactExportStructureError(id, error));
  }
}

export function* dataCleanUpWorker({ space, id, sdmxId, type }) {
  try {
    const log = yield dataCleanup({
      transferUrl: R.prop('transferUrl', space),
      dataspace: space.id,
      referenceId: sdmxId,
      referenceType: type,
    });
    if (log.type === ERROR) {
      yield put(
        artefactCleanUp({
          id: `${id}:cleanup`,
          message: log.message,
          space,
          type: ERROR,
        }),
      );
    } else {
      yield put(
        artefactCleanUp({
          id: `${id}:cleanup`,
          message: log.message,
          space,
          type: SUCCESS,
        }),
      );
    }
  } catch (error) {
    yield put(
      artefactCleanUp({
        id: `${id}:cleanup`,
        message: [error.message],
        space,
        type: ERROR,
      }),
    );
  }
}

export function* deleteArtefactWorker(action) {
  const id = action.payload.id;

  try {
    const options = yield select(getArtefactOptions(id));
    const { space, sdmxId, type } = options;
    if (type === 'dataflow') {
      yield dataCleanUpWorker({ space, id, sdmxId, type });
    }
    const log = yield call(deleteStructureRequest, { ...options, agency: options.agencyId });
    if (log.isError) {
      yield put(artefactDeleteError(id, { message: log.data, space: options.space }));
    } else {
      if (type === 'datastructure') {
        yield dataCleanUpWorker({ space, id, sdmxId, type });
      }
      yield put(artefactDeleteSuccess(id, log.data, options.space));
    }
  } catch (error) {
    yield put(artefactDeleteError(id, error));
  }
}

export default function* saga() {
  yield takeLatest(action => {
    return (
      action.type === REFRESH_ARTEFACTS ||
      action.type === SET_SPACE_EXT_AUTH_OPTIONS ||
      (/^FILTERS_*/.test(action.type) &&
        action.type !== FILTERS_TOGGLE_IS_FINAL &&
        action.type !== FILTERS_SPACE_TOGGLE)
    );
  }, fetchAllArtefactsWorker);
  yield takeEvery(
    [TRANSFER_ARTEFACT_SUCCESS, TRANSFER_ARTEFACT_WARNING],
    fetchTransferedArtefactWorker,
  );
  yield takeEvery(REFRESH_ARTEFACT, fetchArtefactWorker);
  yield takeEvery(ARTEFACT_DELETE, deleteArtefactWorker);
  yield takeEvery(ARTEFACT_EXPORT_STRUCTURE, artefactExportStructureWorker);
}
