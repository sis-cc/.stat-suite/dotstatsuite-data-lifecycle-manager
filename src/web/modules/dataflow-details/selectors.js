import { createSelector } from 'reselect';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { assoc, isNil, lensProp, over, pipe, pick, propOr } from 'ramda';
import { getArtefactOptions } from '../common/selectors';
import { withAuthHeader } from '../common/withauth';
import { getLocale } from '../i18n/selectors';

const getState = state => state.dataflowDetails;

export const getDetails = id => createSelector(getState, state => state[id]);

export const getCategoriesState = id =>
  createSelector(getDetails(id), state => ({
    ...pick(['categories', 'categoriesError'], state || {}),
    isFetching: propOr(false, 'isFetchingCategories', state),
  }));

export const getObservationsState = id =>
  createSelector(getDetails(id), state => ({
    ...pick(['observations', 'series', 'observationsError'], state || {}),
    isFetching: propOr(false, 'isFetchingObservations', state),
  }));

export const getIsExpanded = id => createSelector(getDetails(id), state => !isNil(state));

export const getDataflowConstraintsRequestArgs = id =>
  createSelector(getArtefactOptions(id), getLocale(), ({ space, ...rest }, locale) => {
    return pipe(
      ({ agencyId, code, space, version }) => ({
        datasource: assoc('url', space.endpoint, space),
        identifiers: { agencyId, code, version },
        type: 'dataflow',
        locale,
        params: { references: 'contentconstraint' },
      }),
      getRequestArgs,
      over(lensProp('headers'), withAuthHeader(space)),
    )({ space, ...rest });
  });

export const getDataflowAvailableConstraintsRequestArgs = id =>
  createSelector(getArtefactOptions(id), getLocale(), ({ space, ...rest }, locale) => {
    return pipe(
      ({ agencyId, code, space, version }) => ({
        datasource: assoc('url', space.endpoint, space),
        identifiers: { agencyId, code, version },
        type: 'availableconstraint',
        locale,
        withReferences: false,
        params: { mode: 'exact' },
      }),
      getRequestArgs,
      over(lensProp('headers'), withAuthHeader(space)),
    )({ space, ...rest });
  });

export const getDataflowCategoriesRequestArgs = id =>
  createSelector(getArtefactOptions(id), getLocale(), ({ space, ...rest }, locale) => {
    return pipe(
      ({ agencyId, code, space, version }) => ({
        datasource: assoc('url', space.endpoint, space),
        identifiers: { agencyId, code, version },
        type: 'dataflow',
        withParentsAndSiblings: true,
        space,
        locale,
      }),
      getRequestArgs,
      over(lensProp('headers'), withAuthHeader(space)),
    )({ space, ...rest });
  });
