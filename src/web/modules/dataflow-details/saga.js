import { takeEvery, call, put, select } from 'redux-saga/effects';
import { isNil, path, pathEq, prop } from 'ramda';
import {
  REQUEST_DETAILS,
  requestCategorisationsError,
  requestCategorisationsSuccess,
  requestObservationsError,
  requestObservationsSuccess,
  deleteAllDetails,
} from './action-creators';
import { ARTEFACTS_CLEAR } from '../artefacts/action-creators';
import { getArtefactOptions } from '../common/selectors';
import {
  getDataflowCategoriesRequestArgs,
  getDataflowConstraintsRequestArgs,
  getDataflowAvailableConstraintsRequestArgs,
} from './selectors';
import {
  getArtefactCategories,
  getConstraintsObservationsCount,
  getConstraintsSeriesCount,
} from '@sis-cc/dotstatsuite-sdmxjs';
import apiManager from '../../apiManager';

const getCount = ({ url, headers, params }) =>
  apiManager
    .get(url, { headers, params })
    .then(prop('data'))
    .then(sdmxJson => {
      const observationsCount = getConstraintsObservationsCount(sdmxJson);
      if (isNil(observationsCount)) {
        const seriesCount = getConstraintsSeriesCount(sdmxJson);
        return { seriesCount };
      } else {
        return { observationsCount };
      }
    });

function* getObservationsThroughAvailableConstraintsWorker(id) {
  try {
    const args = yield select(getDataflowAvailableConstraintsRequestArgs(id));
    const { observationsCount, seriesCount } = yield call(getCount, args);
    if (isNil(observationsCount) && isNil(seriesCount)) {
      throw new Error('cannot retrieve observations count');
    } else {
      yield put(requestObservationsSuccess(id, observationsCount, seriesCount));
    }
  } catch (error) {
    if (pathEq(['response', 'status'], 500, error)) {
      yield put(requestObservationsSuccess(id, 0));
    } else {
      yield put(requestObservationsError(id, error));
    }
  }
}

function* getObservationsThroughConstraintsWorker(id) {
  try {
    const args = yield select(getDataflowConstraintsRequestArgs(id));
    const { observationsCount, seriesCount } = yield call(getCount, args);
    if (isNil(observationsCount) && isNil(seriesCount)) {
      yield call(getObservationsThroughAvailableConstraintsWorker, id);
    } else {
      yield put(requestObservationsSuccess(id, observationsCount, seriesCount));
    }
  } catch (error) {
    yield put(requestObservationsError(id, error));
  }
}

function* getCategoriesWorker(id) {
  const { url, headers, params } = yield select(getDataflowCategoriesRequestArgs(id));
  try {
    const response = yield call(apiManager.get, url, { headers, params });
    const { sdmxId } = yield select(getArtefactOptions(id));
    const categories = getArtefactCategories({
      data: path(['data', 'data'], response),
      artefactId: sdmxId,
    });
    yield put(requestCategorisationsSuccess(id, categories));
  } catch (error) {
    yield put(requestCategorisationsError(id, error));
  }
}

export function* detailsWorker(action) {
  const id = action.payload.id;
  yield call(getCategoriesWorker, id);
  yield call(getObservationsThroughConstraintsWorker, id);
}

export function* resetDetailsWorker() {
  yield put(deleteAllDetails());
}

export default function* saga() {
  yield takeEvery(REQUEST_DETAILS, detailsWorker);
  yield takeEvery(ARTEFACTS_CLEAR, resetDetailsWorker);
}
