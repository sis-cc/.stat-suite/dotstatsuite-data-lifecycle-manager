import reducerFactory from '../../store/reducer-factory';
import handlers, { model } from './action-creators';

export const externalAuthReducer = reducerFactory(model, handlers);
export { default as externalAuthSaga } from './saga';
export { default as withExternalAuth } from './with-external-auth';
