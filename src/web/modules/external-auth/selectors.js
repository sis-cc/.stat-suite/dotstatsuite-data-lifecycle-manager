import { createSelector } from 'reselect';
import * as R from 'ramda';

const getState = state => state.externalAuth;

export const getHasAuthOptions = spaceId => createSelector(getState, R.has(spaceId));

export const getSpaceAuthOptions = spaceId => createSelector(getState, R.prop(spaceId));

export const getMenuSpaceId = createSelector(getState, R.prop('menuSpaceId'));

export const getFailedSpaceIds = createSelector(getState, R.prop('failedIds'));
