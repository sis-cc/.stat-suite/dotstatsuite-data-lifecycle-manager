import * as R from 'ramda';
import { takeEvery, call, put, select } from 'redux-saga/effects';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { upload as uploadData } from '../../api/upload-data';
import { transfer as transferData } from '../../api/transfer-data';
import { ERROR, SUCCESS, WARNING } from '../common/constants';
import { requestMetadata } from '../../api/metadata';
import { SDMX_CSV } from '../upload';
import { getArtefactChildrenWorker } from '../relatives/saga';

export default (actions, selectors) => {
  function* externalTransferDataWorker({
    dataflow,
    dataquery,
    space,
    ...options
  }) {
    const filepath = R.pipe(
      getRequestArgs,
      R.prop('url'),
    )({
      datasource: { url: R.path(['space', 'endpoint'], dataflow) },
      identifiers: dataflow,
      dataquery,
      type: 'data',
    });
    const log = yield call(uploadData, {
      space,
      dataflow,
      byPath: true,
      filepath,
      mode: SDMX_CSV,
      authenticateToRemoteURL: R.path(
        ['space', 'authenticateToRemoteURL'],
        dataflow,
      ),
      ...options,
    });

    return {
      ...log,
      source: R.prop('space', dataflow),
      target: space,
    };
  }

  function* externalTransferMetadataWorker({
    dataflow,
    dataquery,
    ...options
  }) {
    try {
      const supportsMetadata = R.hasPath(['space', 'urlv3'], dataflow);
      if (!supportsMetadata) {
        throw new Error(
          `unsupported referential Metadata retrieval in dataspace ${dataflow.space.id}`,
        );
      }
      const metadataCsv = yield call(requestMetadata, {
        dataflow,
        filename: 'metadata.csv',
        dataquery,
      });
      const log = yield call(uploadData, {
        ...options,
        mode: SDMX_CSV,
        files: [metadataCsv],
      });
      return log;
    } catch (error) {
      return {
        type: ERROR,
        message: error.message,
        statusKey: `transfer-ws.status.${R.path(
          ['response', 'status'],
          error,
        )}`,
      };
    }
  }

  function* externalTransferWorker({ contentType, ...options }) {
    const transferDataLog =
      contentType !== 'metadata'
        ? yield call(externalTransferDataWorker, options)
        : null;
    const transferMetadataLog =
      contentType !== 'data'
        ? yield call(externalTransferMetadataWorker, options)
        : null;

    if (contentType !== 'both') {
      return transferDataLog || transferMetadataLog;
    }
    let globalType = SUCCESS;
    if (transferMetadataLog.type === ERROR && transferDataLog.type === ERROR) {
      globalType = ERROR;
    } else if (
      transferMetadataLog.type === ERROR ||
      transferDataLog.type === ERROR
    ) {
      globalType = WARNING;
    }
    return {
      type: globalType,
      message: [transferDataLog.message, transferMetadataLog.message],
      statusKey: transferMetadataLog.statusKey,
    };
  }

  function* internalTransferWorker({ contentType, dataflow, ...options }) {
    const transferDataflowLevelLog = yield call(transferData, {
      ...options,
      dataflow,
      contentType,
    });

    const childrenArtefacts =
      contentType === 'data'
        ? null
        : yield call(getArtefactChildrenWorker, dataflow);

    const dsd = R.find(
      R.propEq('type', 'datastructure'),
      childrenArtefacts || [],
    );
    const transferDsdLevelLog =
      contentType === 'data' || R.isNil(dsd)
        ? null
        : yield call(transferData, {
            ...options,
            contentType,
            attachment: 'dsd',
            dsd,
          });

    if (contentType === 'data') {
      return transferDataflowLevelLog;
    }
    let globalType = SUCCESS;
    if (
      transferDataflowLevelLog.type === ERROR &&
      transferDsdLevelLog.type === ERROR
    ) {
      globalType = ERROR;
    } else if (
      transferDataflowLevelLog.type === ERROR ||
      transferDsdLevelLog.type === ERROR
    ) {
      globalType = WARNING;
    }
    return {
      type: globalType,
      message: [transferDataflowLevelLog.message, transferDsdLevelLog.message],
      statusKey: transferDataflowLevelLog.statusKey,
    };
  }

  function* transferWorker(action) {
    const id = action.payload.id;
    const dataquery = action.payload.dataQuery;
    const validationOptions = action.payload.validation;

    const { dataflow, space } = yield select(selectors.getTransferOptions(id));
    const options = { dataquery, dataflow, space };

    const destinationTransferUrl = R.path(['space', 'transferUrl'], options);
    const sourceTransferUrl = R.path(
      ['dataflow', 'space', 'transferUrl'],
      options,
    );

    try {
      const log =
        destinationTransferUrl === sourceTransferUrl
          ? yield call(internalTransferWorker, {
              ...options,
              ...validationOptions,
            })
          : yield call(externalTransferWorker, {
              ...options,
              ...validationOptions,
            });

      if (log.type === ERROR) {
        yield put(actions.transferError(id, log));
      } else if (log.type === WARNING) {
        yield put(actions.transferWarning(id, log));
      } else {
        yield put(actions.transferSuccess(id, log));
      }
    } catch (error) {
      yield put(
        actions.transferError(id, {
          type: ERROR,
          message: error.message,
          source: dataflow.space,
          target: space,
        }),
      );
    }
  }

  return function* saga() {
    yield takeEvery(actions.TRANSFER, transferWorker);
  };
};
