// export function: model can be configurable and won't be modified if recalled
export default ({ dropzoneConfig, ...rest }) => ({
  //destination spaces
  spaces: {},

  // files to upload => [File,]
  files: [],

  // upload status
  isUploading: false,

  // log => {type, message, error}
  log: {},

  flushable: false,

  hasDataflowSelection: false,

  // config of the form (could be put elsewhere afterwards)
  dropzoneConfig: {
    // accepted MIME types (comma separated)
    accept: '',

    // arity of upload
    multiple: false,

    // minSize in bytes
    minSize: 1,

    // config overrides
    ...dropzoneConfig,
  },

  ...rest,
});
