import { createSelector } from 'reselect';
import { difference, filter, find, head, identity, isEmpty, indexBy, map } from 'ramda';
import { getLocale } from '../i18n';
import { MIME_TYPE_XML } from '../upload';

export default selectors => {
  const getAcceptedFiles = () =>
    createSelector([selectors.getFiles(), selectors.getMaxSize()], (files, maxSize) =>
      filter(
        identity,
        map(mimeType => find(file => mimeType.includes(file.type) && file.size <= maxSize, files), [
          MIME_TYPE_XML,
        ]),
      ),
    );

  const getRejectedFiles = () =>
    createSelector([selectors.getFiles(), getAcceptedFiles()], (files, acceptedFiles) =>
      difference(files, acceptedFiles),
    );

  return {
    ...selectors,
    getAcceptedFiles,
    getRejectedFiles,
    getUploadOptions: () =>
      createSelector([getAcceptedFiles(), getLocale()], (acceptedFiles, locale) => ({
        file: head(acceptedFiles),
        locale,
      })),
    getKeyedAcceptedFiles: () =>
      createSelector([getAcceptedFiles()], files => indexBy(file => file.name, files)),
    getKeyedRejectedFiles: () =>
      createSelector([getRejectedFiles()], files => indexBy(file => file.name, files)),
    getFormIsValid: () =>
      createSelector(
        [getAcceptedFiles(), selectors.getSpaces()],
        (files, spaces) => !!(!isEmpty(spaces) && !isEmpty(files)),
      ),
  };
};
