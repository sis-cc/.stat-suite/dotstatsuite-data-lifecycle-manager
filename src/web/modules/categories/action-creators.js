import * as R from 'ramda';
import model from './model';
import { USER_SIGNED_OUT } from '../oidc/action-creators';

export const CATEGORIES_FETCH = 'CATEGORIES_FETCH';
export const categoriesFetch = () => ({ type: CATEGORIES_FETCH });
export const categoriesFetchHandler = state => ({
  ...state,
  categories: {},
  errors: {},
  isFetching: true,
});

export const CATEGORIES_FETCH_SUCCESS = 'CATEGORIES_FETCH_SUCCESS';
export const categoriesFetchSuccess = categories => ({
  type: CATEGORIES_FETCH_SUCCESS,
  payload: { categories },
});
export const categoriesFetchSuccessHandler = (state, action) => ({
  ...state,
  categories: {
    ...state.categories,
    ...action.payload.categories,
  },
});

export const CATEGORIES_FETCH_ERROR = 'CATEGORIES_FETCH_ERROR';
export const categoriesFetchError = (spaceId, error) => ({
  type: CATEGORIES_FETCH_ERROR,
  payload: { spaceId, error },
});
export const categoriesFetchErrorHandler = (state, action) => ({
  ...state,
  errors: {
    ...state.errors,
    [action.payload.spaceId]: action.payload.error,
  },
});

export const CATEGORIES_FETCH_DONE = 'CATEGORIES_FETCH_DONE';
export const categoriesFetchDone = () => ({ type: CATEGORIES_FETCH_DONE });
export const categoriesFetchDoneHandler = state => ({
  ...state,
  isFetching: false,
});

export const CATEGORY_EXPAND_TOGGLE = 'CATEGORY_EXPAND_TOGGLE';
export const categoryExpandToggle = category => ({
  type: CATEGORY_EXPAND_TOGGLE,
  payload: { id: category.id },
});
export const categoryExpandToggleHandler = (state, action) => ({
  ...state,
  categories: R.over(
    R.lensPath([action.payload.id, 'isExpanded']),
    R.ifElse(R.isNil, R.always(true), R.not),
  )(state.categories),
});

export const REFRESH_CATEGORIES = 'REFRESH_CATEGORIES';

export default {
  [CATEGORIES_FETCH]: categoriesFetchHandler,
  [CATEGORIES_FETCH_SUCCESS]: categoriesFetchSuccessHandler,
  [CATEGORIES_FETCH_ERROR]: categoriesFetchErrorHandler,
  [CATEGORIES_FETCH_DONE]: categoriesFetchDoneHandler,
  [CATEGORY_EXPAND_TOGGLE]: categoryExpandToggleHandler,
  [USER_SIGNED_OUT]: () => model(),
};
