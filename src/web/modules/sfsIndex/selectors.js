import { createSelector } from 'reselect';
import { path } from 'ramda';

const getState = state => state.sfsIndex;

export const getArtefactIndexationLog = id => createSelector(getState, path([id, 'log']));

export const getArtefactIsIndexing = id => createSelector(getState, path([id, 'isIndexing']));
