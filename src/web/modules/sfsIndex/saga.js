import * as R from 'ramda';
import {
  all,
  call,
  cancel,
  put,
  select,
  takeEvery,
  takeLatest,
} from 'redux-saga/effects';
import { getArtefactOptions } from '../common/selectors';
import apiManager from '../../apiManager';
import {
  SFS_INDEX,
  SFS_INDEX_SELECTION,
  sfsIndexCallback,
  sfsIndex,
} from './action-creators';
import { getIsInternalDataflows } from '../../components/artefacts/actions';

const hasAuthz = R.pipe(
  R.prop('authzServerUrl'),
  R.anyPass([R.isNil, R.isEmpty]),
  R.not,
)(window.CONFIG);

const indexRequest = body => {
  return apiManager.post('/api/sfs/dataflow', body).then(res => {
    return { type: 'SUCCESS', data: res.data };
  });
};

// 128 stands for Structure Edition right
const isAuthorizedCheck = ({ spaceId, id, version, agencyId }) => {
  return apiManager
    .get(`${window.CONFIG.authzServerUrl}/AuthorizationRules/IsAuthorized`, {
      params: {
        dataSpace: spaceId,
        artefactId: id,
        artefactVersion: version,
        artefactAgencyId: agencyId,
        requestedPermission: 128,
      },
    })
    .catch(R.always(false))
    .then(R.pathOr(false, ['data', 'success']));
};

function* indexArtefactWorker(action) {
  try {
    const artefact = yield select(getArtefactOptions(action.payload.id));
    const dataflow = {
      spaceId: R.path(['space', 'id'], artefact),
      id: R.prop('code', artefact),
      version: R.prop('version', artefact),
      agencyId: R.prop('agencyId', artefact),
    };
    const needAuthz = getIsInternalDataflows([artefact]);
    if (hasAuthz && needAuthz) {
      const isAuthorized = yield call(isAuthorizedCheck, dataflow);
      if (!isAuthorized) {
        yield put(
          sfsIndexCallback(action.payload.id, {
            type: 'ERROR',
            statusKey: 'sfs.index.unauthorized',
          }),
        );
        yield cancel();
      }
    }
    const log = yield call(indexRequest, dataflow);
    yield put(sfsIndexCallback(action.payload.id, log));
  } catch (error) {
    let log;
    if (error.response) {
      log = { type: 'ERROR', ...R.pathOr({}, ['response', 'data'], error) };
    } else {
      log = { type: 'ERROR', message: error.message };
    }
    yield put(sfsIndexCallback(action.payload.id, log));
  }
}

function* indexSelectionWorker(action) {
  const { ids } = action.payload;
  yield all(R.map(id => put(sfsIndex(id)), R.values(ids)));
}

export default function* saga() {
  yield takeEvery(SFS_INDEX, indexArtefactWorker);
  yield takeLatest(SFS_INDEX_SELECTION, indexSelectionWorker);
}
