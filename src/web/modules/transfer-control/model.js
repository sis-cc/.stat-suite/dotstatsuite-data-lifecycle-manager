export default () => ({
  artefactId: undefined,
  dataQuery: undefined,
  destinationSpace: undefined,
  hasDataQuery: undefined,
  isOpenMenu: false,
  isSelection: false,
  transferType: undefined, // 'data' or 'artefact'
});
