import { createSelector } from 'reselect';
import { prop, propOr, props } from 'ramda';
import { getArtefacts as getAllArtefacts } from '../artefacts';

const getState = state => propOr({}, 'transferControl', state);

export const getIsOpenMenu = () =>
  createSelector(getState, state => propOr(false, 'isOpenMenu', state));

export const getArtefactIds = createSelector(getState, state => propOr([], 'artefactIds', state));

export const getArtefacts = createSelector(getArtefactIds, getAllArtefacts(), (ids, artefacts) =>
  props(ids, artefacts),
);

export const getTransferType = () => createSelector(getState, state => prop('transferType', state));

export const getHasDataQuery = () =>
  createSelector(getState, state => propOr(false, 'hasDataQuery', state));

export const getDataQuery = () => createSelector(getState, state => prop('dataQuery', state));

export const getDestinationSpace = () =>
  createSelector(getState, state => prop('destinationSpace', state));
