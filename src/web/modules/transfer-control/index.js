import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const transferControlReducer = reducerFactory(model(), handlers);
export { default as withTransferControl } from './with-transfer-control';
export { default as transferControlSaga } from './saga';
export { openItemTransfer, openSelectionTransfer } from './action-creators';
