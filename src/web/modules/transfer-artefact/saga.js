import { takeEvery, call, put, select } from 'redux-saga/effects';
import { transferStructure } from '../../sdmx-lib';
import { ERROR, SUCCESS, WARNING } from '../common/constants';
import { getLocale } from '../i18n';

export default (actions, selectors) => {
  function* transferWorker(action) {
    const id = action.payload.id;
    const withReferences = action.payload.withReferences;

    const artefactOptions = yield select(selectors.getArtefactOptions(id));
    const transferOptions = yield select(selectors.getTransferOptions(id));
    const formatLog = (type, log) => ({
      type,
      message: log.data,
      source: artefactOptions.space,
      target: transferOptions.space,
    });

    try {
      const locale = yield select(getLocale());
      const log = yield call(
        transferStructure,
        { ...artefactOptions, locale, withReferences, agency: artefactOptions.agencyId },
        transferOptions.space,
      );
      if (log.isError) yield put(actions.transferError(id, formatLog(ERROR, log)));
      else if (log.isWarning) yield put(actions.transferWarning(id, formatLog(WARNING, log)));
      else yield put(actions.transferSuccess(id, formatLog(SUCCESS, log)));
    } catch (error) {
      yield put(actions.transferError(id, formatLog(ERROR, { data: error })));
    }
  }

  return function* saga() {
    yield takeEvery(actions.TRANSFER, transferWorker);
  };
};
