import * as R from 'ramda';
import { USER_SIGNED_OUT } from '../oidc/action-creators';

// selection = [];
export const model = () => ({
  id: undefined,
  isArtefactSelection: false,
  categorizing: {},
  isOpenMenu: false,
  logs: {},
  selection: {},
  space: undefined,
  expanded: {},
});

export const ARTEFACT_CATEGORISATION = 'ARTEFACT_CATEGORISATION';
export const artefactCategorisation = ({ id, space }) => ({
  type: ARTEFACT_CATEGORISATION,
  payload: { id, space },
});
export const artefactCategorisationHandler = (state, action) => ({
  ...state,
  id: action.payload.id,
  isOpenMenu: true,
  space: action.payload.space,
});

export const ARTEFACTS_CATEGORISATION = 'ARTEFACTS_CATEGORISATION';
export const artefactsCategorisation = ({ ids, space }) => ({
  type: ARTEFACTS_CATEGORISATION,
  payload: { ids, space },
});
export const artefactsCategorisationHandler = (state, action) => ({
  ...state,
  ids: action.payload.ids,
  isArtefactSelection: true,
  isOpenMenu: true,
  space: action.payload.space,
});

export const CLOSE_ARTEFACT_CATEGORISATION = 'CLOSE_ARTEFACT_CATEGORISATION';
export const closeArtefactCategorisation = () => ({ type: CLOSE_ARTEFACT_CATEGORISATION });
export const closeArtefactCategorisationHandler = () => model();

export const TOGGLE_CATEGORY = 'TOGGLE_CATEGORY';
export const toggleCategory = category => ({
  type: TOGGLE_CATEGORY,
  payload: { category },
});
export const toggleCategoryHandler = (state, action) => {
  const isSelectable = R.pathOr(true, ['payload', 'category', 'isSelectable'], action);
  if (!isSelectable) {
    return state;
  }
  return {
    ...state,
    selection: R.ifElse(
      R.has(action.payload.category.id),
      R.dissoc(action.payload.category.id),
      R.assoc(action.payload.category.id, action.payload.category),
    )(state.selection),
  };
};

export const EXPAND_TOGGLE_CATEGORY = 'EXPAND_TOGGLE_CATEGORY';
export const expandToggleCategory = category => ({
  type: EXPAND_TOGGLE_CATEGORY,
  payload: { id: category.id },
});
export const expandToggleCategoryHandler = (state, action) => ({
  ...state,
  expanded: R.ifElse(
    R.has(action.payload.id),
    R.dissoc(action.payload.id),
    R.assoc(action.payload.id, action.payload.id),
  )(state.expanded),
});

export const CATEGORIZE = 'CATEGORIZE';
export const categorize = () => ({
  type: CATEGORIZE,
});
export const categorizeHandler = state => ({
  ...state,
  isOpenMenu: false,
});

export const CATEGORIZE_ARTEFACT = 'CATEGORIZE_ARTEFACT';
export const categorizeArtefact = id => ({
  type: CATEGORIZE_ARTEFACT,
  payload: { id },
});
export const categorizeArtefactHandler = (state, action) => ({
  ...state,
  categorizing: { ...state.categorizing, [action.payload.id]: true },
});

export const CATEGORIZE_ARTEFACT_DONE = 'CATEGORIZE_ARTEFACT_DONE';
export const categorizeArtefactDone = (id, logs) => ({
  type: CATEGORIZE_ARTEFACT_DONE,
  payload: { id, logs },
});
export const categorizeArtefactDoneHandler = (state, action) => ({
  ...state,
  categorizing: { ...state.categorizing, [action.payload.id]: false },
  logs: { ...state.logs, [action.payload.id]: action.payload.logs },
});

export const CATEGORIZE_DONE = 'CATEGORIZE_DONE';
export const categorizeDone = () => ({ type: CATEGORIZE_DONE });
export const categorizeDoneHandler = state => ({
  ...state,
  selection: {},
  expanded: {},
  space: undefined,
  id: undefined,
  isArtefactSelection: false,
});

export const handlers = {
  [ARTEFACT_CATEGORISATION]: artefactCategorisationHandler,
  [ARTEFACTS_CATEGORISATION]: artefactsCategorisationHandler,
  [TOGGLE_CATEGORY]: toggleCategoryHandler,
  [EXPAND_TOGGLE_CATEGORY]: expandToggleCategoryHandler,
  [CATEGORIZE]: categorizeHandler,
  [CATEGORIZE_ARTEFACT]: categorizeArtefactHandler,
  [CATEGORIZE_ARTEFACT_DONE]: categorizeArtefactDoneHandler,
  [CATEGORIZE_DONE]: categorizeDoneHandler,
  [CLOSE_ARTEFACT_CATEGORISATION]: closeArtefactCategorisationHandler,
  [USER_SIGNED_OUT]: () => model(),
};
