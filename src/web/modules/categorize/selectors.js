import { createSelector } from 'reselect';
import * as R from 'ramda';
import { getCategories, recurse } from '../categories/selectors';

const getState = state => state.categorize;

export const getArtefactId = createSelector(getState, R.prop('id'));

export const getArtefactIds = createSelector(getState, R.prop('ids'));

export const getSpace = createSelector(getState, R.prop('space'));

export const getSelection = createSelector(getState, R.prop('selection'));

export const getExpandedCategoriesIds = createSelector(getState, R.prop('expanded'));

export const getIsOpenCategorisationMenu = createSelector(getState, R.prop('isOpenMenu'));

export const getIsCategorizing = id => createSelector(getState, R.path(['categorizing', id]));

export const getLogs = id => createSelector(getState, R.path(['logs', id]));

export const getIsArtefactSelection = createSelector(getState, R.prop('isArtefactSelection'));

export const getCategoriesTree = createSelector(
  getSpace,
  getCategories(),
  getSelection,
  getExpandedCategoriesIds,
  (space, categories, selection, expandedIds) => {
    if (R.isNil(space)) {
      return [];
    }
    return R.pipe(
      R.filter(cat => cat.spaceId === space.id),
      R.mapObjIndexed(cat => {
        const scheme = R.propOr({}, cat.topParentId, categories);
        const res = {
          ...cat,
          isSelectable: cat.parentId !== '#root',
          isSelected: R.has(cat.id, selection),
          isExpanded: R.has(cat.id, expandedIds),
          scheme,
        };
        return res;
      }),
      R.values,
      R.groupBy(R.pipe(R.prop('parentId'), R.when(R.isNil, R.always('#root')))),
      recurse('#root'),
    )(categories);
  },
);
