import reducerFactory from '../../store/reducer-factory';
import { model, handlers } from './action-creators';

export const categorizeReducer = reducerFactory(model(), handlers);
export { default as categorizeSaga } from './saga';
export { withCategorisation, withCategorisationState } from './with-categorisation';
export { artefactCategorisation, artefactsCategorisation } from './action-creators';
