import * as R from 'ramda';
import { all, takeEvery, call, put, select } from 'redux-saga/effects';
import apiManager from '../../apiManager';
import { USER_SIGNED_IN } from '../oidc/action-creators';
import { registerAuthzUrls } from './action-creators';
import { getInternalSpaces } from './selectors';

const getAuthz = ({ url, spaces }) => {
  return apiManager
    .get(R.replace(/\/[\w.\d]+$/, '/health', url))
    .catch(() => {
      return R.reduce(
        (acc, spaceId) => ({ ...acc, [spaceId]: window.CONFIG.authzServerUrl }),
        {},
        spaces,
      );
    })
    .then(R.path(['data', 'payload']))
    .then(payload => {
      const authzServerUrl = R.pathOr(
        window.CONFIG.authzServerUrl,
        ['authService', 'url'],
        payload,
      );
      return R.reduce((acc, spaceId) => ({ ...acc, [spaceId]: authzServerUrl }), {}, spaces);
    });
};

function* getAuthorizationUrlsWorker() {
  const spaces = yield select(getInternalSpaces());
  const transfers = R.pipe(
    R.reduce((acc, space) => {
      const transferUrl = R.prop('transferUrl', space);
      if (R.has(transferUrl, acc)) {
        return R.over(R.lensPath([transferUrl, 'spaces']), R.append(space.id))(acc);
      }
      if (!R.isEmpty(transferUrl)) {
        return R.assoc(transferUrl, { url: transferUrl, spaces: [space.id] }, acc);
      }
      return acc;
    }, {}),
    R.values,
  )(R.values(spaces || {}));

  const authzs = yield all(R.map(transfer => call(getAuthz, transfer), transfers));
  yield put(registerAuthzUrls(R.mergeAll(authzs)));
}

export default function* saga() {
  yield takeEvery(USER_SIGNED_IN, getAuthorizationUrlsWorker);
}
