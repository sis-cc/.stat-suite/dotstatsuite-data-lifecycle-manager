import { all, takeLatest, call, put, select } from 'redux-saga/effects';
import fileSaver from 'file-saver';
import * as R from 'ramda';
import {
  getArtefactsListRequestArgs,
  getRequestArgs,
} from '@sis-cc/dotstatsuite-sdmxjs';
import {
  dumpDone,
  dumpDownloadFileSuccess,
  dumpDownloadFileError,
  dumpDownloadDataflowsError,
  DUMP_REQUEST,
} from './action-creators';
import { getTypes } from '../config';
import { getSelectedSpace } from './selectors';
import { getArtefactsFilename, getDataflowFilename, MIME_TYPES } from './utils';
import { withAuthHeader } from '../common/withauth';
import apiManager from '../../apiManager';
import { requestMetadata } from '../../api/metadata';
import { sdmxXmlListParser } from '../../sdmx-lib/parsers/xml';

export const download = (data, filename, format) => {
  let blob = new Blob([data], MIME_TYPES[format]);
  fileSaver.saveAs(blob, filename);
};

export function* downloadArtefactsList({ type, space }) {
  const format = 'xml';
  const { url, headers } = getRequestArgs({
    type,
    datasource: R.assoc('url', space.endpoint, space),
    identifiers: { agencyId: 'all', code: 'all', version: 'all' },
    format,
    asFile: true,
  });
  const filename = getArtefactsFilename({ type, format });
  try {
    const response = yield call(apiManager.get, url, {
      headers: withAuthHeader(space)(headers),
    });
    yield call(download, response.data, filename, format);
    yield put(dumpDownloadFileSuccess(filename));
  } catch (error) {
    yield put(dumpDownloadFileError(filename, error));
  }
}

export function* downloadDataflowStructure({ dataflow, space }) {
  const format = 'xml';
  const { agencyId, code, version } = R.pick(
    ['agencyId', 'code', 'version'],
    dataflow,
  );
  const { url, headers, params } = getRequestArgs({
    datasource: R.assoc('url', space.endpoint, space),
    identifiers: { agencyId, code, version },
    type: 'dataflow',
    format,
    asFile: true,
  });
  const filename = getDataflowFilename({
    ...dataflow,
    type: 'structure',
    format,
  });
  try {
    const response = yield call(apiManager.get, url, {
      headers: withAuthHeader(space)(headers),
      params,
    });
    yield call(download, response.data, filename, format);
    yield put(dumpDownloadFileSuccess(filename));
  } catch (error) {
    yield put(dumpDownloadFileError(filename, error));
  }
}

export function* downloadDataflowData({ dataflow, space }) {
  const format = 'csv';
  const { agencyId, code, version } = R.pick(
    ['agencyId', 'code', 'version'],
    dataflow,
  );
  const { url, headers, params } = getRequestArgs({
    datasource: R.assoc('url', space.endpoint, space),
    identifiers: { agencyId, code, version },
    type: 'data',
    format,
    asZip: true,
  });
  const filename = getDataflowFilename({
    ...dataflow,
    type: 'data',
    format: 'zip',
  });
  try {
    const response = yield call(apiManager.get, url, {
      headers: withAuthHeader(space)(headers),
      params,
      responseType: 'blob',
    });
    yield call(download, response.data, filename, 'zip');
    yield put(dumpDownloadFileSuccess(filename));
  } catch (error) {
    yield put(dumpDownloadFileError(filename, error));
  }
}

export function* downloadDataflowMetadata({ dataflow, space }) {
  const { agencyId, code, version } = dataflow;
  const identifiers = `${agencyId}_${code}(${version})`;
  const timestamp = new Date().toUTCString();
  const filename = `refmetadata-${identifiers}-${timestamp}.zip`;
  try {
    const file = yield call(requestMetadata, {
      dataflow: { ...dataflow, space },
      dataquery: null,
      filename,
      asZip: true,
    });
    yield call(download, file, filename, 'zip');
    yield put(dumpDownloadFileSuccess(filename));
  } catch (error) {
    yield put(dumpDownloadFileError(filename, error));
  }
}

export function* downloadDataflow({ dataflow, space }) {
  yield call(downloadDataflowStructure, { dataflow, space });
  yield call(downloadDataflowData, { dataflow, space });
  if (R.has('urlv3', space)) {
    yield call(downloadDataflowMetadata, { dataflow, space });
  }
}

const request = ({ url, headers, params }) => {
  return apiManager.get(url, { headers, params }).then(res => ({
    xml: res.data,
    dataflows: sdmxXmlListParser(res.data),
  }));
};

function* reccurDownloadDataflowsWorker(dataflows, space) {
  if (R.isEmpty(dataflows)) {
    return;
  }
  yield call(downloadDataflow, { dataflow: R.head(dataflows), space });
  yield call(reccurDownloadDataflowsWorker, R.tail(dataflows), space);
}

export function* downloadDataflows(space) {
  const args = getArtefactsListRequestArgs({
    agencyId: 'all',
    code: 'all',
    datasource: { url: space.endpoint, id: space.id },
    type: 'dataflow',
    version: 'all',
    format: 'xml',
  });
  try {
    const { dataflows, xml } = yield call(request, args);
    yield call(reccurDownloadDataflowsWorker, R.values(dataflows), space);
    const filename = getArtefactsFilename({ type: 'dataflow', format: 'xml' });
    yield call(download, xml, filename, 'xml');
  } catch (error) {
    yield put(dumpDownloadDataflowsError(error));
  }
}

export function* dumpDownloadWorker() {
  const types = yield select(getTypes());
  const space = yield select(getSelectedSpace());
  yield all(
    R.map(
      type => call(downloadArtefactsList, { space, type }),
      R.pipe(R.omit(['dataflow']), R.keys)(types),
    ),
  );
  yield call(downloadDataflows, space);
  yield put(dumpDone());
}

export default function* saga() {
  yield takeLatest(DUMP_REQUEST, dumpDownloadWorker);
}
