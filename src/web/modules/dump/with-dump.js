import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getSelectedSpaceId, getIsDownloading } from './selectors';
import { dumpRemoveSpace, dumpRequest, dumpReset, dumpSelectSpace } from './action-creators';

export default Component => {
  const mapStateToProps = createStructuredSelector({
    isDownloading: getIsDownloading(),
    spaceId: getSelectedSpaceId(),
  });

  const mapDispatchToProps = {
    dumpRemoveSpace,
    dumpRequest,
    dumpReset,
    dumpSelectSpace,
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
