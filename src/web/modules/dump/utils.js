import * as R from 'ramda';

const timestamp = () => new Date().toUTCString();

export const getDataflowFilename = ({ type, sdmxId, format }) => {
  const _type = R.when(R.equals('structure'), R.always('dataflow'))(type);
  return `${_type}-${sdmxId}-${timestamp()}.${format}`;
};

export const getArtefactsFilename = ({ type, format }) => `${type}s-${timestamp()}.${format}`;

export const MIME_TYPES = {
  xml: { type: 'text/html' },
  csv: { type: 'text/csv' },
  zip: { type: 'application/zip;charset=utf-8' },
};
