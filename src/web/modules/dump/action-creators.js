import model from './model';
import { USER_SIGNED_OUT } from '../oidc/action-creators';

export const DUMP_REQUEST = 'DUMP_REQUEST';
export const DUMP_DONE = 'DUMP_DONE';
export const DUMP_SELECT_SPACE = 'DUMP_SELECT_SPACE';
export const DUMP_RESET = 'DUMP_RESET';
export const DUMP_REMOVE_SPACE = 'DUMP_REMOVE_SPACE';
export const DUMP_DOWNLOAD_FILE_SUCCESS = 'DUMP_DOWNLOAD_FILE_SUCCESS';
export const DUMP_DOWNLOAD_FILE_ERROR = 'DUMP_DOWNLOAD_FILE_ERROR';
export const DUMP_DOWNLOAD_DATAFLOWS_ERROR = 'DUMP_DOWNLOAD_DATAFLOWS_ERROR';

export const dumpRequest = () => ({ type: DUMP_REQUEST });

export const dumpDone = () => ({ type: DUMP_DONE });

export const dumpSelectSpace = space => ({ type: DUMP_SELECT_SPACE, payload: { id: space.id } });
export const dumpRemoveSpace = space => ({ type: DUMP_REMOVE_SPACE, payload: { id: space.id } });

export const dumpReset = () => ({ type: DUMP_RESET });

export const dumpDownloadFileSuccess = file => ({
  type: DUMP_DOWNLOAD_FILE_SUCCESS,
  payload: { file },
});
export const dumpDownloadFileError = (file, error) => ({
  type: DUMP_DOWNLOAD_FILE_ERROR,
  payload: { file, error },
});
export const dumpDownloadDataflowsError = error => ({
  type: DUMP_DOWNLOAD_DATAFLOWS_ERROR,
  payload: { error },
});

export const dumpRequestHandler = state => ({
  ...state,
  isDownloading: true,
});

export const dumpDoneHandler = state => ({
  ...state,
  isDownloading: false,
});

export const dumpSelectSpaceHandler = (state, action) => ({
  ...state,
  spaceId: action.payload.id,
});

export const dumpRemoveSpaceHandler = state => ({
  ...state,
  spaceId: undefined,
});

export const dumpResetHandler = () => model();

export default {
  [DUMP_REQUEST]: dumpRequestHandler,
  [DUMP_DONE]: dumpDoneHandler,
  [DUMP_SELECT_SPACE]: dumpSelectSpaceHandler,
  [DUMP_REMOVE_SPACE]: dumpRemoveSpaceHandler,
  [DUMP_RESET]: dumpResetHandler,
  [USER_SIGNED_OUT]: () => model(),
};
