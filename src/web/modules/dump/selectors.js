import { createSelector } from 'reselect';
import * as R from 'ramda';
import { getSpaces } from '../config';

const getState = R.prop('dump');

export const getIsDownloading = () =>
  createSelector(
    getState,
    R.prop('isDownloading'),
  );

export const getSelectedSpaceId = () =>
  createSelector(
    getState,
    R.prop('spaceId'),
  );

export const getSelectedSpace = () =>
  createSelector(
    getSelectedSpaceId(),
    getSpaces(),
    R.prop,
  );
