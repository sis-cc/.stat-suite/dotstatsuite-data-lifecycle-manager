import { createSelector } from 'reselect';
import * as R from 'ramda';
import {
  XML_MIME_TYPES,
  MIME_TYPE_XLSX,
  MIME_TYPE_CSV,
  MIME_TYPE_ZIP,
  EXCEL_EDD,
  SDMX_CSV,
} from '../upload';

const XLXS_MIME_TYPES = [MIME_TYPE_XLSX, ...XML_MIME_TYPES];
const SDMX_MIME_TYPES = [
  ...XML_MIME_TYPES,
  ...R.split(',', MIME_TYPE_CSV),
  ...R.split(',', MIME_TYPE_ZIP),
];

export default selectors => {
  const getByPath = () =>
    createSelector(selectors.getState, state => state.byPath);
  const getFilePath = () =>
    createSelector(selectors.getState, state => state.filepath);
  const getMode = () =>
    createSelector(
      [selectors.getFiles(), getByPath(), getFilePath()],
      (files, byPath, filepath = '') => {
        const fileTypes = R.uniq(R.pluck('type', files));
        const isXlsxValid =
          R.isEmpty(R.difference(fileTypes, XLXS_MIME_TYPES)) &&
          R.length(files) === 2 &&
          R.length(fileTypes) === 2;
        if (isXlsxValid) return EXCEL_EDD;

        const isXmlValid =
          R.isEmpty(R.difference(fileTypes, SDMX_MIME_TYPES)) &&
          R.length(files) === 1;
        if (isXmlValid || (byPath && !R.isEmpty(filepath))) return SDMX_CSV;
        return null;
      },
    );

  const getAcceptedFiles = () =>
    createSelector(
      [selectors.getFiles(), selectors.getMaxSize(), getMode()],
      (files, maxSize, mode) => {
        let xlsxFile;
        let sdmxFile;
        if (mode === EXCEL_EDD) {
          xlsxFile = R.find(
            file => R.includes(file.type, MIME_TYPE_XLSX),
            files,
          );
          sdmxFile = R.find(
            file => R.includes(file.type, XML_MIME_TYPES),
            files,
          );
        }
        if (mode === SDMX_CSV) {
          sdmxFile = R.find(
            file => R.includes(file.type, SDMX_MIME_TYPES),
            files,
          );
        }
        return R.reject(R.isNil)([
          xlsxFile?.size <= maxSize ? xlsxFile : null,
          sdmxFile?.size <= maxSize ? sdmxFile : null,
        ]);
      },
    );

  const getRejectedFiles = () =>
    createSelector(
      [selectors.getFiles(), getAcceptedFiles()],
      (files, acceptedFiles) => R.difference(files, acceptedFiles),
    );

  const getFormIsValid = () =>
    createSelector(
      [
        selectors.getSpaces(),
        getMode(),
        getByPath(),
        getFilePath(),
        selectors.getFiles(),
        selectors.getMaxSize(),
      ],
      (spaces, mode, byPath, filepath, files, maxSize) => {
        const hasSource = byPath
          ? !R.isNil(filepath) && !R.isEmpty(filepath)
          : !R.isEmpty(files);
        return R.all(R.equals(true))([
          hasSource,
          !R.isEmpty(spaces),
          !R.isNil(mode),
          R.all(size => size <= maxSize, R.pluck('size', files)),
        ]);
      },
    );

  const getUploadOptions = () =>
    createSelector(
      [selectors.getFiles(), getByPath(), getFilePath(), getMode()],
      (files, byPath, filepath, mode) => ({
        files,
        byPath,
        filepath,
        mode,
      }),
    );

  return {
    ...selectors,
    getMode,
    getDropzoneConfig: () =>
      createSelector([selectors.getDropzoneConfig()], config => ({
        ...config,
        multiple: true, // false by default
        accept: '.xml, .xlsx, .xslt, .xbl, .xsl, .csv, .zip',
      })),
    getFormIsValid,
    getByPath,
    getFilePath,
    getUploadOptions,
    getKeyedAcceptedFiles: () =>
      createSelector(getAcceptedFiles(), R.indexBy(R.prop('name'))),
    getKeyedRejectedFiles: () =>
      createSelector(getRejectedFiles(), R.indexBy(R.prop('name'))),
    getHasPathOption: () => createSelector([], () => true),
  };
};
