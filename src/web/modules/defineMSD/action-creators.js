export const FETCH_ALL_MSD = 'FETCH_ALL_MSD';
export const FETCH_ALL_MSD_SUCCESS = 'FETCH_ALL_MSD_SUCCESS';
export const FETCH_ALL_MSD_ERROR = 'FETCH_ALL_MSD_ERROR';

export const fetchAllMSD = dsds => ({
  type: FETCH_ALL_MSD,
  payload: { dsds },
});

const fetchAllMSDHandler = (state, action) => ({
  ...state,
  dsds: action.payload.dsds,
  msds: {},
  isFetchingMsds: true,
  isOpen: true,
  msdsLog: null,
});

export const fetchAllMSDSuccess = msds => ({
  type: FETCH_ALL_MSD_SUCCESS,
  payload: { msds },
});

const fetchAllMSDSuccessHandler = (state, action) => ({
  ...state,
  isFetchingMsds: false,
  msds: action.payload.msds,
});

export const fetchAllMSDError = error => ({
  type: FETCH_ALL_MSD_ERROR,
  payload: { error },
});

const fetchAllMSDErrorHandler = (state, action) => ({
  ...state,
  isFetchingMsds: false,
  msdsLog: action.payload.error,
});

export const LINK_ALL_MSD = `LINK_ALL_MSD`;
export const LINK_MSD = `LINK_MSD`;
export const LINK_MSD_CALLBACK = `LINK_MSD_CALLBACK`;

export const linkAllMsd = msd => ({
  type: LINK_ALL_MSD,
  payload: { msd },
});
const linkAllMSDHandler = state => ({
  ...state,
  isOpen: false,
  msds: [],
  msdsLog: null,
});

export const linkMsd = id => ({
  type: LINK_MSD,
  payload: { id },
});
const linkMsdHandler = (state, action) => ({
  ...state,
  links: {
    ...state.links,
    [action.payload.id]: {
      id: action.payload.id,
      isLinking: true,
      log: null,
    },
  },
});

export const linkMsdCallback = (id, log) => ({
  type: LINK_MSD_CALLBACK,
  payload: { id, log },
});
const linkMsdCallbackHandler = (state, action) => ({
  ...state,
  links: {
    ...(state.links || {}),
    [action.payload.id]: {
      id: action.payload.id,
      isLinking: false,
      log: action.payload.log,
    },
  },
});

export const model = {
  isFetchingMsds: false,
  isOpen: false,
  msds: [],
  msdsLog: null,
  dsds: null,
  links: {},
  initDataflows: {},
};

export const INIT_DATAFLOWS = 'INIT_DATAFLOWS';
export const initDataflows = id => ({
  type: INIT_DATAFLOWS,
  payload: { id },
});
const initDataflowsHandler = (state, action) => ({
  ...state,
  initDataflows: {
    ...(state.initDataflows || {}),
    [action.payload.id]: {
      isInitializing: true,
      logs: null,
    },
  },
});
export const INIT_DATAFLOWS_CALLBACK = 'INIT_DATAFLOWS_CALLBACK';
export const initDataflowsCallback = (id, logs) => ({
  type: INIT_DATAFLOWS_CALLBACK,
  payload: { id, logs },
});
const initDataflowsCallbackHandler = (state, action) => ({
  ...state,
  initDataflows: {
    ...(state.initDataflows || {}),
    [action.payload.id]: {
      isInitializing: false,
      logs: action.payload.logs,
    },
  },
});

export const CLOSE_MSD_DIALOG = `CLOSE_MSD_DIALOG`;
export const closeMSDDialog = () => ({ type: CLOSE_MSD_DIALOG });
const closeMSDDialogHandler = () => ({
  ...model,
  isOpen: false,
});

export default {
  [FETCH_ALL_MSD]: fetchAllMSDHandler,
  [FETCH_ALL_MSD_SUCCESS]: fetchAllMSDSuccessHandler,
  [FETCH_ALL_MSD_ERROR]: fetchAllMSDErrorHandler,
  [CLOSE_MSD_DIALOG]: closeMSDDialogHandler,
  [LINK_ALL_MSD]: linkAllMSDHandler,
  [LINK_MSD]: linkMsdHandler,
  [LINK_MSD_CALLBACK]: linkMsdCallbackHandler,
  [INIT_DATAFLOWS]: initDataflowsHandler,
  [INIT_DATAFLOWS_CALLBACK]: initDataflowsCallbackHandler,
};
