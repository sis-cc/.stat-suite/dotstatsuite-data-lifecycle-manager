import reducerFactory from '../../store/reducer-factory';
import handlers, { model } from './action-creators';

export const defineMsdReducer = reducerFactory(model, handlers);
export { default as defineMSDSaga } from './saga';
