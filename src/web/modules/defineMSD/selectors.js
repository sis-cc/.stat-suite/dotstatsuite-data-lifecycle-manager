import { createSelector } from 'reselect';
import * as R from 'ramda';

const getState = state => R.propOr({}, 'defineMsd', state);

export const getIsOpen = createSelector(getState, R.prop('isOpen'));
export const getIsLoading = createSelector(getState, R.prop('isFetchingMsds'));
export const getMsds = createSelector(getState, R.prop('msds'));
export const getDsds = createSelector(getState, R.prop('dsds'));
export const getLogs = createSelector(getState, R.propOr({}, 'links'));
export const getInitLogs = createSelector(getState, R.propOr({}, 'initDataflows'));

export const getRefinedMsds = createSelector(getDsds, getMsds, (dsds, msds) => {
  const isFinal = R.any(R.propEq('isFinal', true), R.values(dsds));
  if (!isFinal) {
    return msds;
  }
  return R.filter(R.propEq('isFinal', true), msds);
});

export const getDsdLinkLog = id => createSelector(getLogs, R.prop(id));

export const getInitDataflowsLog = id => createSelector(getInitLogs, R.prop(id));
