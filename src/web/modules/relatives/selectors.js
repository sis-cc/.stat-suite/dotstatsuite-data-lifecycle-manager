import { createSelector } from 'reselect';
import * as R from 'ramda';

const getState = R.prop('relatives');

export const getIsOpenRelatives = createSelector(
  getState,
  R.complement(R.isEmpty),
);

export const getMode = createSelector(getState, R.propOr(null, 'mode'));

export const getIsFetching = createSelector(getState, R.prop('isFetching'));

const getArtefact = createSelector(getState, R.prop('artefact'));

const getAncestors = createSelector(getState, R.propOr([], 'ancestors'));

const getDescendants = createSelector(
  getState,
  R.pipe(R.propOr([], 'descendants'), artefacts => {
    const artefactIds = R.pluck('id', artefacts);
    return R.map(artefact => {
      const [parentsInTree, otherParents] = R.pipe(
        R.propOr([], 'sdmxParents'),
        R.partition(parent => R.includes(parent.id, artefactIds)),
      )(artefact);

      return {
        ...artefact,
        sdmxParents: otherParents,
        parentsInTree,
      };
    }, artefacts);
  }),
);

export const getSelection = createSelector(getState, R.propOr({}, 'selection'));

export const getDeleteLogs = createSelector(
  getState,
  R.propOr({}, 'deleteLogs'),
);

export const getCleanUpLogs = createSelector(
  getState,
  R.propOr({}, 'cleanUpLogs'),
);

export const getDeletingIds = createSelector(
  getState,
  R.propOr({}, 'deleting'),
);

const getIsSelectable = artefact => {
  return (
    !R.propOr(false, 'isError', artefact) &&
    R.isEmpty(R.propOr([], 'sdmxParents', artefact)) &&
    !R.propOr(false, 'isMsdLinked', artefact) &&
    R.isNil(R.prop('deleteLog', artefact))
  );
};

export const getIsError = createSelector(getState, state =>
  R.complement(R.isNil)(state.error),
);

export const getErrors = createSelector(getState, R.prop('errors'));

const getTreeChildren = (parentId, groupedChildren) =>
  R.pipe(
    R.propOr([], parentId),
    R.map(artefact => {
      const children = getTreeChildren(artefact.id, groupedChildren);
      const nonSelectableChild = R.find(
        child => !getIsSelectable(child),
        children,
      );
      return {
        ...artefact,
        children,
        nonSelectableChild: nonSelectableChild,
      };
    }),
  )(groupedChildren);

const addParentsToTree = (tree, groupedParents) => {
  const child = R.head(tree);
  const childId = child.id;
  const parents = R.propOr([], childId, groupedParents);
  if (R.isEmpty(parents)) {
    return tree;
  }
  const isChildSelectable = getIsSelectable(child);
  let _parents = parents;
  if (!isChildSelectable) {
    _parents = R.map(
      parent => ({
        ...parent,
        nonSelectableChild: child,
      }),
      parents,
    );
  }
  const enhancedTree = R.over(
    R.lensIndex(-1),
    R.assoc('children', tree),
  )(_parents);

  return addParentsToTree(enhancedTree, groupedParents);
};

export const getArtefactRelativesTree = createSelector(
  getArtefact,
  getAncestors,
  getDescendants,
  (artefact, ancestors, descendants) => {
    if (R.any(R.isNil)([artefact, ancestors, descendants])) {
      return null;
    }
    const groupedAncestors = {
      ...R.groupBy(R.prop('childId'), ancestors),
      '#ROOT': [{ ...artefact, isMainArtefact: true }],
    };
    const groupedDescendants = R.groupBy(R.prop('parentId'), descendants);
    const ancestorsTree = getTreeChildren('#ROOT', groupedAncestors);

    return addParentsToTree(ancestorsTree, groupedDescendants);
  },
);

export const getGroupedSelectedArtefacts = createSelector(
  getArtefact,
  getAncestors,
  getDescendants,
  (artefact, ancestors, descendants) =>
    R.pipe(
      R.unnest,
      R.groupBy(R.prop('depth')),
    )([ancestors, [{ ...artefact, depth: 0 }], descendants]),
);
