import { all, takeLatest, call, put, select } from 'redux-saga/effects';
import * as R from 'ramda';
import { getLocale } from '../i18n';
import { getArtefact } from '../artefacts';
import {
  getArtefactRelativesError,
  getArtefactRelativesSuccess,
  getArtefactChildrenError,
  getArtefactParentsError,
  parseMsdReferenceError,
  GET_ARTEFACT_RELATIVES,
} from './action-creators';
import { getMsdReference, parseMsdReference } from '../../sdmx-lib';
import {
  requestArtefactChildren,
  requestArtefactParents,
  requestLinkedDsds,
} from '../../api/artefacts';

export function* getArtefactParentsWorker(artefact, locale) {
  try {
    const parents = yield call(requestArtefactParents, { artefact, locale });
    return R.map(
      parent =>
        R.merge(parent, {
          depth: artefact.depth + 1,
          space: artefact.space,
          childId: artefact.id,
          id: `${artefact.space.id}:${parent.type}:${parent.sdmxId}`,
        }),
      parents,
    );
  } catch {
    yield put(getArtefactParentsError(artefact));
    return [];
  }
}

function* getLinkedDsdsWorker(msd, locale) {
  try {
    const dsds = yield call(requestLinkedDsds, { msd, locale });
    return R.map(
      dsd => ({
        ...dsd,
        isMsdLinked: true,
        space: msd.space,
        depth: msd.depth + 1,
        id: `${msd.space.id}:${dsd.type}:${dsd.sdmxId}`,
        childId: msd.id,
      }),
      dsds,
    );
  } catch {
    return [];
  }
}

export function* getArtefactAncestorsWorker(artefact, locale) {
  let parents = yield call(getArtefactParentsWorker, artefact, locale);
  const ancestors = yield all(
    R.map(parent => call(getArtefactAncestorsWorker, parent, locale), parents),
  );
  if (artefact.type === 'metadatastructure') {
    const dsds = yield call(getLinkedDsdsWorker, artefact, locale);
    parents = R.concat(parents, dsds);
  }
  return R.concat(parents, R.unnest(ancestors));
}

export function* getArtefactChildrenWorker(
  artefact,
  locale,
  keepArtefact = false,
) {
  try {
    const children = yield call(requestArtefactChildren, {
      artefact,
      locale,
      keepArtefact,
    });
    return R.map(
      child =>
        R.merge(child, {
          depth: artefact.depth - 1,
          space: artefact.space,
          parentId: artefact.id,
          id: `${artefact.space.id}:${child.type}:${child.sdmxId}`,
        }),
      children,
    );
  } catch {
    yield put(getArtefactChildrenError(artefact));
    return [];
  }
}

export function* getArtefactDescendantsWorker(
  artefact,
  locale,
  withEnhancedChildren,
) {
  if (artefact.isError) return artefact;
  let children = yield call(getArtefactChildrenWorker, artefact, locale);
  if (artefact.type === 'datastructure') {
    const msdRef = getMsdReference(R.propOr([], 'annotations', artefact));
    if (!R.isNil(msdRef) && !R.isEmpty(msdRef)) {
      const msd = parseMsdReference(msdRef);
      if (R.isNil(msd)) {
        yield put(parseMsdReferenceError(artefact, msdRef));
      } else {
        let msdAndChildren = yield call(
          getArtefactChildrenWorker,
          { ...msd, space: artefact.space },
          locale,
          true,
        );
        msdAndChildren = R.map(
          art => ({
            ...art,
            parentId: artefact.id,
            depth: artefact.depth - 1,
          }),
          msdAndChildren,
        );
        children = R.concat(children, msdAndChildren);
      }
    }
  }
  if (withEnhancedChildren) {
    const childrenParents = yield all(
      R.map(
        child =>
          child.type === 'metadatastructure'
            ? call(getLinkedDsdsWorker, child, locale)
            : call(getArtefactParentsWorker, child, locale),
        children,
      ),
    );
    children = R.addIndex(R.map)(
      (child, index) =>
        R.assoc(
          'sdmxParents',
          R.pipe(
            R.nth(index),
            R.filter(parent => parent.id !== artefact.id),
          )(childrenParents),
        )(child),
      children,
    );
  }
  if (R.length(children) > 1) {
    return children;
  }
  const descendants = yield all(
    R.map(
      child =>
        call(getArtefactDescendantsWorker, child, locale, withEnhancedChildren),
      children,
    ),
  );
  return R.concat(children, R.unnest(descendants));
}

/*
 Due to use case of codelist that can be referenced by both conceptScheme(s) and dsd(s) referenced by those
 last(s), ending up to doubles in the tree. In this case better have uniq instances at the lowest node on the tree.
*/
const getUniqAncestors = R.pipe(
  R.groupBy(R.prop('id')),
  R.mapObjIndexed(R.pipe(R.sortBy(R.prop('depth')), R.last)),
  R.values,
);

export function* getArtefactRelativesWorker(action) {
  try {
    const artefact = yield select(getArtefact(action.payload.artefactId));
    const withEnhancedChildren = R.propEq('mode', 'delete', action.payload);
    const locale = yield select(getLocale());
    const ancestors = yield call(
      getArtefactAncestorsWorker,
      { ...artefact, depth: 0 },
      locale,
    );
    const uniqAncestors = getUniqAncestors(ancestors);
    const descendants = yield call(
      getArtefactDescendantsWorker,
      { ...artefact, depth: 0 },
      locale,
      withEnhancedChildren,
    );
    let selection = null;
    if (R.propEq('mode', 'delete', action.payload)) {
      const selectableAncestors = R.filter(
        art => !art.isMsdLinked,
        uniqAncestors,
      );
      selection = R.pipe(
        R.when(
          list => R.length(list) === R.length(uniqAncestors),
          R.append(artefact),
        ),
        R.pluck('id'),
        R.indexBy(R.identity),
      )(selectableAncestors);
    }
    yield put(
      getArtefactRelativesSuccess({
        artefact,
        ancestors: uniqAncestors,
        descendants,
        selection,
      }),
    );
  } catch (error) {
    yield put(getArtefactRelativesError(error));
  }
}

export default function* saga() {
  yield takeLatest(GET_ARTEFACT_RELATIVES, getArtefactRelativesWorker);
}
