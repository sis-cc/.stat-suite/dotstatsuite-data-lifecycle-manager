import {
  all,
  call,
  race,
  put,
  select,
  take,
  takeLatest,
} from 'redux-saga/effects';
import * as R from 'ramda';
import { dataCleanup } from '../../api/cleanup';
import { deleteStructureRequest } from '../../sdmx-lib';
import {
  CANCEL_RELATIVES_DELETE,
  cancelRelativesDelete,
  cleanUpRelativeArtefact,
  deleteRelativeArtefact,
  deleteRelativeArtefactDone,
  DELETE_RELATIVES_ARTEFACTS,
} from './action-creators';
import { getGroupedSelectedArtefacts } from './selectors';
import { ERROR, SUCCESS } from '../common/constants';

export function* dataCleanUpWorker({ space, id, sdmxId, type }) {
  try {
    const log = yield dataCleanup({
      transferUrl: R.prop('transferUrl', space),
      dataspace: space.id,
      referenceId: sdmxId,
      referenceType: type,
    });
    yield put(cleanUpRelativeArtefact({ id, log }));
  } catch (error) {
    yield put(
      cleanUpRelativeArtefact({
        id,
        log: { type: ERROR, message: error.message, space },
      }),
    );
  }
}

export function* deleteArtefact(artefact) {
  const { id, space, sdmxId, type } = artefact;
  yield put(deleteRelativeArtefact(id));
  try {
    if (type === 'dataflow') {
      yield dataCleanUpWorker({ space, id, sdmxId, type });
    }
    const log = yield call(deleteStructureRequest, {
      ...artefact,
      agency: artefact.agencyId,
    });
    if (!log.isError && type === 'datastructure') {
      yield dataCleanUpWorker({ space, id, sdmxId, type });
    }
    yield put(
      deleteRelativeArtefactDone({
        id,
        log: { type: log.isError ? ERROR : SUCCESS, message: log.data, space },
      }),
    );
    return log;
  } catch (error) {
    yield put(
      deleteRelativeArtefactDone({
        id,
        log: { type: ERROR, message: error.message, space },
      }),
    );
    return { isError: true, message: error.message };
  }
}

function* reccursiveDeleteArtefactsWorker({ artefacts, depths }) {
  if (R.isEmpty(depths)) {
    return;
  }
  const depth = R.head(depths);
  const [conceptSchemes, others] = R.pipe(
    R.propOr([], depth),
    R.partition(R.propEq('type', 'conceptscheme')),
  )(artefacts);
  const [msd, newOthers] = R.partition(R.propEq('type', 'metadatastructure'))(
    others,
  );
  const msdLogs = yield all(
    R.map(artefact => call(deleteArtefact, artefact), msd),
  );
  /* in order to avoid issue of codelists referenced by conceptschemes, first delete conceptschemes if any */
  const conceptSchemesLogs = yield all(
    R.map(artefact => call(deleteArtefact, artefact), conceptSchemes),
  );
  const othersLogs = yield all(
    R.map(artefact => call(deleteArtefact, artefact), newOthers),
  );
  const logs = [...msdLogs, ...conceptSchemesLogs, ...othersLogs];
  const failureLogs = R.find(log => log.isError, logs);
  if (R.isNil(failureLogs)) {
    yield call(reccursiveDeleteArtefactsWorker, {
      artefacts,
      depths: R.tail(depths),
    });
  } else {
    yield put(cancelRelativesDelete());
  }
}

export function* deleteArtefactRelativesWorker(action) {
  const { ids } = action.payload;
  const _groupedByDepthsArtefacts = yield select(getGroupedSelectedArtefacts);
  const groupedByDepthsArtefacts = R.map(
    R.filter(({ id }) => R.includes(id, ids)),
    _groupedByDepthsArtefacts,
  );

  const sortedDepths = R.pipe(
    R.keys,
    R.sortBy(depth => R.negate(Number(depth))),
  )(groupedByDepthsArtefacts);

  yield call(reccursiveDeleteArtefactsWorker, {
    artefacts: groupedByDepthsArtefacts,
    depths: sortedDepths,
  });
}

export function* cancellableDeleteWorker(action) {
  yield race([
    call(deleteArtefactRelativesWorker, action),
    take(CANCEL_RELATIVES_DELETE),
  ]);
}

export default function* saga() {
  yield takeLatest(DELETE_RELATIVES_ARTEFACTS, cancellableDeleteWorker);
}
