import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const relativesReducer = reducerFactory(model(), handlers);
export { default as relativesSaga } from './saga';
export { default as deleteRelativesSaga } from './delete-saga';

export {
  DELETE_RELATIVE_ARTEFACT_DONE,
  getArtefactRelatives,
} from './action-creators';
