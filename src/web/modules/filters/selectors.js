import { createSelector } from 'reselect';
import * as R from 'ramda';
import {
  getExternalSpaces as getExternalSpacesFromConfig,
  getInternalSpaces as getInternalSpacesFromConfig,
  getSpaces as getSpacesFromConfig,
  getTypes as getTypesFromConfig,
} from '../config';
import { getCategories as getCategoriesFromCategories } from '../categories';
import { withSort } from '../common/utils';

const getState = state => state.filters;

export const getSelectionIds = key =>
  createSelector(getState, state => R.prop(key, state));

export const getVersion = () =>
  createSelector(getState, state => R.prop('version', state));
export const getVersions = () =>
  createSelector(getState, state => R.prop('versions', state));
export const getIsFinal = () =>
  createSelector(getState, state => R.prop('isFinal', state));
export const getIsMyArtefacts = () =>
  createSelector(getState, state => R.prop('isMyArtefacts', state));

const withSelection = (filters, ids) =>
  R.reduce(
    (memo, { id }) => {
      const filter = R.prop(id, filters);
      return filter
        ? { ...memo, [id]: { ...filter, isSelected: true } }
        : { ...memo };
    },
    { ...filters },
    R.values(ids),
  );

const isSelected = filters => R.pickBy(filter => filter.isSelected, filters);

export const getInternalSpaces = () =>
  createSelector(
    [getInternalSpacesFromConfig(), getSelectionIds('spaceIds')],
    withSelection,
  );

export const getSelectedInternalSpaces = createSelector(
  [getInternalSpaces()],
  isSelected,
);

export const getExternalSpaces = () =>
  createSelector(
    [getExternalSpacesFromConfig(), getSelectionIds('spaceIds')],
    withSelection,
  );

export const getCategories = () =>
  createSelector(
    [getCategoriesFromCategories(), getSelectionIds('categoryIds')],
    withSelection,
  );

export const getSpaces = () =>
  createSelector(
    [getSpacesFromConfig(), getSelectionIds('spaceIds')],
    withSelection,
  );

export const getSortedInternalSpaces = (fields = []) =>
  createSelector([getInternalSpaces()], withSort(fields));

export const getSortedExternalSpaces = (fields = []) =>
  createSelector([getExternalSpaces()], withSort(fields));

export const getSortedSpaces = (fields = []) =>
  createSelector([getSpaces()], withSort(fields));

export const getTypes = () =>
  createSelector(
    [getTypesFromConfig(), getSelectionIds('typeIds')],
    withSelection,
  );

export const getSortedTypes = () =>
  createSelector([getTypes()], types =>
    R.sortBy(R.prop('label'), R.values(types)),
  );

export const getSelectedSpaces = () =>
  createSelector([getSpaces()], isSelected);
export const getSelectedTypes = () => createSelector([getTypes()], isSelected);

export const getAgencies = () =>
  createSelector(getSelectionIds('agencies'), R.values);

export const getSelectionTypes = () =>
  createSelector([getSelectedTypes()], selectedTypes => R.keys(selectedTypes));

export const getSelectionSpaces = () =>
  createSelector([getSelectedSpaces()], selectedSpaces =>
    R.indexBy(R.prop('endpoint'), R.values(selectedSpaces)),
  );

export const getSelectedCategories = () =>
  createSelector(getCategories(), categories => {
    const selected = isSelected(categories);

    return R.mapObjIndexed(category => {
      const topParentId = category.topParentId;

      if (R.has(topParentId, categories)) {
        const scheme = categories[topParentId];
        return {
          scheme,
          ...R.pick(['code', 'hierarchicalCode', 'spaceId'], category),
        };
      }

      return { scheme: category, spaceId: category.spaceId };
    }, selected);
  });

export const getSelection = () =>
  createSelector(
    [
      getSelectionTypes(),
      getSelectionSpaces(),
      getAgencies(),
      getVersion(),
      getSelectedCategories(),
    ],
    (types, spaces, agencies, version, categories) => {
      return {
        types,
        spaces,
        agencies,
        versions: [version],
        categories,
      };
    },
  );
