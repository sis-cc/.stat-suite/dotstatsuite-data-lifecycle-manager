// export function: model can be configurable and won't be modified if recalled
export default () => ({
  // selected spaces => { spaceId: { id } }
  spaceIds: {},

  // selected types => { typeId: { id } }
  typeIds: {},

  // selected agencies => { agencyId: agency }
  agencies: {},

  // available versions
  versions: { latest: 'latest', all: 'all' },

  // selected version
  version: 'latest',

  // isFinal (attribute of an artefact)
  isFinal: false,

  // isMyArtefacts get artefacts from agencies inter my agencies
  isMyArtefacts: true,

  //selected categories => { categoryId: { id } }
  categoryIds: {},
});
