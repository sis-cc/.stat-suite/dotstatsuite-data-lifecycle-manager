import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import commonjs from 'vite-plugin-commonjs';
import { nodePolyfills } from 'vite-plugin-node-polyfills';
import viteCompression from 'vite-plugin-compression';

export default defineConfig(() => ({
  plugins: [
    commonjs(),
    nodePolyfills(),
    react(),
    viteCompression({ algorithm: 'brotliCompress' }),
  ],
  build: {
    minify: false,
    manifest: true,
    chunkSizeWarningLimit: 5000,
    outDir: 'build',
    rollupOptions: {
      input: 'src/web/index.js',
      output: {
        entryFileNames: 'static/js/bundle.js',
        assetFileNames: 'static/[name].[ext]',
        chunkFileNames: 'static/js/vendors~main.chunk.js',
        manualChunks(id) {
          if (id.includes('node_modules')) {
            return 'vendors';
          }
        },
      },
    },
  },
  esbuild: {
    loader: 'jsx',
    include: /src\/.*\.jsx?$/,
    exclude: [],
  },
}));
